/*
*************************************************************************
**  Copyright (c) 2020 CentraleSupélec & EDF.
**  All rights reserved. This program and the accompanying materials
**  are made available under the terms of the Eclipse Public License v2.0
**  which accompanies this distribution, and is available at
**  https://www.eclipse.org/legal/epl-v20.html
** 
**  This file is part of the RiseClipse tool
**  
**  Contributors:
**      Computer Science Department, CentraleSupélec
**      EDF R&D
**  Contacts:
**      dominique.marcadet@centralesupelec.fr
**      aurelie.dehouck-neveu@edf.fr
**  Web site:
**      http://wdi.supelec.fr/software/RiseClipse/
*************************************************************************
*/
package fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feeder</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder#getNamingSecondarySubstation <em>Naming Secondary Substation</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder#getNormalEnergizingSubstation <em>Normal Energizing Substation</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder#getNormalHeadTerminal <em>Normal Head Terminal</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder#getNormalEnergizedSubstation <em>Normal Energized Substation</em>}</li>
 * </ul>
 *
 * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getFeeder()
 * @model
 * @generated
 */
public interface Feeder extends EquipmentContainer {
    /**
     * Returns the value of the '<em><b>Normal Energized Substation</b></em>' reference list.
     * The list contents are of type {@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation}.
     * It is bidirectional and its opposite is '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getNormalEnergizingFeeder <em>Normal Energizing Feeder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Normal Energized Substation</em>' reference list.
     * @see #isSetNormalEnergizedSubstation()
     * @see #unsetNormalEnergizedSubstation()
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getFeeder_NormalEnergizedSubstation()
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getNormalEnergizingFeeder
     * @model opposite="NormalEnergizingFeeder" resolveProxies="false" unsettable="true" ordered="false"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='Feeder.NormalEnergizedSubstation' kind='element'"
     * @generated
     */
    EList< Substation > getNormalEnergizedSubstation();

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder#getNormalEnergizedSubstation <em>Normal Energized Substation</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetNormalEnergizedSubstation()
     * @see #getNormalEnergizedSubstation()
     * @generated
     */
    void unsetNormalEnergizedSubstation();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder#getNormalEnergizedSubstation <em>Normal Energized Substation</em>}' reference list is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>Normal Energized Substation</em>' reference list is set.
     * @see #unsetNormalEnergizedSubstation()
     * @see #getNormalEnergizedSubstation()
     * @generated
     */
    boolean isSetNormalEnergizedSubstation();

    /**
     * Returns the value of the '<em><b>Normal Energizing Substation</b></em>' reference.
     * It is bidirectional and its opposite is '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getNormalEnergizedFeeder <em>Normal Energized Feeder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Normal Energizing Substation</em>' reference.
     * @see #isSetNormalEnergizingSubstation()
     * @see #unsetNormalEnergizingSubstation()
     * @see #setNormalEnergizingSubstation(Substation)
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getFeeder_NormalEnergizingSubstation()
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getNormalEnergizedFeeder
     * @model opposite="NormalEnergizedFeeder" resolveProxies="false" unsettable="true" ordered="false"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='Feeder.NormalEnergizingSubstation' kind='element'"
     * @generated
     */
    Substation getNormalEnergizingSubstation();

    /**
     * Sets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder#getNormalEnergizingSubstation <em>Normal Energizing Substation</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Normal Energizing Substation</em>' reference.
     * @see #isSetNormalEnergizingSubstation()
     * @see #unsetNormalEnergizingSubstation()
     * @see #getNormalEnergizingSubstation()
     * @generated
     */
    void setNormalEnergizingSubstation( Substation value );

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder#getNormalEnergizingSubstation <em>Normal Energizing Substation</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetNormalEnergizingSubstation()
     * @see #getNormalEnergizingSubstation()
     * @see #setNormalEnergizingSubstation(Substation)
     * @generated
     */
    void unsetNormalEnergizingSubstation();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder#getNormalEnergizingSubstation <em>Normal Energizing Substation</em>}' reference is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>Normal Energizing Substation</em>' reference is set.
     * @see #unsetNormalEnergizingSubstation()
     * @see #getNormalEnergizingSubstation()
     * @see #setNormalEnergizingSubstation(Substation)
     * @generated
     */
    boolean isSetNormalEnergizingSubstation();

    /**
     * Returns the value of the '<em><b>Normal Head Terminal</b></em>' reference list.
     * The list contents are of type {@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Terminal}.
     * It is bidirectional and its opposite is '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Terminal#getNormalHeadFeeder <em>Normal Head Feeder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Normal Head Terminal</em>' reference list.
     * @see #isSetNormalHeadTerminal()
     * @see #unsetNormalHeadTerminal()
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getFeeder_NormalHeadTerminal()
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Terminal#getNormalHeadFeeder
     * @model opposite="NormalHeadFeeder" unsettable="true" transient="true" ordered="false"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='Feeder.NormalHeadTerminal' kind='element'"
     * @generated
     */
    EList< Terminal > getNormalHeadTerminal();

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder#getNormalHeadTerminal <em>Normal Head Terminal</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetNormalHeadTerminal()
     * @see #getNormalHeadTerminal()
     * @generated
     */
    void unsetNormalHeadTerminal();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder#getNormalHeadTerminal <em>Normal Head Terminal</em>}' reference list is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>Normal Head Terminal</em>' reference list is set.
     * @see #unsetNormalHeadTerminal()
     * @see #getNormalHeadTerminal()
     * @generated
     */
    boolean isSetNormalHeadTerminal();

    /**
     * Returns the value of the '<em><b>Naming Secondary Substation</b></em>' reference list.
     * The list contents are of type {@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation}.
     * It is bidirectional and its opposite is '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getNamingFeeder <em>Naming Feeder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Naming Secondary Substation</em>' reference list.
     * @see #isSetNamingSecondarySubstation()
     * @see #unsetNamingSecondarySubstation()
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getFeeder_NamingSecondarySubstation()
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getNamingFeeder
     * @model opposite="NamingFeeder" unsettable="true" transient="true" ordered="false"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='Feeder.NamingSecondarySubstation' kind='element'"
     * @generated
     */
    EList< Substation > getNamingSecondarySubstation();

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder#getNamingSecondarySubstation <em>Naming Secondary Substation</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetNamingSecondarySubstation()
     * @see #getNamingSecondarySubstation()
     * @generated
     */
    void unsetNamingSecondarySubstation();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder#getNamingSecondarySubstation <em>Naming Secondary Substation</em>}' reference list is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>Naming Secondary Substation</em>' reference list is set.
     * @see #unsetNamingSecondarySubstation()
     * @see #getNamingSecondarySubstation()
     * @generated
     */
    boolean isSetNamingSecondarySubstation();

} // Feeder
