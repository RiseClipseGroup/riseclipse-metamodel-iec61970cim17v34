/*
*************************************************************************
**  Copyright (c) 2020 CentraleSupélec & EDF.
**  All rights reserved. This program and the accompanying materials
**  are made available under the terms of the Eclipse Public License v2.0
**  which accompanies this distribution, and is available at
**  https://www.eclipse.org/legal/epl-v20.html
** 
**  This file is part of the RiseClipse tool
**  
**  Contributors:
**      Computer Science Department, CentraleSupélec
**      EDF R&D
**  Contacts:
**      dominique.marcadet@centralesupelec.fr
**      aurelie.dehouck-neveu@edf.fr
**  Web site:
**      http://wdi.supelec.fr/software/RiseClipse/
*************************************************************************
*/
package fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.impl;

import fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage;
import fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Equipment;
import fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.EquipmentContainer;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Equipment Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.impl.EquipmentContainerImpl#getAdditionalGroupedEquipment <em>Additional Grouped Equipment</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.impl.EquipmentContainerImpl#getEquipments <em>Equipments</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EquipmentContainerImpl extends ConnectivityNodeContainerImpl implements EquipmentContainer {
    /**
     * The cached value of the '{@link #getAdditionalGroupedEquipment() <em>Additional Grouped Equipment</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getAdditionalGroupedEquipment()
     * @generated
     * @ordered
     */
    protected EList< Equipment > additionalGroupedEquipment;

    /**
     * The cached value of the '{@link #getEquipments() <em>Equipments</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getEquipments()
     * @generated
     * @ordered
     */
    protected EList< Equipment > equipments;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected EquipmentContainerImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CimPackage.eINSTANCE.getEquipmentContainer();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList< Equipment > getEquipments() {
        if( equipments == null ) {
            equipments = new EObjectWithInverseResolvingEList.Unsettable< Equipment >( Equipment.class, this,
                    CimPackage.EQUIPMENT_CONTAINER__EQUIPMENTS, CimPackage.EQUIPMENT__EQUIPMENT_CONTAINER );
        }
        return equipments;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void unsetEquipments() {
        if( equipments != null ) ( ( InternalEList.Unsettable< ? > ) equipments ).unset();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isSetEquipments() {
        return equipments != null && ( ( InternalEList.Unsettable< ? > ) equipments ).isSet();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings( "unchecked" )
    @Override
    public NotificationChain eInverseAdd( InternalEObject otherEnd, int featureID, NotificationChain msgs ) {
        switch( featureID ) {
        case CimPackage.EQUIPMENT_CONTAINER__ADDITIONAL_GROUPED_EQUIPMENT:
            return ( ( InternalEList< InternalEObject > ) ( InternalEList< ? > ) getAdditionalGroupedEquipment() )
                    .basicAdd( otherEnd, msgs );
        case CimPackage.EQUIPMENT_CONTAINER__EQUIPMENTS:
            return ( ( InternalEList< InternalEObject > ) ( InternalEList< ? > ) getEquipments() ).basicAdd( otherEnd,
                    msgs );
        }
        return super.eInverseAdd( otherEnd, featureID, msgs );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove( InternalEObject otherEnd, int featureID, NotificationChain msgs ) {
        switch( featureID ) {
        case CimPackage.EQUIPMENT_CONTAINER__ADDITIONAL_GROUPED_EQUIPMENT:
            return ( ( InternalEList< ? > ) getAdditionalGroupedEquipment() ).basicRemove( otherEnd, msgs );
        case CimPackage.EQUIPMENT_CONTAINER__EQUIPMENTS:
            return ( ( InternalEList< ? > ) getEquipments() ).basicRemove( otherEnd, msgs );
        }
        return super.eInverseRemove( otherEnd, featureID, msgs );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList< Equipment > getAdditionalGroupedEquipment() {
        if( additionalGroupedEquipment == null ) {
            additionalGroupedEquipment = new EObjectWithInverseEList.Unsettable.ManyInverse< Equipment >(
                    Equipment.class, this, CimPackage.EQUIPMENT_CONTAINER__ADDITIONAL_GROUPED_EQUIPMENT,
                    CimPackage.EQUIPMENT__ADDITIONAL_EQUIPMENT_CONTAINER );
        }
        return additionalGroupedEquipment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void unsetAdditionalGroupedEquipment() {
        if( additionalGroupedEquipment != null )
            ( ( InternalEList.Unsettable< ? > ) additionalGroupedEquipment ).unset();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isSetAdditionalGroupedEquipment() {
        return additionalGroupedEquipment != null
                && ( ( InternalEList.Unsettable< ? > ) additionalGroupedEquipment ).isSet();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet( int featureID, boolean resolve, boolean coreType ) {
        switch( featureID ) {
        case CimPackage.EQUIPMENT_CONTAINER__ADDITIONAL_GROUPED_EQUIPMENT:
            return getAdditionalGroupedEquipment();
        case CimPackage.EQUIPMENT_CONTAINER__EQUIPMENTS:
            return getEquipments();
        }
        return super.eGet( featureID, resolve, coreType );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings( "unchecked" )
    @Override
    public void eSet( int featureID, Object newValue ) {
        switch( featureID ) {
        case CimPackage.EQUIPMENT_CONTAINER__ADDITIONAL_GROUPED_EQUIPMENT:
            getAdditionalGroupedEquipment().clear();
            getAdditionalGroupedEquipment().addAll( ( Collection< ? extends Equipment > ) newValue );
            return;
        case CimPackage.EQUIPMENT_CONTAINER__EQUIPMENTS:
            getEquipments().clear();
            getEquipments().addAll( ( Collection< ? extends Equipment > ) newValue );
            return;
        }
        super.eSet( featureID, newValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset( int featureID ) {
        switch( featureID ) {
        case CimPackage.EQUIPMENT_CONTAINER__ADDITIONAL_GROUPED_EQUIPMENT:
            unsetAdditionalGroupedEquipment();
            return;
        case CimPackage.EQUIPMENT_CONTAINER__EQUIPMENTS:
            unsetEquipments();
            return;
        }
        super.eUnset( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet( int featureID ) {
        switch( featureID ) {
        case CimPackage.EQUIPMENT_CONTAINER__ADDITIONAL_GROUPED_EQUIPMENT:
            return isSetAdditionalGroupedEquipment();
        case CimPackage.EQUIPMENT_CONTAINER__EQUIPMENTS:
            return isSetEquipments();
        }
        return super.eIsSet( featureID );
    }

} //EquipmentContainerImpl
