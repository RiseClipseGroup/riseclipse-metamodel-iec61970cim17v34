/*
*************************************************************************
**  Copyright (c) 2020 CentraleSupélec & EDF.
**  All rights reserved. This program and the accompanying materials
**  are made available under the terms of the Eclipse Public License v2.0
**  which accompanies this distribution, and is available at
**  https://www.eclipse.org/legal/epl-v20.html
** 
**  This file is part of the RiseClipse tool
**  
**  Contributors:
**      Computer Science Department, CentraleSupélec
**      EDF R&D
**  Contacts:
**      dominique.marcadet@centralesupelec.fr
**      aurelie.dehouck-neveu@edf.fr
**  Web site:
**      http://wdi.supelec.fr/software/RiseClipse/
*************************************************************************
*/
package fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.impl;

import fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage;
import fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PowerElectronicsConnection;
import fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PowerElectronicsConnectionPhase;
import fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.SinglePhaseKind;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Power Electronics Connection Phase</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.impl.PowerElectronicsConnectionPhaseImpl#getP <em>P</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.impl.PowerElectronicsConnectionPhaseImpl#getPhase <em>Phase</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.impl.PowerElectronicsConnectionPhaseImpl#getQ <em>Q</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.impl.PowerElectronicsConnectionPhaseImpl#getPowerElectronicsConnection <em>Power Electronics Connection</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PowerElectronicsConnectionPhaseImpl extends PowerSystemResourceImpl
        implements PowerElectronicsConnectionPhase {
    /**
     * The default value of the '{@link #getP() <em>P</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getP()
     * @generated
     * @ordered
     */
    protected static final Float P_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getP() <em>P</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getP()
     * @generated
     * @ordered
     */
    protected Float p = P_EDEFAULT;

    /**
     * This is true if the P attribute has been set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    protected boolean pESet;

    /**
     * The default value of the '{@link #getPhase() <em>Phase</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getPhase()
     * @generated
     * @ordered
     */
    protected static final SinglePhaseKind PHASE_EDEFAULT = SinglePhaseKind.A;

    /**
     * The cached value of the '{@link #getPhase() <em>Phase</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getPhase()
     * @generated
     * @ordered
     */
    protected SinglePhaseKind phase = PHASE_EDEFAULT;

    /**
     * This is true if the Phase attribute has been set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    protected boolean phaseESet;

    /**
     * The default value of the '{@link #getQ() <em>Q</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getQ()
     * @generated
     * @ordered
     */
    protected static final Float Q_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getQ() <em>Q</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getQ()
     * @generated
     * @ordered
     */
    protected Float q = Q_EDEFAULT;

    /**
     * This is true if the Q attribute has been set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    protected boolean qESet;

    /**
     * The cached value of the '{@link #getPowerElectronicsConnection() <em>Power Electronics Connection</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getPowerElectronicsConnection()
     * @generated
     * @ordered
     */
    protected PowerElectronicsConnection powerElectronicsConnection;

    /**
     * This is true if the Power Electronics Connection reference has been set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    protected boolean powerElectronicsConnectionESet;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected PowerElectronicsConnectionPhaseImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CimPackage.eINSTANCE.getPowerElectronicsConnectionPhase();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Float getP() {
        return p;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setP( Float newP ) {
        Float oldP = p;
        p = newP;
        boolean oldPESet = pESet;
        pESet = true;
        if( eNotificationRequired() )
            eNotify( new ENotificationImpl( this, Notification.SET, CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__P,
                    oldP, p, !oldPESet ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void unsetP() {
        Float oldP = p;
        boolean oldPESet = pESet;
        p = P_EDEFAULT;
        pESet = false;
        if( eNotificationRequired() )
            eNotify( new ENotificationImpl( this, Notification.UNSET, CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__P,
                    oldP, P_EDEFAULT, oldPESet ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isSetP() {
        return pESet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SinglePhaseKind getPhase() {
        return phase;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setPhase( SinglePhaseKind newPhase ) {
        SinglePhaseKind oldPhase = phase;
        phase = newPhase == null ? PHASE_EDEFAULT : newPhase;
        boolean oldPhaseESet = phaseESet;
        phaseESet = true;
        if( eNotificationRequired() )
            eNotify( new ENotificationImpl( this, Notification.SET,
                    CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__PHASE, oldPhase, phase, !oldPhaseESet ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void unsetPhase() {
        SinglePhaseKind oldPhase = phase;
        boolean oldPhaseESet = phaseESet;
        phase = PHASE_EDEFAULT;
        phaseESet = false;
        if( eNotificationRequired() )
            eNotify( new ENotificationImpl( this, Notification.UNSET,
                    CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__PHASE, oldPhase, PHASE_EDEFAULT, oldPhaseESet ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isSetPhase() {
        return phaseESet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Float getQ() {
        return q;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setQ( Float newQ ) {
        Float oldQ = q;
        q = newQ;
        boolean oldQESet = qESet;
        qESet = true;
        if( eNotificationRequired() )
            eNotify( new ENotificationImpl( this, Notification.SET, CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__Q,
                    oldQ, q, !oldQESet ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void unsetQ() {
        Float oldQ = q;
        boolean oldQESet = qESet;
        q = Q_EDEFAULT;
        qESet = false;
        if( eNotificationRequired() )
            eNotify( new ENotificationImpl( this, Notification.UNSET, CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__Q,
                    oldQ, Q_EDEFAULT, oldQESet ) );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isSetQ() {
        return qESet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PowerElectronicsConnection getPowerElectronicsConnection() {
        return powerElectronicsConnection;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetPowerElectronicsConnection(
            PowerElectronicsConnection newPowerElectronicsConnection, NotificationChain msgs ) {
        PowerElectronicsConnection oldPowerElectronicsConnection = powerElectronicsConnection;
        powerElectronicsConnection = newPowerElectronicsConnection;
        boolean oldPowerElectronicsConnectionESet = powerElectronicsConnectionESet;
        powerElectronicsConnectionESet = true;
        if( eNotificationRequired() ) {
            ENotificationImpl notification = new ENotificationImpl( this, Notification.SET,
                    CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__POWER_ELECTRONICS_CONNECTION,
                    oldPowerElectronicsConnection, newPowerElectronicsConnection, !oldPowerElectronicsConnectionESet );
            if( msgs == null )
                msgs = notification;
            else
                msgs.add( notification );
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setPowerElectronicsConnection( PowerElectronicsConnection newPowerElectronicsConnection ) {
        if( newPowerElectronicsConnection != powerElectronicsConnection ) {
            NotificationChain msgs = null;
            if( powerElectronicsConnection != null )
                msgs = ( ( InternalEObject ) powerElectronicsConnection ).eInverseRemove( this,
                        CimPackage.POWER_ELECTRONICS_CONNECTION__POWER_ELECTRONICS_CONNECTION_PHASE,
                        PowerElectronicsConnection.class, msgs );
            if( newPowerElectronicsConnection != null )
                msgs = ( ( InternalEObject ) newPowerElectronicsConnection ).eInverseAdd( this,
                        CimPackage.POWER_ELECTRONICS_CONNECTION__POWER_ELECTRONICS_CONNECTION_PHASE,
                        PowerElectronicsConnection.class, msgs );
            msgs = basicSetPowerElectronicsConnection( newPowerElectronicsConnection, msgs );
            if( msgs != null ) msgs.dispatch();
        }
        else {
            boolean oldPowerElectronicsConnectionESet = powerElectronicsConnectionESet;
            powerElectronicsConnectionESet = true;
            if( eNotificationRequired() )
                eNotify( new ENotificationImpl( this, Notification.SET,
                        CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__POWER_ELECTRONICS_CONNECTION,
                        newPowerElectronicsConnection, newPowerElectronicsConnection,
                        !oldPowerElectronicsConnectionESet ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicUnsetPowerElectronicsConnection( NotificationChain msgs ) {
        PowerElectronicsConnection oldPowerElectronicsConnection = powerElectronicsConnection;
        powerElectronicsConnection = null;
        boolean oldPowerElectronicsConnectionESet = powerElectronicsConnectionESet;
        powerElectronicsConnectionESet = false;
        if( eNotificationRequired() ) {
            ENotificationImpl notification = new ENotificationImpl( this, Notification.UNSET,
                    CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__POWER_ELECTRONICS_CONNECTION,
                    oldPowerElectronicsConnection, null, oldPowerElectronicsConnectionESet );
            if( msgs == null )
                msgs = notification;
            else
                msgs.add( notification );
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void unsetPowerElectronicsConnection() {
        if( powerElectronicsConnection != null ) {
            NotificationChain msgs = null;
            msgs = ( ( InternalEObject ) powerElectronicsConnection ).eInverseRemove( this,
                    CimPackage.POWER_ELECTRONICS_CONNECTION__POWER_ELECTRONICS_CONNECTION_PHASE,
                    PowerElectronicsConnection.class, msgs );
            msgs = basicUnsetPowerElectronicsConnection( msgs );
            if( msgs != null ) msgs.dispatch();
        }
        else {
            boolean oldPowerElectronicsConnectionESet = powerElectronicsConnectionESet;
            powerElectronicsConnectionESet = false;
            if( eNotificationRequired() )
                eNotify( new ENotificationImpl( this, Notification.UNSET,
                        CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__POWER_ELECTRONICS_CONNECTION, null, null,
                        oldPowerElectronicsConnectionESet ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isSetPowerElectronicsConnection() {
        return powerElectronicsConnectionESet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseAdd( InternalEObject otherEnd, int featureID, NotificationChain msgs ) {
        switch( featureID ) {
        case CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__POWER_ELECTRONICS_CONNECTION:
            if( powerElectronicsConnection != null )
                msgs = ( ( InternalEObject ) powerElectronicsConnection ).eInverseRemove( this,
                        CimPackage.POWER_ELECTRONICS_CONNECTION__POWER_ELECTRONICS_CONNECTION_PHASE,
                        PowerElectronicsConnection.class, msgs );
            return basicSetPowerElectronicsConnection( ( PowerElectronicsConnection ) otherEnd, msgs );
        }
        return super.eInverseAdd( otherEnd, featureID, msgs );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove( InternalEObject otherEnd, int featureID, NotificationChain msgs ) {
        switch( featureID ) {
        case CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__POWER_ELECTRONICS_CONNECTION:
            return basicUnsetPowerElectronicsConnection( msgs );
        }
        return super.eInverseRemove( otherEnd, featureID, msgs );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet( int featureID, boolean resolve, boolean coreType ) {
        switch( featureID ) {
        case CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__P:
            return getP();
        case CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__PHASE:
            return getPhase();
        case CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__Q:
            return getQ();
        case CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__POWER_ELECTRONICS_CONNECTION:
            return getPowerElectronicsConnection();
        }
        return super.eGet( featureID, resolve, coreType );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eSet( int featureID, Object newValue ) {
        switch( featureID ) {
        case CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__P:
            setP( ( Float ) newValue );
            return;
        case CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__PHASE:
            setPhase( ( SinglePhaseKind ) newValue );
            return;
        case CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__Q:
            setQ( ( Float ) newValue );
            return;
        case CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__POWER_ELECTRONICS_CONNECTION:
            setPowerElectronicsConnection( ( PowerElectronicsConnection ) newValue );
            return;
        }
        super.eSet( featureID, newValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset( int featureID ) {
        switch( featureID ) {
        case CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__P:
            unsetP();
            return;
        case CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__PHASE:
            unsetPhase();
            return;
        case CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__Q:
            unsetQ();
            return;
        case CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__POWER_ELECTRONICS_CONNECTION:
            unsetPowerElectronicsConnection();
            return;
        }
        super.eUnset( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet( int featureID ) {
        switch( featureID ) {
        case CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__P:
            return isSetP();
        case CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__PHASE:
            return isSetPhase();
        case CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__Q:
            return isSetQ();
        case CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE__POWER_ELECTRONICS_CONNECTION:
            return isSetPowerElectronicsConnection();
        }
        return super.eIsSet( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String toString() {
        if( eIsProxy() ) return super.toString();

        StringBuilder result = new StringBuilder( super.toString() );
        result.append( " (p: " );
        if( pESet )
            result.append( p );
        else
            result.append( "<unset>" );
        result.append( ", phase: " );
        if( phaseESet )
            result.append( phase );
        else
            result.append( "<unset>" );
        result.append( ", q: " );
        if( qESet )
            result.append( q );
        else
            result.append( "<unset>" );
        result.append( ')' );
        return result.toString();
    }

} //PowerElectronicsConnectionPhaseImpl
