/*
*************************************************************************
**  Copyright (c) 2020 CentraleSupélec & EDF.
**  All rights reserved. This program and the accompanying materials
**  are made available under the terms of the Eclipse Public License v2.0
**  which accompanies this distribution, and is available at
**  https://www.eclipse.org/legal/epl-v20.html
** 
**  This file is part of the RiseClipse tool
**  
**  Contributors:
**      Computer Science Department, CentraleSupélec
**      EDF R&D
**  Contacts:
**      dominique.marcadet@centralesupelec.fr
**      aurelie.dehouck-neveu@edf.fr
**  Web site:
**      http://wdi.supelec.fr/software/RiseClipse/
*************************************************************************
*/
package fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.impl;

import fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CimFactoryImpl extends EFactoryImpl implements CimFactory {
    /**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public static CimFactory init() {
        try {
            CimFactory theCimFactory = ( CimFactory ) EPackage.Registry.INSTANCE.getEFactory( CimPackage.eNS_URI );
            if( theCimFactory != null ) {
                return theCimFactory;
            }
        }
        catch( Exception exception ) {
            EcorePlugin.INSTANCE.log( exception );
        }
        return new CimFactoryImpl();
    }

    /**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public CimFactoryImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EObject create( EClass eClass ) {
        switch( eClass.getClassifierID() ) {
        case CimPackage.MONTH_DAY_INTERVAL:
            return createMonthDayInterval();
        case CimPackage.DATE_INTERVAL:
            return createDateInterval();
        case CimPackage.INTEGER_QUANTITY:
            return createIntegerQuantity();
        case CimPackage.FLOAT_QUANTITY:
            return createFloatQuantity();
        case CimPackage.DECIMAL_QUANTITY:
            return createDecimalQuantity();
        case CimPackage.DATE_TIME_INTERVAL:
            return createDateTimeInterval();
        case CimPackage.FAULT_IMPEDANCE:
            return createFaultImpedance();
        case CimPackage.STRING_QUANTITY:
            return createStringQuantity();
        case CimPackage.TIME_INTERVAL:
            return createTimeInterval();
        case CimPackage.EQUIVALENT_BRANCH:
            return createEquivalentBranch();
        case CimPackage.CONTROL_AREA_GENERATING_UNIT:
            return createControlAreaGeneratingUnit();
        case CimPackage.PENSTOCK_LOSS_CURVE:
            return createPenstockLossCurve();
        case CimPackage.OPERATIONAL_LIMIT_TYPE:
            return createOperationalLimitType();
        case CimPackage.VS_CONVERTER:
            return createVsConverter();
        case CimPackage.SV_SWITCH:
            return createSvSwitch();
        case CimPackage.SERIES_COMPENSATOR:
            return createSeriesCompensator();
        case CimPackage.FAULT_CAUSE_TYPE:
            return createFaultCauseType();
        case CimPackage.JUMPER:
            return createJumper();
        case CimPackage.TEXT_DIAGRAM_OBJECT:
            return createTextDiagramObject();
        case CimPackage.DC_DISCONNECTOR:
            return createDCDisconnector();
        case CimPackage.AIR_COMPRESSOR:
            return createAirCompressor();
        case CimPackage.REMOTE_UNIT:
            return createRemoteUnit();
        case CimPackage.BREAKER:
            return createBreaker();
        case CimPackage.STRING_MEASUREMENT:
            return createStringMeasurement();
        case CimPackage.POWER_SYSTEM_RESOURCE:
            return createPowerSystemResource();
        case CimPackage.OPERATING_PARTICIPANT:
            return createOperatingParticipant();
        case CimPackage.LINE_FAULT:
            return createLineFault();
        case CimPackage.SUBCRITICAL:
            return createSubcritical();
        case CimPackage.CAES_PLANT:
            return createCAESPlant();
        case CimPackage.NAME:
            return createName();
        case CimPackage.CURRENT_LIMIT:
            return createCurrentLimit();
        case CimPackage.PHASE_TAP_CHANGER_SYMMETRICAL:
            return createPhaseTapChangerSymmetrical();
        case CimPackage.STEAM_SENDOUT_SCHEDULE:
            return createSteamSendoutSchedule();
        case CimPackage.TRANSFORMER_MESH_IMPEDANCE:
            return createTransformerMeshImpedance();
        case CimPackage.PHASE_IMPEDANCE_DATA:
            return createPhaseImpedanceData();
        case CimPackage.APPARENT_POWER_LIMIT:
            return createApparentPowerLimit();
        case CimPackage.LIMIT_SET:
            return createLimitSet();
        case CimPackage.PHASE_TAP_CHANGER_TABLE_POINT:
            return createPhaseTapChangerTablePoint();
        case CimPackage.SHUNT_COMPENSATOR:
            return createShuntCompensator();
        case CimPackage.VISIBILITY_LAYER:
            return createVisibilityLayer();
        case CimPackage.ENERGY_SOURCE_PHASE:
            return createEnergySourcePhase();
        case CimPackage.RAISE_LOWER_COMMAND:
            return createRaiseLowerCommand();
        case CimPackage.REPORTING_SUPER_GROUP:
            return createReportingSuperGroup();
        case CimPackage.HYDRO_POWER_PLANT:
            return createHydroPowerPlant();
        case CimPackage.TARGET_LEVEL_SCHEDULE:
            return createTargetLevelSchedule();
        case CimPackage.SV_SHUNT_COMPENSATOR_SECTIONS:
            return createSvShuntCompensatorSections();
        case CimPackage.CONNECTIVITY_NODE:
            return createConnectivityNode();
        case CimPackage.POWER_CUT_ZONE:
            return createPowerCutZone();
        case CimPackage.CONFORM_LOAD_GROUP:
            return createConformLoadGroup();
        case CimPackage.BASE_VOLTAGE:
            return createBaseVoltage();
        case CimPackage.PHASE_TAP_CHANGER_ASYMMETRICAL:
            return createPhaseTapChangerAsymmetrical();
        case CimPackage.NONLINEAR_SHUNT_COMPENSATOR_PHASE_POINT:
            return createNonlinearShuntCompensatorPhasePoint();
        case CimPackage.IRREGULAR_INTERVAL_SCHEDULE:
            return createIrregularIntervalSchedule();
        case CimPackage.MEASUREMENT_VALUE_SOURCE:
            return createMeasurementValueSource();
        case CimPackage.FAULT:
            return createFault();
        case CimPackage.SYNCHRONOUS_MACHINE:
            return createSynchronousMachine();
        case CimPackage.RATIO_TAP_CHANGER_TABLE:
            return createRatioTapChangerTable();
        case CimPackage.EQUIPMENT:
            return createEquipment();
        case CimPackage.BAY:
            return createBay();
        case CimPackage.CT_TEMP_ACTIVE_POWER_CURVE:
            return createCTTempActivePowerCurve();
        case CimPackage.ROTATING_MACHINE:
            return createRotatingMachine();
        case CimPackage.BATTERY_UNIT:
            return createBatteryUnit();
        case CimPackage.PER_LENGTH_DC_LINE_PARAMETER:
            return createPerLengthDCLineParameter();
        case CimPackage.LINE:
            return createLine();
        case CimPackage.TAILBAY_LOSS_CURVE:
            return createTailbayLossCurve();
        case CimPackage.CURVE_DATA:
            return createCurveData();
        case CimPackage.GEOGRAPHICAL_REGION:
            return createGeographicalRegion();
        case CimPackage.CURRENT_RELAY:
            return createCurrentRelay();
        case CimPackage.NONLINEAR_SHUNT_COMPENSATOR_PHASE:
            return createNonlinearShuntCompensatorPhase();
        case CimPackage.BASE_POWER:
            return createBasePower();
        case CimPackage.CONNECTOR:
            return createConnector();
        case CimPackage.SEASON_DAY_TYPE_SCHEDULE:
            return createSeasonDayTypeSchedule();
        case CimPackage.DC_TOPOLOGICAL_ISLAND:
            return createDCTopologicalIsland();
        case CimPackage.EQUIPMENT_CONTAINER:
            return createEquipmentContainer();
        case CimPackage.PROTECTED_SWITCH:
            return createProtectedSwitch();
        case CimPackage.HEAT_RATE_CURVE:
            return createHeatRateCurve();
        case CimPackage.SV_INJECTION:
            return createSvInjection();
        case CimPackage.STEAM_SUPPLY:
            return createSteamSupply();
        case CimPackage.REGULAR_TIME_POINT:
            return createRegularTimePoint();
        case CimPackage.EQUIVALENT_EQUIPMENT:
            return createEquivalentEquipment();
        case CimPackage.POTENTIAL_TRANSFORMER:
            return createPotentialTransformer();
        case CimPackage.SET_POINT:
            return createSetPoint();
        case CimPackage.JUNCTION:
            return createJunction();
        case CimPackage.DC_SERIES_DEVICE:
            return createDCSeriesDevice();
        case CimPackage.PHASE_TAP_CHANGER_TABULAR:
            return createPhaseTapChangerTabular();
        case CimPackage.CONNECTIVITY_NODE_CONTAINER:
            return createConnectivityNodeContainer();
        case CimPackage.ACCUMULATOR_LIMIT_SET:
            return createAccumulatorLimitSet();
        case CimPackage.TAP_CHANGER_TABLE_POINT:
            return createTapChangerTablePoint();
        case CimPackage.MEASUREMENT:
            return createMeasurement();
        case CimPackage.NONLINEAR_SHUNT_COMPENSATOR_POINT:
            return createNonlinearShuntCompensatorPoint();
        case CimPackage.FUEL_ALLOCATION_SCHEDULE:
            return createFuelAllocationSchedule();
        case CimPackage.MEASUREMENT_VALUE_QUALITY:
            return createMeasurementValueQuality();
        case CimPackage.VOLTAGE_LIMIT:
            return createVoltageLimit();
        case CimPackage.STATE_VARIABLE:
            return createStateVariable();
        case CimPackage.FREQUENCY_CONVERTER:
            return createFrequencyConverter();
        case CimPackage.DIAGRAM_OBJECT_POINT:
            return createDiagramObjectPoint();
        case CimPackage.EXTERNAL_NETWORK_INJECTION:
            return createExternalNetworkInjection();
        case CimPackage.COMMAND:
            return createCommand();
        case CimPackage.HYDRO_GENERATING_UNIT:
            return createHydroGeneratingUnit();
        case CimPackage.POST_LINE_SENSOR:
            return createPostLineSensor();
        case CimPackage.PWR_STEAM_SUPPLY:
            return createPWRSteamSupply();
        case CimPackage.CUT:
            return createCut();
        case CimPackage.FEEDER:
            return createFeeder();
        case CimPackage.ANALOG_LIMIT_SET:
            return createAnalogLimitSet();
        case CimPackage.SV_TAP_STEP:
            return createSvTapStep();
        case CimPackage.ENERGY_AREA:
            return createEnergyArea();
        case CimPackage.START_RAMP_CURVE:
            return createStartRampCurve();
        case CimPackage.DIAGRAM:
            return createDiagram();
        case CimPackage.DISCRETE:
            return createDiscrete();
        case CimPackage.BASIC_INTERVAL_SCHEDULE:
            return createBasicIntervalSchedule();
        case CimPackage.RATIO_TAP_CHANGER:
            return createRatioTapChanger();
        case CimPackage.SWITCH_SCHEDULE:
            return createSwitchSchedule();
        case CimPackage.DISCONNECTOR:
            return createDisconnector();
        case CimPackage.PHASE_TAP_CHANGER_TABLE:
            return createPhaseTapChangerTable();
        case CimPackage.COMPOSITE_SWITCH:
            return createCompositeSwitch();
        case CimPackage.STARTUP_MODEL:
            return createStartupModel();
        case CimPackage.ALT_GENERATING_UNIT_MEAS:
            return createAltGeneratingUnitMeas();
        case CimPackage.DC_CHOPPER:
            return createDCChopper();
        case CimPackage.POWER_ELECTRONICS_CONNECTION_PHASE:
            return createPowerElectronicsConnectionPhase();
        case CimPackage.ASYNCHRONOUS_MACHINE:
            return createAsynchronousMachine();
        case CimPackage.REACTIVE_CAPABILITY_CURVE:
            return createReactiveCapabilityCurve();
        case CimPackage.QUALITY61850:
            return createQuality61850();
        case CimPackage.VOLTAGE_LEVEL:
            return createVoltageLevel();
        case CimPackage.SYNCHROCHECK_RELAY:
            return createSynchrocheckRelay();
        case CimPackage.POWER_ELECTRONICS_UNIT:
            return createPowerElectronicsUnit();
        case CimPackage.GENERATING_UNIT:
            return createGeneratingUnit();
        case CimPackage.ACTIVE_POWER_LIMIT:
            return createActivePowerLimit();
        case CimPackage.HYDRO_GENERATING_EFFICIENCY_CURVE:
            return createHydroGeneratingEfficiencyCurve();
        case CimPackage.CONTINGENCY_ELEMENT:
            return createContingencyElement();
        case CimPackage.OPERATING_SHARE:
            return createOperatingShare();
        case CimPackage.SV_VOLTAGE:
            return createSvVoltage();
        case CimPackage.PER_LENGTH_PHASE_IMPEDANCE:
            return createPerLengthPhaseImpedance();
        case CimPackage.COGENERATION_PLANT:
            return createCogenerationPlant();
        case CimPackage.ANALOG:
            return createAnalog();
        case CimPackage.SUPERCRITICAL:
            return createSupercritical();
        case CimPackage.REMOTE_SOURCE:
            return createRemoteSource();
        case CimPackage.INCREMENTAL_HEAT_RATE_CURVE:
            return createIncrementalHeatRateCurve();
        case CimPackage.DC_SHUNT:
            return createDCShunt();
        case CimPackage.PER_LENGTH_IMPEDANCE:
            return createPerLengthImpedance();
        case CimPackage.COMBUSTION_TURBINE:
            return createCombustionTurbine();
        case CimPackage.PSR_TYPE:
            return createPSRType();
        case CimPackage.HYDRO_TURBINE:
            return createHydroTurbine();
        case CimPackage.EMISSION_CURVE:
            return createEmissionCurve();
        case CimPackage.COMBINED_CYCLE_PLANT:
            return createCombinedCyclePlant();
        case CimPackage.CONFORM_LOAD:
            return createConformLoad();
        case CimPackage.SECTIONALISER:
            return createSectionaliser();
        case CimPackage.EQUIVALENT_NETWORK:
            return createEquivalentNetwork();
        case CimPackage.STATION_SUPPLY:
            return createStationSupply();
        case CimPackage.RECLOSE_SEQUENCE:
            return createRecloseSequence();
        case CimPackage.BUSBAR_SECTION:
            return createBusbarSection();
        case CimPackage.TAP_SCHEDULE:
            return createTapSchedule();
        case CimPackage.REGULAR_INTERVAL_SCHEDULE:
            return createRegularIntervalSchedule();
        case CimPackage.START_MAIN_FUEL_CURVE:
            return createStartMainFuelCurve();
        case CimPackage.DIAGRAM_STYLE:
            return createDiagramStyle();
        case CimPackage.ANALOG_VALUE:
            return createAnalogValue();
        case CimPackage.RATIO_TAP_CHANGER_TABLE_POINT:
            return createRatioTapChangerTablePoint();
        case CimPackage.REGULATING_COND_EQ:
            return createRegulatingCondEq();
        case CimPackage.TRANSFORMER_TANK_END:
            return createTransformerTankEnd();
        case CimPackage.EMISSION_ACCOUNT:
            return createEmissionAccount();
        case CimPackage.REMOTE_POINT:
            return createRemotePoint();
        case CimPackage.ACCUMULATOR_RESET:
            return createAccumulatorReset();
        case CimPackage.PER_LENGTH_SEQUENCE_IMPEDANCE:
            return createPerLengthSequenceImpedance();
        case CimPackage.MUTUAL_COUPLING:
            return createMutualCoupling();
        case CimPackage.STEAM_TURBINE:
            return createSteamTurbine();
        case CimPackage.IRREGULAR_TIME_POINT:
            return createIrregularTimePoint();
        case CimPackage.PHASE_TAP_CHANGER:
            return createPhaseTapChanger();
        case CimPackage.COMMUNICATION_LINK:
            return createCommunicationLink();
        case CimPackage.ENERGY_SCHEDULING_TYPE:
            return createEnergySchedulingType();
        case CimPackage.LINEAR_SHUNT_COMPENSATOR:
            return createLinearShuntCompensator();
        case CimPackage.LOAD_BREAK_SWITCH:
            return createLoadBreakSwitch();
        case CimPackage.TOPOLOGICAL_NODE:
            return createTopologicalNode();
        case CimPackage.BASE_FREQUENCY:
            return createBaseFrequency();
        case CimPackage.GROUNDING_IMPEDANCE:
            return createGroundingImpedance();
        case CimPackage.NON_CONFORM_LOAD_SCHEDULE:
            return createNonConformLoadSchedule();
        case CimPackage.CONFORM_LOAD_SCHEDULE:
            return createConformLoadSchedule();
        case CimPackage.STRING_MEASUREMENT_VALUE:
            return createStringMeasurementValue();
        case CimPackage.NON_CONFORM_LOAD_GROUP:
            return createNonConformLoadGroup();
        case CimPackage.CONTROL:
            return createControl();
        case CimPackage.FUSE:
            return createFuse();
        case CimPackage.SHUTDOWN_CURVE:
            return createShutdownCurve();
        case CimPackage.CURRENT_TRANSFORMER:
            return createCurrentTransformer();
        case CimPackage.VS_CAPABILITY_CURVE:
            return createVsCapabilityCurve();
        case CimPackage.ACCUMULATOR_LIMIT:
            return createAccumulatorLimit();
        case CimPackage.START_IGN_FUEL_CURVE:
            return createStartIgnFuelCurve();
        case CimPackage.NONLINEAR_SHUNT_COMPENSATOR:
            return createNonlinearShuntCompensator();
        case CimPackage.DIAGRAM_OBJECT_GLUE_POINT:
            return createDiagramObjectGluePoint();
        case CimPackage.EQUIVALENT_SHUNT:
            return createEquivalentShunt();
        case CimPackage.SUBSTATION:
            return createSubstation();
        case CimPackage.HYDRO_PUMP_OP_SCHEDULE:
            return createHydroPumpOpSchedule();
        case CimPackage.LEVEL_VS_VOLUME_CURVE:
            return createLevelVsVolumeCurve();
        case CimPackage.SEASON:
            return createSeason();
        case CimPackage.TRANSFORMER_CORE_ADMITTANCE:
            return createTransformerCoreAdmittance();
        case CimPackage.VALUE_TO_ALIAS:
            return createValueToAlias();
        case CimPackage.ACDC_CONVERTER_DC_TERMINAL:
            return createACDCConverterDCTerminal();
        case CimPackage.REPORTING_GROUP:
            return createReportingGroup();
        case CimPackage.STATIC_VAR_COMPENSATOR:
            return createStaticVarCompensator();
        case CimPackage.DC_SWITCH:
            return createDCSwitch();
        case CimPackage.LINEAR_SHUNT_COMPENSATOR_PHASE:
            return createLinearShuntCompensatorPhase();
        case CimPackage.AC_LINE_SEGMENT:
            return createACLineSegment();
        case CimPackage.TRANSFORMER_STAR_IMPEDANCE:
            return createTransformerStarImpedance();
        case CimPackage.BWR_STEAM_SUPPLY:
            return createBWRSteamSupply();
        case CimPackage.GROUND:
            return createGround();
        case CimPackage.SHUNT_COMPENSATOR_PHASE:
            return createShuntCompensatorPhase();
        case CimPackage.DAY_TYPE:
            return createDayType();
        case CimPackage.HEAT_INPUT_CURVE:
            return createHeatInputCurve();
        case CimPackage.POWER_TRANSFORMER_END:
            return createPowerTransformerEnd();
        case CimPackage.REMOTE_CONTROL:
            return createRemoteControl();
        case CimPackage.DC_LINE_SEGMENT:
            return createDCLineSegment();
        case CimPackage.DRUM_BOILER:
            return createDrumBoiler();
        case CimPackage.SWITCH_PHASE:
            return createSwitchPhase();
        case CimPackage.TOPOLOGICAL_ISLAND:
            return createTopologicalIsland();
        case CimPackage.IO_POINT:
            return createIOPoint();
        case CimPackage.ACDC_TERMINAL:
            return createACDCTerminal();
        case CimPackage.PER_LENGTH_LINE_PARAMETER:
            return createPerLengthLineParameter();
        case CimPackage.ACCUMULATOR:
            return createAccumulator();
        case CimPackage.GEN_UNIT_OP_SCHEDULE:
            return createGenUnitOpSchedule();
        case CimPackage.DC_TERMINAL:
            return createDCTerminal();
        case CimPackage.RESERVOIR:
            return createReservoir();
        case CimPackage.PLANT:
            return createPlant();
        case CimPackage.DC_TOPOLOGICAL_NODE:
            return createDCTopologicalNode();
        case CimPackage.IDENTIFIED_OBJECT:
            return createIdentifiedObject();
        case CimPackage.SV_STATUS:
            return createSvStatus();
        case CimPackage.NUCLEAR_GENERATING_UNIT:
            return createNuclearGeneratingUnit();
        case CimPackage.POWER_ELECTRONICS_WIND_UNIT:
            return createPowerElectronicsWindUnit();
        case CimPackage.AUXILIARY_EQUIPMENT:
            return createAuxiliaryEquipment();
        case CimPackage.SV_POWER_FLOW:
            return createSvPowerFlow();
        case CimPackage.WIND_GENERATING_UNIT:
            return createWindGeneratingUnit();
        case CimPackage.DISCRETE_VALUE:
            return createDiscreteValue();
        case CimPackage.DC_BUSBAR:
            return createDCBusbar();
        case CimPackage.CONDUCTOR:
            return createConductor();
        case CimPackage.GEN_UNIT_OP_COST_CURVE:
            return createGenUnitOpCostCurve();
        case CimPackage.OPERATIONAL_LIMIT_SET:
            return createOperationalLimitSet();
        case CimPackage.DC_CONVERTER_UNIT:
            return createDCConverterUnit();
        case CimPackage.PHASE_TAP_CHANGER_LINEAR:
            return createPhaseTapChangerLinear();
        case CimPackage.DC_BASE_TERMINAL:
            return createDCBaseTerminal();
        case CimPackage.PETERSEN_COIL:
            return createPetersenCoil();
        case CimPackage.LOAD_AREA:
            return createLoadArea();
        case CimPackage.DC_BREAKER:
            return createDCBreaker();
        case CimPackage.AC_LINE_SEGMENT_PHASE:
            return createACLineSegmentPhase();
        case CimPackage.DC_GROUND:
            return createDCGround();
        case CimPackage.POWER_ELECTRONICS_CONNECTION:
            return createPowerElectronicsConnection();
        case CimPackage.PROTECTION_EQUIPMENT:
            return createProtectionEquipment();
        case CimPackage.ANALOG_CONTROL:
            return createAnalogControl();
        case CimPackage.THERMAL_GENERATING_UNIT:
            return createThermalGeneratingUnit();
        case CimPackage.NON_CONFORM_LOAD:
            return createNonConformLoad();
        case CimPackage.TAP_CHANGER_CONTROL:
            return createTapChangerControl();
        case CimPackage.WAVE_TRAP:
            return createWaveTrap();
        case CimPackage.ENERGY_CONSUMER:
            return createEnergyConsumer();
        case CimPackage.SUB_GEOGRAPHICAL_REGION:
            return createSubGeographicalRegion();
        case CimPackage.SUB_LOAD_AREA:
            return createSubLoadArea();
        case CimPackage.EQUIPMENT_FAULT:
            return createEquipmentFault();
        case CimPackage.GROSS_TO_NET_ACTIVE_POWER_CURVE:
            return createGrossToNetActivePowerCurve();
        case CimPackage.ACCUMULATOR_VALUE:
            return createAccumulatorValue();
        case CimPackage.HEAT_RECOVERY_BOILER:
            return createHeatRecoveryBoiler();
        case CimPackage.VALUE_ALIAS_SET:
            return createValueAliasSet();
        case CimPackage.EARTH_FAULT_COMPENSATOR:
            return createEarthFaultCompensator();
        case CimPackage.PHOTO_VOLTAIC_UNIT:
            return createPhotoVoltaicUnit();
        case CimPackage.ALT_TIE_MEAS:
            return createAltTieMeas();
        case CimPackage.CONDUCTING_EQUIPMENT:
            return createConductingEquipment();
        case CimPackage.BRANCH_GROUP_TERMINAL:
            return createBranchGroupTerminal();
        case CimPackage.ANALOG_LIMIT:
            return createAnalogLimit();
        case CimPackage.TIE_FLOW:
            return createTieFlow();
        case CimPackage.DC_EQUIPMENT_CONTAINER:
            return createDCEquipmentContainer();
        case CimPackage.DIAGRAM_OBJECT_STYLE:
            return createDiagramObjectStyle();
        case CimPackage.NAME_TYPE:
            return createNameType();
        case CimPackage.CONTROL_AREA:
            return createControlArea();
        case CimPackage.BUS_NAME_MARKER:
            return createBusNameMarker();
        case CimPackage.LOAD_RESPONSE_CHARACTERISTIC:
            return createLoadResponseCharacteristic();
        case CimPackage.FAULT_INDICATOR:
            return createFaultIndicator();
        case CimPackage.ACDC_CONVERTER:
            return createACDCConverter();
        case CimPackage.ENERGY_SOURCE:
            return createEnergySource();
        case CimPackage.DC_CONDUCTING_EQUIPMENT:
            return createDCConductingEquipment();
        case CimPackage.CS_CONVERTER:
            return createCsConverter();
        case CimPackage.TERMINAL:
            return createTerminal();
        case CimPackage.REGULATING_CONTROL:
            return createRegulatingControl();
        case CimPackage.INFLOW_FORECAST:
            return createInflowForecast();
        case CimPackage.TRANSFORMER_END:
            return createTransformerEnd();
        case CimPackage.ENERGY_CONSUMER_PHASE:
            return createEnergyConsumerPhase();
        case CimPackage.BRANCH_GROUP:
            return createBranchGroup();
        case CimPackage.TRANSFORMER_TANK:
            return createTransformerTank();
        case CimPackage.CONTINGENCY_EQUIPMENT:
            return createContingencyEquipment();
        case CimPackage.OPERATIONAL_LIMIT:
            return createOperationalLimit();
        case CimPackage.CURVE:
            return createCurve();
        case CimPackage.IEC61970CIM_VERSION:
            return createIEC61970CIMVersion();
        case CimPackage.DC_NODE:
            return createDCNode();
        case CimPackage.TAP_CHANGER:
            return createTapChanger();
        case CimPackage.MEASUREMENT_VALUE:
            return createMeasurementValue();
        case CimPackage.LOAD_GROUP:
            return createLoadGroup();
        case CimPackage.RECLOSER:
            return createRecloser();
        case CimPackage.NAME_TYPE_AUTHORITY:
            return createNameTypeAuthority();
        case CimPackage.SURGE_ARRESTER:
            return createSurgeArrester();
        case CimPackage.SOLAR_GENERATING_UNIT:
            return createSolarGeneratingUnit();
        case CimPackage.EQUIVALENT_INJECTION:
            return createEquivalentInjection();
        case CimPackage.GROUND_DISCONNECTOR:
            return createGroundDisconnector();
        case CimPackage.REGULATION_SCHEDULE:
            return createRegulationSchedule();
        case CimPackage.CLAMP:
            return createClamp();
        case CimPackage.PRIME_MOVER:
            return createPrimeMover();
        case CimPackage.CONTINGENCY:
            return createContingency();
        case CimPackage.ENERGY_CONNECTION:
            return createEnergyConnection();
        case CimPackage.VOLTAGE_CONTROL_ZONE:
            return createVoltageControlZone();
        case CimPackage.DIAGRAM_OBJECT:
            return createDiagramObject();
        case CimPackage.LIMIT:
            return createLimit();
        case CimPackage.SWITCH:
            return createSwitch();
        case CimPackage.FOSSIL_FUEL:
            return createFossilFuel();
        case CimPackage.HYDRO_PUMP:
            return createHydroPump();
        case CimPackage.PHASE_TAP_CHANGER_NON_LINEAR:
            return createPhaseTapChangerNonLinear();
        case CimPackage.POWER_TRANSFORMER:
            return createPowerTransformer();
        case CimPackage.SENSOR:
            return createSensor();
        case CimPackage.DC_LINE:
            return createDCLine();
        case CimPackage.FOSSIL_STEAM_SUPPLY:
            return createFossilSteamSupply();
        case CimPackage.FLOW_SENSOR:
            return createFlowSensor();
        default:
            throw new IllegalArgumentException( "The class '" + eClass.getName() + "' is not a valid classifier" );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object createFromString( EDataType eDataType, String initialValue ) {
        switch( eDataType.getClassifierID() ) {
        case CimPackage.SINGLE_PHASE_KIND:
            return createSinglePhaseKindFromString( eDataType, initialValue );
        case CimPackage.BATTERY_STATE_KIND:
            return createBatteryStateKindFromString( eDataType, initialValue );
        case CimPackage.BUSBAR_CONFIGURATION:
            return createBusbarConfigurationFromString( eDataType, initialValue );
        case CimPackage.ORIENTATION_KIND:
            return createOrientationKindFromString( eDataType, initialValue );
        case CimPackage.GENERATOR_CONTROL_SOURCE:
            return createGeneratorControlSourceFromString( eDataType, initialValue );
        case CimPackage.EMISSION_TYPE:
            return createEmissionTypeFromString( eDataType, initialValue );
        case CimPackage.VS_QPCC_CONTROL_KIND:
            return createVsQpccControlKindFromString( eDataType, initialValue );
        case CimPackage.HYDRO_ENERGY_CONVERSION_KIND:
            return createHydroEnergyConversionKindFromString( eDataType, initialValue );
        case CimPackage.HYDRO_TURBINE_KIND:
            return createHydroTurbineKindFromString( eDataType, initialValue );
        case CimPackage.UNIT_MULTIPLIER:
            return createUnitMultiplierFromString( eDataType, initialValue );
        case CimPackage.CS_PPCC_CONTROL_KIND:
            return createCsPpccControlKindFromString( eDataType, initialValue );
        case CimPackage.ASYNCHRONOUS_MACHINE_KIND:
            return createAsynchronousMachineKindFromString( eDataType, initialValue );
        case CimPackage.PHASE_SHUNT_CONNECTION_KIND:
            return createPhaseShuntConnectionKindFromString( eDataType, initialValue );
        case CimPackage.REMOTE_UNIT_TYPE:
            return createRemoteUnitTypeFromString( eDataType, initialValue );
        case CimPackage.POTENTIAL_TRANSFORMER_KIND:
            return createPotentialTransformerKindFromString( eDataType, initialValue );
        case CimPackage.COOLANT_TYPE:
            return createCoolantTypeFromString( eDataType, initialValue );
        case CimPackage.SYNCHRONOUS_MACHINE_OPERATING_MODE:
            return createSynchronousMachineOperatingModeFromString( eDataType, initialValue );
        case CimPackage.CURRENCY:
            return createCurrencyFromString( eDataType, initialValue );
        case CimPackage.SVC_CONTROL_MODE:
            return createSVCControlModeFromString( eDataType, initialValue );
        case CimPackage.PETERSEN_COIL_MODE_KIND:
            return createPetersenCoilModeKindFromString( eDataType, initialValue );
        case CimPackage.CURVE_STYLE:
            return createCurveStyleFromString( eDataType, initialValue );
        case CimPackage.EMISSION_VALUE_SOURCE:
            return createEmissionValueSourceFromString( eDataType, initialValue );
        case CimPackage.VS_PPCC_CONTROL_KIND:
            return createVsPpccControlKindFromString( eDataType, initialValue );
        case CimPackage.HYDRO_PLANT_STORAGE_KIND:
            return createHydroPlantStorageKindFromString( eDataType, initialValue );
        case CimPackage.CONTINGENCY_EQUIPMENT_STATUS_KIND:
            return createContingencyEquipmentStatusKindFromString( eDataType, initialValue );
        case CimPackage.GENERATOR_CONTROL_MODE:
            return createGeneratorControlModeFromString( eDataType, initialValue );
        case CimPackage.WINDING_CONNECTION:
            return createWindingConnectionFromString( eDataType, initialValue );
        case CimPackage.PHASE_CONNECTED_FAULT_KIND:
            return createPhaseConnectedFaultKindFromString( eDataType, initialValue );
        case CimPackage.SHORT_CIRCUIT_ROTOR_KIND:
            return createShortCircuitRotorKindFromString( eDataType, initialValue );
        case CimPackage.DC_POLARITY_KIND:
            return createDCPolarityKindFromString( eDataType, initialValue );
        case CimPackage.CS_OPERATING_MODE_KIND:
            return createCsOperatingModeKindFromString( eDataType, initialValue );
        case CimPackage.DC_CONVERTER_OPERATING_MODE_KIND:
            return createDCConverterOperatingModeKindFromString( eDataType, initialValue );
        case CimPackage.PHASE_CODE:
            return createPhaseCodeFromString( eDataType, initialValue );
        case CimPackage.SOURCE:
            return createSourceFromString( eDataType, initialValue );
        case CimPackage.SYNCHRONOUS_MACHINE_KIND:
            return createSynchronousMachineKindFromString( eDataType, initialValue );
        case CimPackage.OPERATIONAL_LIMIT_DIRECTION_KIND:
            return createOperationalLimitDirectionKindFromString( eDataType, initialValue );
        case CimPackage.FUEL_TYPE:
            return createFuelTypeFromString( eDataType, initialValue );
        case CimPackage.CONTROL_AREA_TYPE_KIND:
            return createControlAreaTypeKindFromString( eDataType, initialValue );
        case CimPackage.BREAKER_CONFIGURATION:
            return createBreakerConfigurationFromString( eDataType, initialValue );
        case CimPackage.TRANSFORMER_CONTROL_MODE:
            return createTransformerControlModeFromString( eDataType, initialValue );
        case CimPackage.BOILER_CONTROL_MODE:
            return createBoilerControlModeFromString( eDataType, initialValue );
        case CimPackage.UNIT_SYMBOL:
            return createUnitSymbolFromString( eDataType, initialValue );
        case CimPackage.REGULATING_CONTROL_MODE_KIND:
            return createRegulatingControlModeKindFromString( eDataType, initialValue );
        case CimPackage.WIND_GEN_UNIT_KIND:
            return createWindGenUnitKindFromString( eDataType, initialValue );
        case CimPackage.VALIDITY:
            return createValidityFromString( eDataType, initialValue );
        default:
            throw new IllegalArgumentException(
                    "The datatype '" + eDataType.getName() + "' is not a valid classifier" );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public String convertToString( EDataType eDataType, Object instanceValue ) {
        switch( eDataType.getClassifierID() ) {
        case CimPackage.SINGLE_PHASE_KIND:
            return convertSinglePhaseKindToString( eDataType, instanceValue );
        case CimPackage.BATTERY_STATE_KIND:
            return convertBatteryStateKindToString( eDataType, instanceValue );
        case CimPackage.BUSBAR_CONFIGURATION:
            return convertBusbarConfigurationToString( eDataType, instanceValue );
        case CimPackage.ORIENTATION_KIND:
            return convertOrientationKindToString( eDataType, instanceValue );
        case CimPackage.GENERATOR_CONTROL_SOURCE:
            return convertGeneratorControlSourceToString( eDataType, instanceValue );
        case CimPackage.EMISSION_TYPE:
            return convertEmissionTypeToString( eDataType, instanceValue );
        case CimPackage.VS_QPCC_CONTROL_KIND:
            return convertVsQpccControlKindToString( eDataType, instanceValue );
        case CimPackage.HYDRO_ENERGY_CONVERSION_KIND:
            return convertHydroEnergyConversionKindToString( eDataType, instanceValue );
        case CimPackage.HYDRO_TURBINE_KIND:
            return convertHydroTurbineKindToString( eDataType, instanceValue );
        case CimPackage.UNIT_MULTIPLIER:
            return convertUnitMultiplierToString( eDataType, instanceValue );
        case CimPackage.CS_PPCC_CONTROL_KIND:
            return convertCsPpccControlKindToString( eDataType, instanceValue );
        case CimPackage.ASYNCHRONOUS_MACHINE_KIND:
            return convertAsynchronousMachineKindToString( eDataType, instanceValue );
        case CimPackage.PHASE_SHUNT_CONNECTION_KIND:
            return convertPhaseShuntConnectionKindToString( eDataType, instanceValue );
        case CimPackage.REMOTE_UNIT_TYPE:
            return convertRemoteUnitTypeToString( eDataType, instanceValue );
        case CimPackage.POTENTIAL_TRANSFORMER_KIND:
            return convertPotentialTransformerKindToString( eDataType, instanceValue );
        case CimPackage.COOLANT_TYPE:
            return convertCoolantTypeToString( eDataType, instanceValue );
        case CimPackage.SYNCHRONOUS_MACHINE_OPERATING_MODE:
            return convertSynchronousMachineOperatingModeToString( eDataType, instanceValue );
        case CimPackage.CURRENCY:
            return convertCurrencyToString( eDataType, instanceValue );
        case CimPackage.SVC_CONTROL_MODE:
            return convertSVCControlModeToString( eDataType, instanceValue );
        case CimPackage.PETERSEN_COIL_MODE_KIND:
            return convertPetersenCoilModeKindToString( eDataType, instanceValue );
        case CimPackage.CURVE_STYLE:
            return convertCurveStyleToString( eDataType, instanceValue );
        case CimPackage.EMISSION_VALUE_SOURCE:
            return convertEmissionValueSourceToString( eDataType, instanceValue );
        case CimPackage.VS_PPCC_CONTROL_KIND:
            return convertVsPpccControlKindToString( eDataType, instanceValue );
        case CimPackage.HYDRO_PLANT_STORAGE_KIND:
            return convertHydroPlantStorageKindToString( eDataType, instanceValue );
        case CimPackage.CONTINGENCY_EQUIPMENT_STATUS_KIND:
            return convertContingencyEquipmentStatusKindToString( eDataType, instanceValue );
        case CimPackage.GENERATOR_CONTROL_MODE:
            return convertGeneratorControlModeToString( eDataType, instanceValue );
        case CimPackage.WINDING_CONNECTION:
            return convertWindingConnectionToString( eDataType, instanceValue );
        case CimPackage.PHASE_CONNECTED_FAULT_KIND:
            return convertPhaseConnectedFaultKindToString( eDataType, instanceValue );
        case CimPackage.SHORT_CIRCUIT_ROTOR_KIND:
            return convertShortCircuitRotorKindToString( eDataType, instanceValue );
        case CimPackage.DC_POLARITY_KIND:
            return convertDCPolarityKindToString( eDataType, instanceValue );
        case CimPackage.CS_OPERATING_MODE_KIND:
            return convertCsOperatingModeKindToString( eDataType, instanceValue );
        case CimPackage.DC_CONVERTER_OPERATING_MODE_KIND:
            return convertDCConverterOperatingModeKindToString( eDataType, instanceValue );
        case CimPackage.PHASE_CODE:
            return convertPhaseCodeToString( eDataType, instanceValue );
        case CimPackage.SOURCE:
            return convertSourceToString( eDataType, instanceValue );
        case CimPackage.SYNCHRONOUS_MACHINE_KIND:
            return convertSynchronousMachineKindToString( eDataType, instanceValue );
        case CimPackage.OPERATIONAL_LIMIT_DIRECTION_KIND:
            return convertOperationalLimitDirectionKindToString( eDataType, instanceValue );
        case CimPackage.FUEL_TYPE:
            return convertFuelTypeToString( eDataType, instanceValue );
        case CimPackage.CONTROL_AREA_TYPE_KIND:
            return convertControlAreaTypeKindToString( eDataType, instanceValue );
        case CimPackage.BREAKER_CONFIGURATION:
            return convertBreakerConfigurationToString( eDataType, instanceValue );
        case CimPackage.TRANSFORMER_CONTROL_MODE:
            return convertTransformerControlModeToString( eDataType, instanceValue );
        case CimPackage.BOILER_CONTROL_MODE:
            return convertBoilerControlModeToString( eDataType, instanceValue );
        case CimPackage.UNIT_SYMBOL:
            return convertUnitSymbolToString( eDataType, instanceValue );
        case CimPackage.REGULATING_CONTROL_MODE_KIND:
            return convertRegulatingControlModeKindToString( eDataType, instanceValue );
        case CimPackage.WIND_GEN_UNIT_KIND:
            return convertWindGenUnitKindToString( eDataType, instanceValue );
        case CimPackage.VALIDITY:
            return convertValidityToString( eDataType, instanceValue );
        default:
            throw new IllegalArgumentException(
                    "The datatype '" + eDataType.getName() + "' is not a valid classifier" );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DateInterval createDateInterval() {
        DateIntervalImpl dateInterval = new DateIntervalImpl();
        return dateInterval;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public MonthDayInterval createMonthDayInterval() {
        MonthDayIntervalImpl monthDayInterval = new MonthDayIntervalImpl();
        return monthDayInterval;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public StringQuantity createStringQuantity() {
        StringQuantityImpl stringQuantity = new StringQuantityImpl();
        return stringQuantity;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TimeInterval createTimeInterval() {
        TimeIntervalImpl timeInterval = new TimeIntervalImpl();
        return timeInterval;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public FaultImpedance createFaultImpedance() {
        FaultImpedanceImpl faultImpedance = new FaultImpedanceImpl();
        return faultImpedance;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DecimalQuantity createDecimalQuantity() {
        DecimalQuantityImpl decimalQuantity = new DecimalQuantityImpl();
        return decimalQuantity;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DateTimeInterval createDateTimeInterval() {
        DateTimeIntervalImpl dateTimeInterval = new DateTimeIntervalImpl();
        return dateTimeInterval;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public FloatQuantity createFloatQuantity() {
        FloatQuantityImpl floatQuantity = new FloatQuantityImpl();
        return floatQuantity;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IntegerQuantity createIntegerQuantity() {
        IntegerQuantityImpl integerQuantity = new IntegerQuantityImpl();
        return integerQuantity;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CompositeSwitch createCompositeSwitch() {
        CompositeSwitchImpl compositeSwitch = new CompositeSwitchImpl();
        return compositeSwitch;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DCLine createDCLine() {
        DCLineImpl dcLine = new DCLineImpl();
        return dcLine;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PenstockLossCurve createPenstockLossCurve() {
        PenstockLossCurveImpl penstockLossCurve = new PenstockLossCurveImpl();
        return penstockLossCurve;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IEC61970CIMVersion createIEC61970CIMVersion() {
        IEC61970CIMVersionImpl iec61970CIMVersion = new IEC61970CIMVersionImpl();
        return iec61970CIMVersion;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DCBusbar createDCBusbar() {
        DCBusbarImpl dcBusbar = new DCBusbarImpl();
        return dcBusbar;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public RegulatingCondEq createRegulatingCondEq() {
        RegulatingCondEqImpl regulatingCondEq = new RegulatingCondEqImpl();
        return regulatingCondEq;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DCDisconnector createDCDisconnector() {
        DCDisconnectorImpl dcDisconnector = new DCDisconnectorImpl();
        return dcDisconnector;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ReactiveCapabilityCurve createReactiveCapabilityCurve() {
        ReactiveCapabilityCurveImpl reactiveCapabilityCurve = new ReactiveCapabilityCurveImpl();
        return reactiveCapabilityCurve;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Recloser createRecloser() {
        RecloserImpl recloser = new RecloserImpl();
        return recloser;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public AuxiliaryEquipment createAuxiliaryEquipment() {
        AuxiliaryEquipmentImpl auxiliaryEquipment = new AuxiliaryEquipmentImpl();
        return auxiliaryEquipment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Discrete createDiscrete() {
        DiscreteImpl discrete = new DiscreteImpl();
        return discrete;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public RotatingMachine createRotatingMachine() {
        RotatingMachineImpl rotatingMachine = new RotatingMachineImpl();
        return rotatingMachine;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NonlinearShuntCompensatorPhasePoint createNonlinearShuntCompensatorPhasePoint() {
        NonlinearShuntCompensatorPhasePointImpl nonlinearShuntCompensatorPhasePoint = new NonlinearShuntCompensatorPhasePointImpl();
        return nonlinearShuntCompensatorPhasePoint;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public OperatingShare createOperatingShare() {
        OperatingShareImpl operatingShare = new OperatingShareImpl();
        return operatingShare;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public FrequencyConverter createFrequencyConverter() {
        FrequencyConverterImpl frequencyConverter = new FrequencyConverterImpl();
        return frequencyConverter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public AnalogControl createAnalogControl() {
        AnalogControlImpl analogControl = new AnalogControlImpl();
        return analogControl;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Switch createSwitch() {
        SwitchImpl switch_ = new SwitchImpl();
        return switch_;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public InflowForecast createInflowForecast() {
        InflowForecastImpl inflowForecast = new InflowForecastImpl();
        return inflowForecast;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ProtectedSwitch createProtectedSwitch() {
        ProtectedSwitchImpl protectedSwitch = new ProtectedSwitchImpl();
        return protectedSwitch;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EmissionAccount createEmissionAccount() {
        EmissionAccountImpl emissionAccount = new EmissionAccountImpl();
        return emissionAccount;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public LoadBreakSwitch createLoadBreakSwitch() {
        LoadBreakSwitchImpl loadBreakSwitch = new LoadBreakSwitchImpl();
        return loadBreakSwitch;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CurrentRelay createCurrentRelay() {
        CurrentRelayImpl currentRelay = new CurrentRelayImpl();
        return currentRelay;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public RemoteControl createRemoteControl() {
        RemoteControlImpl remoteControl = new RemoteControlImpl();
        return remoteControl;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SteamSendoutSchedule createSteamSendoutSchedule() {
        SteamSendoutScheduleImpl steamSendoutSchedule = new SteamSendoutScheduleImpl();
        return steamSendoutSchedule;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public StartIgnFuelCurve createStartIgnFuelCurve() {
        StartIgnFuelCurveImpl startIgnFuelCurve = new StartIgnFuelCurveImpl();
        return startIgnFuelCurve;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public StaticVarCompensator createStaticVarCompensator() {
        StaticVarCompensatorImpl staticVarCompensator = new StaticVarCompensatorImpl();
        return staticVarCompensator;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DCSwitch createDCSwitch() {
        DCSwitchImpl dcSwitch = new DCSwitchImpl();
        return dcSwitch;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public BWRSteamSupply createBWRSteamSupply() {
        BWRSteamSupplyImpl bwrSteamSupply = new BWRSteamSupplyImpl();
        return bwrSteamSupply;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Control createControl() {
        ControlImpl control = new ControlImpl();
        return control;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DayType createDayType() {
        DayTypeImpl dayType = new DayTypeImpl();
        return dayType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ConnectivityNode createConnectivityNode() {
        ConnectivityNodeImpl connectivityNode = new ConnectivityNodeImpl();
        return connectivityNode;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CombustionTurbine createCombustionTurbine() {
        CombustionTurbineImpl combustionTurbine = new CombustionTurbineImpl();
        return combustionTurbine;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public RatioTapChangerTable createRatioTapChangerTable() {
        RatioTapChangerTableImpl ratioTapChangerTable = new RatioTapChangerTableImpl();
        return ratioTapChangerTable;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ValueToAlias createValueToAlias() {
        ValueToAliasImpl valueToAlias = new ValueToAliasImpl();
        return valueToAlias;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public GenUnitOpCostCurve createGenUnitOpCostCurve() {
        GenUnitOpCostCurveImpl genUnitOpCostCurve = new GenUnitOpCostCurveImpl();
        return genUnitOpCostCurve;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PhaseTapChangerNonLinear createPhaseTapChangerNonLinear() {
        PhaseTapChangerNonLinearImpl phaseTapChangerNonLinear = new PhaseTapChangerNonLinearImpl();
        return phaseTapChangerNonLinear;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NameTypeAuthority createNameTypeAuthority() {
        NameTypeAuthorityImpl nameTypeAuthority = new NameTypeAuthorityImpl();
        return nameTypeAuthority;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ACDCTerminal createACDCTerminal() {
        ACDCTerminalImpl acdcTerminal = new ACDCTerminalImpl();
        return acdcTerminal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TextDiagramObject createTextDiagramObject() {
        TextDiagramObjectImpl textDiagramObject = new TextDiagramObjectImpl();
        return textDiagramObject;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NuclearGeneratingUnit createNuclearGeneratingUnit() {
        NuclearGeneratingUnitImpl nuclearGeneratingUnit = new NuclearGeneratingUnitImpl();
        return nuclearGeneratingUnit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Clamp createClamp() {
        ClampImpl clamp = new ClampImpl();
        return clamp;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ApparentPowerLimit createApparentPowerLimit() {
        ApparentPowerLimitImpl apparentPowerLimit = new ApparentPowerLimitImpl();
        return apparentPowerLimit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public StringMeasurementValue createStringMeasurementValue() {
        StringMeasurementValueImpl stringMeasurementValue = new StringMeasurementValueImpl();
        return stringMeasurementValue;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EquipmentContainer createEquipmentContainer() {
        EquipmentContainerImpl equipmentContainer = new EquipmentContainerImpl();
        return equipmentContainer;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Subcritical createSubcritical() {
        SubcriticalImpl subcritical = new SubcriticalImpl();
        return subcritical;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TapChangerControl createTapChangerControl() {
        TapChangerControlImpl tapChangerControl = new TapChangerControlImpl();
        return tapChangerControl;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public HydroPump createHydroPump() {
        HydroPumpImpl hydroPump = new HydroPumpImpl();
        return hydroPump;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public AsynchronousMachine createAsynchronousMachine() {
        AsynchronousMachineImpl asynchronousMachine = new AsynchronousMachineImpl();
        return asynchronousMachine;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EnergyConsumerPhase createEnergyConsumerPhase() {
        EnergyConsumerPhaseImpl energyConsumerPhase = new EnergyConsumerPhaseImpl();
        return energyConsumerPhase;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ContingencyElement createContingencyElement() {
        ContingencyElementImpl contingencyElement = new ContingencyElementImpl();
        return contingencyElement;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PowerElectronicsConnectionPhase createPowerElectronicsConnectionPhase() {
        PowerElectronicsConnectionPhaseImpl powerElectronicsConnectionPhase = new PowerElectronicsConnectionPhaseImpl();
        return powerElectronicsConnectionPhase;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SeriesCompensator createSeriesCompensator() {
        SeriesCompensatorImpl seriesCompensator = new SeriesCompensatorImpl();
        return seriesCompensator;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DiagramObjectPoint createDiagramObjectPoint() {
        DiagramObjectPointImpl diagramObjectPoint = new DiagramObjectPointImpl();
        return diagramObjectPoint;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EquivalentEquipment createEquivalentEquipment() {
        EquivalentEquipmentImpl equivalentEquipment = new EquivalentEquipmentImpl();
        return equivalentEquipment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EquivalentShunt createEquivalentShunt() {
        EquivalentShuntImpl equivalentShunt = new EquivalentShuntImpl();
        return equivalentShunt;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EnergyConsumer createEnergyConsumer() {
        EnergyConsumerImpl energyConsumer = new EnergyConsumerImpl();
        return energyConsumer;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public OperationalLimitSet createOperationalLimitSet() {
        OperationalLimitSetImpl operationalLimitSet = new OperationalLimitSetImpl();
        return operationalLimitSet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PowerCutZone createPowerCutZone() {
        PowerCutZoneImpl powerCutZone = new PowerCutZoneImpl();
        return powerCutZone;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Curve createCurve() {
        CurveImpl curve = new CurveImpl();
        return curve;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NonConformLoadSchedule createNonConformLoadSchedule() {
        NonConformLoadScheduleImpl nonConformLoadSchedule = new NonConformLoadScheduleImpl();
        return nonConformLoadSchedule;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TransformerEnd createTransformerEnd() {
        TransformerEndImpl transformerEnd = new TransformerEndImpl();
        return transformerEnd;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TargetLevelSchedule createTargetLevelSchedule() {
        TargetLevelScheduleImpl targetLevelSchedule = new TargetLevelScheduleImpl();
        return targetLevelSchedule;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public VoltageLimit createVoltageLimit() {
        VoltageLimitImpl voltageLimit = new VoltageLimitImpl();
        return voltageLimit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public OperatingParticipant createOperatingParticipant() {
        OperatingParticipantImpl operatingParticipant = new OperatingParticipantImpl();
        return operatingParticipant;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CurrentLimit createCurrentLimit() {
        CurrentLimitImpl currentLimit = new CurrentLimitImpl();
        return currentLimit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public AltGeneratingUnitMeas createAltGeneratingUnitMeas() {
        AltGeneratingUnitMeasImpl altGeneratingUnitMeas = new AltGeneratingUnitMeasImpl();
        return altGeneratingUnitMeas;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PrimeMover createPrimeMover() {
        PrimeMoverImpl primeMover = new PrimeMoverImpl();
        return primeMover;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PhaseTapChangerTabular createPhaseTapChangerTabular() {
        PhaseTapChangerTabularImpl phaseTapChangerTabular = new PhaseTapChangerTabularImpl();
        return phaseTapChangerTabular;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TopologicalNode createTopologicalNode() {
        TopologicalNodeImpl topologicalNode = new TopologicalNodeImpl();
        return topologicalNode;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public StringMeasurement createStringMeasurement() {
        StringMeasurementImpl stringMeasurement = new StringMeasurementImpl();
        return stringMeasurement;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EnergyArea createEnergyArea() {
        EnergyAreaImpl energyArea = new EnergyAreaImpl();
        return energyArea;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CogenerationPlant createCogenerationPlant() {
        CogenerationPlantImpl cogenerationPlant = new CogenerationPlantImpl();
        return cogenerationPlant;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public GenUnitOpSchedule createGenUnitOpSchedule() {
        GenUnitOpScheduleImpl genUnitOpSchedule = new GenUnitOpScheduleImpl();
        return genUnitOpSchedule;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NonConformLoadGroup createNonConformLoadGroup() {
        NonConformLoadGroupImpl nonConformLoadGroup = new NonConformLoadGroupImpl();
        return nonConformLoadGroup;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public RaiseLowerCommand createRaiseLowerCommand() {
        RaiseLowerCommandImpl raiseLowerCommand = new RaiseLowerCommandImpl();
        return raiseLowerCommand;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public BasicIntervalSchedule createBasicIntervalSchedule() {
        BasicIntervalScheduleImpl basicIntervalSchedule = new BasicIntervalScheduleImpl();
        return basicIntervalSchedule;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public OperationalLimit createOperationalLimit() {
        OperationalLimitImpl operationalLimit = new OperationalLimitImpl();
        return operationalLimit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public StationSupply createStationSupply() {
        StationSupplyImpl stationSupply = new StationSupplyImpl();
        return stationSupply;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SubGeographicalRegion createSubGeographicalRegion() {
        SubGeographicalRegionImpl subGeographicalRegion = new SubGeographicalRegionImpl();
        return subGeographicalRegion;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public HydroTurbine createHydroTurbine() {
        HydroTurbineImpl hydroTurbine = new HydroTurbineImpl();
        return hydroTurbine;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ACDCConverterDCTerminal createACDCConverterDCTerminal() {
        ACDCConverterDCTerminalImpl acdcConverterDCTerminal = new ACDCConverterDCTerminalImpl();
        return acdcConverterDCTerminal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TransformerTankEnd createTransformerTankEnd() {
        TransformerTankEndImpl transformerTankEnd = new TransformerTankEndImpl();
        return transformerTankEnd;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NameType createNameType() {
        NameTypeImpl nameType = new NameTypeImpl();
        return nameType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public RegulatingControl createRegulatingControl() {
        RegulatingControlImpl regulatingControl = new RegulatingControlImpl();
        return regulatingControl;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ReportingGroup createReportingGroup() {
        ReportingGroupImpl reportingGroup = new ReportingGroupImpl();
        return reportingGroup;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public AltTieMeas createAltTieMeas() {
        AltTieMeasImpl altTieMeas = new AltTieMeasImpl();
        return altTieMeas;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EnergySourcePhase createEnergySourcePhase() {
        EnergySourcePhaseImpl energySourcePhase = new EnergySourcePhaseImpl();
        return energySourcePhase;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CommunicationLink createCommunicationLink() {
        CommunicationLinkImpl communicationLink = new CommunicationLinkImpl();
        return communicationLink;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NonlinearShuntCompensatorPoint createNonlinearShuntCompensatorPoint() {
        NonlinearShuntCompensatorPointImpl nonlinearShuntCompensatorPoint = new NonlinearShuntCompensatorPointImpl();
        return nonlinearShuntCompensatorPoint;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SynchronousMachine createSynchronousMachine() {
        SynchronousMachineImpl synchronousMachine = new SynchronousMachineImpl();
        return synchronousMachine;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public HeatRateCurve createHeatRateCurve() {
        HeatRateCurveImpl heatRateCurve = new HeatRateCurveImpl();
        return heatRateCurve;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public BaseVoltage createBaseVoltage() {
        BaseVoltageImpl baseVoltage = new BaseVoltageImpl();
        return baseVoltage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public VsConverter createVsConverter() {
        VsConverterImpl vsConverter = new VsConverterImpl();
        return vsConverter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EnergyConnection createEnergyConnection() {
        EnergyConnectionImpl energyConnection = new EnergyConnectionImpl();
        return energyConnection;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SwitchPhase createSwitchPhase() {
        SwitchPhaseImpl switchPhase = new SwitchPhaseImpl();
        return switchPhase;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ConformLoadGroup createConformLoadGroup() {
        ConformLoadGroupImpl conformLoadGroup = new ConformLoadGroupImpl();
        return conformLoadGroup;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DiagramObject createDiagramObject() {
        DiagramObjectImpl diagramObject = new DiagramObjectImpl();
        return diagramObject;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public HydroGeneratingUnit createHydroGeneratingUnit() {
        HydroGeneratingUnitImpl hydroGeneratingUnit = new HydroGeneratingUnitImpl();
        return hydroGeneratingUnit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Disconnector createDisconnector() {
        DisconnectorImpl disconnector = new DisconnectorImpl();
        return disconnector;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PerLengthImpedance createPerLengthImpedance() {
        PerLengthImpedanceImpl perLengthImpedance = new PerLengthImpedanceImpl();
        return perLengthImpedance;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Substation createSubstation() {
        SubstationImpl substation = new SubstationImpl();
        return substation;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DiagramObjectStyle createDiagramObjectStyle() {
        DiagramObjectStyleImpl diagramObjectStyle = new DiagramObjectStyleImpl();
        return diagramObjectStyle;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Command createCommand() {
        CommandImpl command = new CommandImpl();
        return command;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ShuntCompensator createShuntCompensator() {
        ShuntCompensatorImpl shuntCompensator = new ShuntCompensatorImpl();
        return shuntCompensator;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ShuntCompensatorPhase createShuntCompensatorPhase() {
        ShuntCompensatorPhaseImpl shuntCompensatorPhase = new ShuntCompensatorPhaseImpl();
        return shuntCompensatorPhase;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public RecloseSequence createRecloseSequence() {
        RecloseSequenceImpl recloseSequence = new RecloseSequenceImpl();
        return recloseSequence;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public VoltageControlZone createVoltageControlZone() {
        VoltageControlZoneImpl voltageControlZone = new VoltageControlZoneImpl();
        return voltageControlZone;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SteamSupply createSteamSupply() {
        SteamSupplyImpl steamSupply = new SteamSupplyImpl();
        return steamSupply;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public MeasurementValueQuality createMeasurementValueQuality() {
        MeasurementValueQualityImpl measurementValueQuality = new MeasurementValueQualityImpl();
        return measurementValueQuality;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EquipmentFault createEquipmentFault() {
        EquipmentFaultImpl equipmentFault = new EquipmentFaultImpl();
        return equipmentFault;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DCEquipmentContainer createDCEquipmentContainer() {
        DCEquipmentContainerImpl dcEquipmentContainer = new DCEquipmentContainerImpl();
        return dcEquipmentContainer;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public HeatRecoveryBoiler createHeatRecoveryBoiler() {
        HeatRecoveryBoilerImpl heatRecoveryBoiler = new HeatRecoveryBoilerImpl();
        return heatRecoveryBoiler;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EnergySchedulingType createEnergySchedulingType() {
        EnergySchedulingTypeImpl energySchedulingType = new EnergySchedulingTypeImpl();
        return energySchedulingType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ContingencyEquipment createContingencyEquipment() {
        ContingencyEquipmentImpl contingencyEquipment = new ContingencyEquipmentImpl();
        return contingencyEquipment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ConformLoadSchedule createConformLoadSchedule() {
        ConformLoadScheduleImpl conformLoadSchedule = new ConformLoadScheduleImpl();
        return conformLoadSchedule;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public RegulationSchedule createRegulationSchedule() {
        RegulationScheduleImpl regulationSchedule = new RegulationScheduleImpl();
        return regulationSchedule;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TailbayLossCurve createTailbayLossCurve() {
        TailbayLossCurveImpl tailbayLossCurve = new TailbayLossCurveImpl();
        return tailbayLossCurve;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IdentifiedObject createIdentifiedObject() {
        IdentifiedObjectImpl identifiedObject = new IdentifiedObjectImpl();
        return identifiedObject;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public GroundingImpedance createGroundingImpedance() {
        GroundingImpedanceImpl groundingImpedance = new GroundingImpedanceImpl();
        return groundingImpedance;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PowerElectronicsConnection createPowerElectronicsConnection() {
        PowerElectronicsConnectionImpl powerElectronicsConnection = new PowerElectronicsConnectionImpl();
        return powerElectronicsConnection;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Ground createGround() {
        GroundImpl ground = new GroundImpl();
        return ground;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Diagram createDiagram() {
        DiagramImpl diagram = new DiagramImpl();
        return diagram;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SvVoltage createSvVoltage() {
        SvVoltageImpl svVoltage = new SvVoltageImpl();
        return svVoltage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TransformerMeshImpedance createTransformerMeshImpedance() {
        TransformerMeshImpedanceImpl transformerMeshImpedance = new TransformerMeshImpedanceImpl();
        return transformerMeshImpedance;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DCNode createDCNode() {
        DCNodeImpl dcNode = new DCNodeImpl();
        return dcNode;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SurgeArrester createSurgeArrester() {
        SurgeArresterImpl surgeArrester = new SurgeArresterImpl();
        return surgeArrester;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IrregularIntervalSchedule createIrregularIntervalSchedule() {
        IrregularIntervalScheduleImpl irregularIntervalSchedule = new IrregularIntervalScheduleImpl();
        return irregularIntervalSchedule;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public RatioTapChangerTablePoint createRatioTapChangerTablePoint() {
        RatioTapChangerTablePointImpl ratioTapChangerTablePoint = new RatioTapChangerTablePointImpl();
        return ratioTapChangerTablePoint;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CsConverter createCsConverter() {
        CsConverterImpl csConverter = new CsConverterImpl();
        return csConverter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SeasonDayTypeSchedule createSeasonDayTypeSchedule() {
        SeasonDayTypeScheduleImpl seasonDayTypeSchedule = new SeasonDayTypeScheduleImpl();
        return seasonDayTypeSchedule;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PowerSystemResource createPowerSystemResource() {
        PowerSystemResourceImpl powerSystemResource = new PowerSystemResourceImpl();
        return powerSystemResource;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Junction createJunction() {
        JunctionImpl junction = new JunctionImpl();
        return junction;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Line createLine() {
        LineImpl line = new LineImpl();
        return line;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Jumper createJumper() {
        JumperImpl jumper = new JumperImpl();
        return jumper;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NonConformLoad createNonConformLoad() {
        NonConformLoadImpl nonConformLoad = new NonConformLoadImpl();
        return nonConformLoad;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public BusbarSection createBusbarSection() {
        BusbarSectionImpl busbarSection = new BusbarSectionImpl();
        return busbarSection;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IOPoint createIOPoint() {
        IOPointImpl ioPoint = new IOPointImpl();
        return ioPoint;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public BusNameMarker createBusNameMarker() {
        BusNameMarkerImpl busNameMarker = new BusNameMarkerImpl();
        return busNameMarker;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DCTopologicalNode createDCTopologicalNode() {
        DCTopologicalNodeImpl dcTopologicalNode = new DCTopologicalNodeImpl();
        return dcTopologicalNode;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DCShunt createDCShunt() {
        DCShuntImpl dcShunt = new DCShuntImpl();
        return dcShunt;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Bay createBay() {
        BayImpl bay = new BayImpl();
        return bay;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PerLengthPhaseImpedance createPerLengthPhaseImpedance() {
        PerLengthPhaseImpedanceImpl perLengthPhaseImpedance = new PerLengthPhaseImpedanceImpl();
        return perLengthPhaseImpedance;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public MutualCoupling createMutualCoupling() {
        MutualCouplingImpl mutualCoupling = new MutualCouplingImpl();
        return mutualCoupling;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public LoadGroup createLoadGroup() {
        LoadGroupImpl loadGroup = new LoadGroupImpl();
        return loadGroup;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Supercritical createSupercritical() {
        SupercriticalImpl supercritical = new SupercriticalImpl();
        return supercritical;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Analog createAnalog() {
        AnalogImpl analog = new AnalogImpl();
        return analog;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ControlArea createControlArea() {
        ControlAreaImpl controlArea = new ControlAreaImpl();
        return controlArea;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public StartMainFuelCurve createStartMainFuelCurve() {
        StartMainFuelCurveImpl startMainFuelCurve = new StartMainFuelCurveImpl();
        return startMainFuelCurve;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public MeasurementValue createMeasurementValue() {
        MeasurementValueImpl measurementValue = new MeasurementValueImpl();
        return measurementValue;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PowerElectronicsWindUnit createPowerElectronicsWindUnit() {
        PowerElectronicsWindUnitImpl powerElectronicsWindUnit = new PowerElectronicsWindUnitImpl();
        return powerElectronicsWindUnit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TapChanger createTapChanger() {
        TapChangerImpl tapChanger = new TapChangerImpl();
        return tapChanger;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Connector createConnector() {
        ConnectorImpl connector = new ConnectorImpl();
        return connector;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CurrentTransformer createCurrentTransformer() {
        CurrentTransformerImpl currentTransformer = new CurrentTransformerImpl();
        return currentTransformer;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DrumBoiler createDrumBoiler() {
        DrumBoilerImpl drumBoiler = new DrumBoilerImpl();
        return drumBoiler;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DCTopologicalIsland createDCTopologicalIsland() {
        DCTopologicalIslandImpl dcTopologicalIsland = new DCTopologicalIslandImpl();
        return dcTopologicalIsland;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PWRSteamSupply createPWRSteamSupply() {
        PWRSteamSupplyImpl pwrSteamSupply = new PWRSteamSupplyImpl();
        return pwrSteamSupply;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DiagramObjectGluePoint createDiagramObjectGluePoint() {
        DiagramObjectGluePointImpl diagramObjectGluePoint = new DiagramObjectGluePointImpl();
        return diagramObjectGluePoint;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public RegularIntervalSchedule createRegularIntervalSchedule() {
        RegularIntervalScheduleImpl regularIntervalSchedule = new RegularIntervalScheduleImpl();
        return regularIntervalSchedule;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SubLoadArea createSubLoadArea() {
        SubLoadAreaImpl subLoadArea = new SubLoadAreaImpl();
        return subLoadArea;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CurveData createCurveData() {
        CurveDataImpl curveData = new CurveDataImpl();
        return curveData;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public WindGeneratingUnit createWindGeneratingUnit() {
        WindGeneratingUnitImpl windGeneratingUnit = new WindGeneratingUnitImpl();
        return windGeneratingUnit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TransformerTank createTransformerTank() {
        TransformerTankImpl transformerTank = new TransformerTankImpl();
        return transformerTank;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Equipment createEquipment() {
        EquipmentImpl equipment = new EquipmentImpl();
        return equipment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CombinedCyclePlant createCombinedCyclePlant() {
        CombinedCyclePlantImpl combinedCyclePlant = new CombinedCyclePlantImpl();
        return combinedCyclePlant;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public BranchGroup createBranchGroup() {
        BranchGroupImpl branchGroup = new BranchGroupImpl();
        return branchGroup;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SetPoint createSetPoint() {
        SetPointImpl setPoint = new SetPointImpl();
        return setPoint;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public AccumulatorLimit createAccumulatorLimit() {
        AccumulatorLimitImpl accumulatorLimit = new AccumulatorLimitImpl();
        return accumulatorLimit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Sectionaliser createSectionaliser() {
        SectionaliserImpl sectionaliser = new SectionaliserImpl();
        return sectionaliser;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TapSchedule createTapSchedule() {
        TapScheduleImpl tapSchedule = new TapScheduleImpl();
        return tapSchedule;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public LineFault createLineFault() {
        LineFaultImpl lineFault = new LineFaultImpl();
        return lineFault;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public GeneratingUnit createGeneratingUnit() {
        GeneratingUnitImpl generatingUnit = new GeneratingUnitImpl();
        return generatingUnit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public HydroGeneratingEfficiencyCurve createHydroGeneratingEfficiencyCurve() {
        HydroGeneratingEfficiencyCurveImpl hydroGeneratingEfficiencyCurve = new HydroGeneratingEfficiencyCurveImpl();
        return hydroGeneratingEfficiencyCurve;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public RemoteUnit createRemoteUnit() {
        RemoteUnitImpl remoteUnit = new RemoteUnitImpl();
        return remoteUnit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SvPowerFlow createSvPowerFlow() {
        SvPowerFlowImpl svPowerFlow = new SvPowerFlowImpl();
        return svPowerFlow;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public FossilSteamSupply createFossilSteamSupply() {
        FossilSteamSupplyImpl fossilSteamSupply = new FossilSteamSupplyImpl();
        return fossilSteamSupply;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public BasePower createBasePower() {
        BasePowerImpl basePower = new BasePowerImpl();
        return basePower;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DCBaseTerminal createDCBaseTerminal() {
        DCBaseTerminalImpl dcBaseTerminal = new DCBaseTerminalImpl();
        return dcBaseTerminal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public OperationalLimitType createOperationalLimitType() {
        OperationalLimitTypeImpl operationalLimitType = new OperationalLimitTypeImpl();
        return operationalLimitType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SvInjection createSvInjection() {
        SvInjectionImpl svInjection = new SvInjectionImpl();
        return svInjection;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ActivePowerLimit createActivePowerLimit() {
        ActivePowerLimitImpl activePowerLimit = new ActivePowerLimitImpl();
        return activePowerLimit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ConformLoad createConformLoad() {
        ConformLoadImpl conformLoad = new ConformLoadImpl();
        return conformLoad;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DCTerminal createDCTerminal() {
        DCTerminalImpl dcTerminal = new DCTerminalImpl();
        return dcTerminal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SynchrocheckRelay createSynchrocheckRelay() {
        SynchrocheckRelayImpl synchrocheckRelay = new SynchrocheckRelayImpl();
        return synchrocheckRelay;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public StartRampCurve createStartRampCurve() {
        StartRampCurveImpl startRampCurve = new StartRampCurveImpl();
        return startRampCurve;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PSRType createPSRType() {
        PSRTypeImpl psrType = new PSRTypeImpl();
        return psrType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Plant createPlant() {
        PlantImpl plant = new PlantImpl();
        return plant;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public MeasurementValueSource createMeasurementValueSource() {
        MeasurementValueSourceImpl measurementValueSource = new MeasurementValueSourceImpl();
        return measurementValueSource;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Feeder createFeeder() {
        FeederImpl feeder = new FeederImpl();
        return feeder;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public StartupModel createStartupModel() {
        StartupModelImpl startupModel = new StartupModelImpl();
        return startupModel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public FaultIndicator createFaultIndicator() {
        FaultIndicatorImpl faultIndicator = new FaultIndicatorImpl();
        return faultIndicator;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public FlowSensor createFlowSensor() {
        FlowSensorImpl flowSensor = new FlowSensorImpl();
        return flowSensor;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ACDCConverter createACDCConverter() {
        ACDCConverterImpl acdcConverter = new ACDCConverterImpl();
        return acdcConverter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EnergySource createEnergySource() {
        EnergySourceImpl energySource = new EnergySourceImpl();
        return energySource;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public GroundDisconnector createGroundDisconnector() {
        GroundDisconnectorImpl groundDisconnector = new GroundDisconnectorImpl();
        return groundDisconnector;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PhaseImpedanceData createPhaseImpedanceData() {
        PhaseImpedanceDataImpl phaseImpedanceData = new PhaseImpedanceDataImpl();
        return phaseImpedanceData;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public AnalogLimit createAnalogLimit() {
        AnalogLimitImpl analogLimit = new AnalogLimitImpl();
        return analogLimit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public RegularTimePoint createRegularTimePoint() {
        RegularTimePointImpl regularTimePoint = new RegularTimePointImpl();
        return regularTimePoint;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Fuse createFuse() {
        FuseImpl fuse = new FuseImpl();
        return fuse;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Cut createCut() {
        CutImpl cut = new CutImpl();
        return cut;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Measurement createMeasurement() {
        MeasurementImpl measurement = new MeasurementImpl();
        return measurement;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public LevelVsVolumeCurve createLevelVsVolumeCurve() {
        LevelVsVolumeCurveImpl levelVsVolumeCurve = new LevelVsVolumeCurveImpl();
        return levelVsVolumeCurve;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DCGround createDCGround() {
        DCGroundImpl dcGround = new DCGroundImpl();
        return dcGround;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public BatteryUnit createBatteryUnit() {
        BatteryUnitImpl batteryUnit = new BatteryUnitImpl();
        return batteryUnit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public AccumulatorLimitSet createAccumulatorLimitSet() {
        AccumulatorLimitSetImpl accumulatorLimitSet = new AccumulatorLimitSetImpl();
        return accumulatorLimitSet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PhotoVoltaicUnit createPhotoVoltaicUnit() {
        PhotoVoltaicUnitImpl photoVoltaicUnit = new PhotoVoltaicUnitImpl();
        return photoVoltaicUnit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DCChopper createDCChopper() {
        DCChopperImpl dcChopper = new DCChopperImpl();
        return dcChopper;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public RemoteSource createRemoteSource() {
        RemoteSourceImpl remoteSource = new RemoteSourceImpl();
        return remoteSource;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PhaseTapChanger createPhaseTapChanger() {
        PhaseTapChangerImpl phaseTapChanger = new PhaseTapChangerImpl();
        return phaseTapChanger;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public HeatInputCurve createHeatInputCurve() {
        HeatInputCurveImpl heatInputCurve = new HeatInputCurveImpl();
        return heatInputCurve;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ThermalGeneratingUnit createThermalGeneratingUnit() {
        ThermalGeneratingUnitImpl thermalGeneratingUnit = new ThermalGeneratingUnitImpl();
        return thermalGeneratingUnit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public GrossToNetActivePowerCurve createGrossToNetActivePowerCurve() {
        GrossToNetActivePowerCurveImpl grossToNetActivePowerCurve = new GrossToNetActivePowerCurveImpl();
        return grossToNetActivePowerCurve;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PhaseTapChangerSymmetrical createPhaseTapChangerSymmetrical() {
        PhaseTapChangerSymmetricalImpl phaseTapChangerSymmetrical = new PhaseTapChangerSymmetricalImpl();
        return phaseTapChangerSymmetrical;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TieFlow createTieFlow() {
        TieFlowImpl tieFlow = new TieFlowImpl();
        return tieFlow;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Accumulator createAccumulator() {
        AccumulatorImpl accumulator = new AccumulatorImpl();
        return accumulator;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NonlinearShuntCompensator createNonlinearShuntCompensator() {
        NonlinearShuntCompensatorImpl nonlinearShuntCompensator = new NonlinearShuntCompensatorImpl();
        return nonlinearShuntCompensator;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Fault createFault() {
        FaultImpl fault = new FaultImpl();
        return fault;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Contingency createContingency() {
        ContingencyImpl contingency = new ContingencyImpl();
        return contingency;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Breaker createBreaker() {
        BreakerImpl breaker = new BreakerImpl();
        return breaker;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CAESPlant createCAESPlant() {
        CAESPlantImpl caesPlant = new CAESPlantImpl();
        return caesPlant;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TapChangerTablePoint createTapChangerTablePoint() {
        TapChangerTablePointImpl tapChangerTablePoint = new TapChangerTablePointImpl();
        return tapChangerTablePoint;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public FaultCauseType createFaultCauseType() {
        FaultCauseTypeImpl faultCauseType = new FaultCauseTypeImpl();
        return faultCauseType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DCBreaker createDCBreaker() {
        DCBreakerImpl dcBreaker = new DCBreakerImpl();
        return dcBreaker;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public RatioTapChanger createRatioTapChanger() {
        RatioTapChangerImpl ratioTapChanger = new RatioTapChangerImpl();
        return ratioTapChanger;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public LoadArea createLoadArea() {
        LoadAreaImpl loadArea = new LoadAreaImpl();
        return loadArea;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public FuelAllocationSchedule createFuelAllocationSchedule() {
        FuelAllocationScheduleImpl fuelAllocationSchedule = new FuelAllocationScheduleImpl();
        return fuelAllocationSchedule;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ACLineSegment createACLineSegment() {
        ACLineSegmentImpl acLineSegment = new ACLineSegmentImpl();
        return acLineSegment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Limit createLimit() {
        LimitImpl limit = new LimitImpl();
        return limit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ValueAliasSet createValueAliasSet() {
        ValueAliasSetImpl valueAliasSet = new ValueAliasSetImpl();
        return valueAliasSet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public LimitSet createLimitSet() {
        LimitSetImpl limitSet = new LimitSetImpl();
        return limitSet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public StateVariable createStateVariable() {
        StateVariableImpl stateVariable = new StateVariableImpl();
        return stateVariable;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DiscreteValue createDiscreteValue() {
        DiscreteValueImpl discreteValue = new DiscreteValueImpl();
        return discreteValue;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public AccumulatorValue createAccumulatorValue() {
        AccumulatorValueImpl accumulatorValue = new AccumulatorValueImpl();
        return accumulatorValue;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TransformerCoreAdmittance createTransformerCoreAdmittance() {
        TransformerCoreAdmittanceImpl transformerCoreAdmittance = new TransformerCoreAdmittanceImpl();
        return transformerCoreAdmittance;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TransformerStarImpedance createTransformerStarImpedance() {
        TransformerStarImpedanceImpl transformerStarImpedance = new TransformerStarImpedanceImpl();
        return transformerStarImpedance;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public VoltageLevel createVoltageLevel() {
        VoltageLevelImpl voltageLevel = new VoltageLevelImpl();
        return voltageLevel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public AccumulatorReset createAccumulatorReset() {
        AccumulatorResetImpl accumulatorReset = new AccumulatorResetImpl();
        return accumulatorReset;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PhaseTapChangerTablePoint createPhaseTapChangerTablePoint() {
        PhaseTapChangerTablePointImpl phaseTapChangerTablePoint = new PhaseTapChangerTablePointImpl();
        return phaseTapChangerTablePoint;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SvSwitch createSvSwitch() {
        SvSwitchImpl svSwitch = new SvSwitchImpl();
        return svSwitch;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public TopologicalIsland createTopologicalIsland() {
        TopologicalIslandImpl topologicalIsland = new TopologicalIslandImpl();
        return topologicalIsland;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DCConductingEquipment createDCConductingEquipment() {
        DCConductingEquipmentImpl dcConductingEquipment = new DCConductingEquipmentImpl();
        return dcConductingEquipment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ProtectionEquipment createProtectionEquipment() {
        ProtectionEquipmentImpl protectionEquipment = new ProtectionEquipmentImpl();
        return protectionEquipment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public LoadResponseCharacteristic createLoadResponseCharacteristic() {
        LoadResponseCharacteristicImpl loadResponseCharacteristic = new LoadResponseCharacteristicImpl();
        return loadResponseCharacteristic;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PowerElectronicsUnit createPowerElectronicsUnit() {
        PowerElectronicsUnitImpl powerElectronicsUnit = new PowerElectronicsUnitImpl();
        return powerElectronicsUnit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public AnalogLimitSet createAnalogLimitSet() {
        AnalogLimitSetImpl analogLimitSet = new AnalogLimitSetImpl();
        return analogLimitSet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PostLineSensor createPostLineSensor() {
        PostLineSensorImpl postLineSensor = new PostLineSensorImpl();
        return postLineSensor;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ConnectivityNodeContainer createConnectivityNodeContainer() {
        ConnectivityNodeContainerImpl connectivityNodeContainer = new ConnectivityNodeContainerImpl();
        return connectivityNodeContainer;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SolarGeneratingUnit createSolarGeneratingUnit() {
        SolarGeneratingUnitImpl solarGeneratingUnit = new SolarGeneratingUnitImpl();
        return solarGeneratingUnit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public AirCompressor createAirCompressor() {
        AirCompressorImpl airCompressor = new AirCompressorImpl();
        return airCompressor;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PhaseTapChangerAsymmetrical createPhaseTapChangerAsymmetrical() {
        PhaseTapChangerAsymmetricalImpl phaseTapChangerAsymmetrical = new PhaseTapChangerAsymmetricalImpl();
        return phaseTapChangerAsymmetrical;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EarthFaultCompensator createEarthFaultCompensator() {
        EarthFaultCompensatorImpl earthFaultCompensator = new EarthFaultCompensatorImpl();
        return earthFaultCompensator;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SvShuntCompensatorSections createSvShuntCompensatorSections() {
        SvShuntCompensatorSectionsImpl svShuntCompensatorSections = new SvShuntCompensatorSectionsImpl();
        return svShuntCompensatorSections;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ACLineSegmentPhase createACLineSegmentPhase() {
        ACLineSegmentPhaseImpl acLineSegmentPhase = new ACLineSegmentPhaseImpl();
        return acLineSegmentPhase;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public LinearShuntCompensatorPhase createLinearShuntCompensatorPhase() {
        LinearShuntCompensatorPhaseImpl linearShuntCompensatorPhase = new LinearShuntCompensatorPhaseImpl();
        return linearShuntCompensatorPhase;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PetersenCoil createPetersenCoil() {
        PetersenCoilImpl petersenCoil = new PetersenCoilImpl();
        return petersenCoil;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DCSeriesDevice createDCSeriesDevice() {
        DCSeriesDeviceImpl dcSeriesDevice = new DCSeriesDeviceImpl();
        return dcSeriesDevice;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Conductor createConductor() {
        ConductorImpl conductor = new ConductorImpl();
        return conductor;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EquivalentInjection createEquivalentInjection() {
        EquivalentInjectionImpl equivalentInjection = new EquivalentInjectionImpl();
        return equivalentInjection;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public FossilFuel createFossilFuel() {
        FossilFuelImpl fossilFuel = new FossilFuelImpl();
        return fossilFuel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DCLineSegment createDCLineSegment() {
        DCLineSegmentImpl dcLineSegment = new DCLineSegmentImpl();
        return dcLineSegment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public AnalogValue createAnalogValue() {
        AnalogValueImpl analogValue = new AnalogValueImpl();
        return analogValue;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ReportingSuperGroup createReportingSuperGroup() {
        ReportingSuperGroupImpl reportingSuperGroup = new ReportingSuperGroupImpl();
        return reportingSuperGroup;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PerLengthSequenceImpedance createPerLengthSequenceImpedance() {
        PerLengthSequenceImpedanceImpl perLengthSequenceImpedance = new PerLengthSequenceImpedanceImpl();
        return perLengthSequenceImpedance;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Season createSeason() {
        SeasonImpl season = new SeasonImpl();
        return season;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DiagramStyle createDiagramStyle() {
        DiagramStyleImpl diagramStyle = new DiagramStyleImpl();
        return diagramStyle;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PerLengthLineParameter createPerLengthLineParameter() {
        PerLengthLineParameterImpl perLengthLineParameter = new PerLengthLineParameterImpl();
        return perLengthLineParameter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SteamTurbine createSteamTurbine() {
        SteamTurbineImpl steamTurbine = new SteamTurbineImpl();
        return steamTurbine;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PowerTransformerEnd createPowerTransformerEnd() {
        PowerTransformerEndImpl powerTransformerEnd = new PowerTransformerEndImpl();
        return powerTransformerEnd;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ControlAreaGeneratingUnit createControlAreaGeneratingUnit() {
        ControlAreaGeneratingUnitImpl controlAreaGeneratingUnit = new ControlAreaGeneratingUnitImpl();
        return controlAreaGeneratingUnit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public LinearShuntCompensator createLinearShuntCompensator() {
        LinearShuntCompensatorImpl linearShuntCompensator = new LinearShuntCompensatorImpl();
        return linearShuntCompensator;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CTTempActivePowerCurve createCTTempActivePowerCurve() {
        CTTempActivePowerCurveImpl ctTempActivePowerCurve = new CTTempActivePowerCurveImpl();
        return ctTempActivePowerCurve;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PhaseTapChangerLinear createPhaseTapChangerLinear() {
        PhaseTapChangerLinearImpl phaseTapChangerLinear = new PhaseTapChangerLinearImpl();
        return phaseTapChangerLinear;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public HydroPowerPlant createHydroPowerPlant() {
        HydroPowerPlantImpl hydroPowerPlant = new HydroPowerPlantImpl();
        return hydroPowerPlant;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SvStatus createSvStatus() {
        SvStatusImpl svStatus = new SvStatusImpl();
        return svStatus;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public HydroPumpOpSchedule createHydroPumpOpSchedule() {
        HydroPumpOpScheduleImpl hydroPumpOpSchedule = new HydroPumpOpScheduleImpl();
        return hydroPumpOpSchedule;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PotentialTransformer createPotentialTransformer() {
        PotentialTransformerImpl potentialTransformer = new PotentialTransformerImpl();
        return potentialTransformer;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public VisibilityLayer createVisibilityLayer() {
        VisibilityLayerImpl visibilityLayer = new VisibilityLayerImpl();
        return visibilityLayer;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Terminal createTerminal() {
        TerminalImpl terminal = new TerminalImpl();
        return terminal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Name createName() {
        NameImpl name = new NameImpl();
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public BranchGroupTerminal createBranchGroupTerminal() {
        BranchGroupTerminalImpl branchGroupTerminal = new BranchGroupTerminalImpl();
        return branchGroupTerminal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Sensor createSensor() {
        SensorImpl sensor = new SensorImpl();
        return sensor;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ConductingEquipment createConductingEquipment() {
        ConductingEquipmentImpl conductingEquipment = new ConductingEquipmentImpl();
        return conductingEquipment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public BaseFrequency createBaseFrequency() {
        BaseFrequencyImpl baseFrequency = new BaseFrequencyImpl();
        return baseFrequency;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SvTapStep createSvTapStep() {
        SvTapStepImpl svTapStep = new SvTapStepImpl();
        return svTapStep;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PerLengthDCLineParameter createPerLengthDCLineParameter() {
        PerLengthDCLineParameterImpl perLengthDCLineParameter = new PerLengthDCLineParameterImpl();
        return perLengthDCLineParameter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EquivalentBranch createEquivalentBranch() {
        EquivalentBranchImpl equivalentBranch = new EquivalentBranchImpl();
        return equivalentBranch;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public DCConverterUnit createDCConverterUnit() {
        DCConverterUnitImpl dcConverterUnit = new DCConverterUnitImpl();
        return dcConverterUnit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PowerTransformer createPowerTransformer() {
        PowerTransformerImpl powerTransformer = new PowerTransformerImpl();
        return powerTransformer;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public GeographicalRegion createGeographicalRegion() {
        GeographicalRegionImpl geographicalRegion = new GeographicalRegionImpl();
        return geographicalRegion;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public PhaseTapChangerTable createPhaseTapChangerTable() {
        PhaseTapChangerTableImpl phaseTapChangerTable = new PhaseTapChangerTableImpl();
        return phaseTapChangerTable;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IncrementalHeatRateCurve createIncrementalHeatRateCurve() {
        IncrementalHeatRateCurveImpl incrementalHeatRateCurve = new IncrementalHeatRateCurveImpl();
        return incrementalHeatRateCurve;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EmissionCurve createEmissionCurve() {
        EmissionCurveImpl emissionCurve = new EmissionCurveImpl();
        return emissionCurve;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NonlinearShuntCompensatorPhase createNonlinearShuntCompensatorPhase() {
        NonlinearShuntCompensatorPhaseImpl nonlinearShuntCompensatorPhase = new NonlinearShuntCompensatorPhaseImpl();
        return nonlinearShuntCompensatorPhase;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ExternalNetworkInjection createExternalNetworkInjection() {
        ExternalNetworkInjectionImpl externalNetworkInjection = new ExternalNetworkInjectionImpl();
        return externalNetworkInjection;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public WaveTrap createWaveTrap() {
        WaveTrapImpl waveTrap = new WaveTrapImpl();
        return waveTrap;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public IrregularTimePoint createIrregularTimePoint() {
        IrregularTimePointImpl irregularTimePoint = new IrregularTimePointImpl();
        return irregularTimePoint;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Quality61850 createQuality61850() {
        Quality61850Impl quality61850 = new Quality61850Impl();
        return quality61850;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EquivalentNetwork createEquivalentNetwork() {
        EquivalentNetworkImpl equivalentNetwork = new EquivalentNetworkImpl();
        return equivalentNetwork;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public VsCapabilityCurve createVsCapabilityCurve() {
        VsCapabilityCurveImpl vsCapabilityCurve = new VsCapabilityCurveImpl();
        return vsCapabilityCurve;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Reservoir createReservoir() {
        ReservoirImpl reservoir = new ReservoirImpl();
        return reservoir;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public SwitchSchedule createSwitchSchedule() {
        SwitchScheduleImpl switchSchedule = new SwitchScheduleImpl();
        return switchSchedule;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public ShutdownCurve createShutdownCurve() {
        ShutdownCurveImpl shutdownCurve = new ShutdownCurveImpl();
        return shutdownCurve;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public RemotePoint createRemotePoint() {
        RemotePointImpl remotePoint = new RemotePointImpl();
        return remotePoint;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public VsPpccControlKind createVsPpccControlKindFromString( EDataType eDataType, String initialValue ) {
        VsPpccControlKind result = VsPpccControlKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertVsPpccControlKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public CsOperatingModeKind createCsOperatingModeKindFromString( EDataType eDataType, String initialValue ) {
        CsOperatingModeKind result = CsOperatingModeKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertCsOperatingModeKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public TransformerControlMode createTransformerControlModeFromString( EDataType eDataType, String initialValue ) {
        TransformerControlMode result = TransformerControlMode.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertTransformerControlModeToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public PhaseShuntConnectionKind createPhaseShuntConnectionKindFromString( EDataType eDataType,
            String initialValue ) {
        PhaseShuntConnectionKind result = PhaseShuntConnectionKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertPhaseShuntConnectionKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public OperationalLimitDirectionKind createOperationalLimitDirectionKindFromString( EDataType eDataType,
            String initialValue ) {
        OperationalLimitDirectionKind result = OperationalLimitDirectionKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertOperationalLimitDirectionKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public PhaseConnectedFaultKind createPhaseConnectedFaultKindFromString( EDataType eDataType, String initialValue ) {
        PhaseConnectedFaultKind result = PhaseConnectedFaultKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertPhaseConnectedFaultKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public BatteryStateKind createBatteryStateKindFromString( EDataType eDataType, String initialValue ) {
        BatteryStateKind result = BatteryStateKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertBatteryStateKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public CurveStyle createCurveStyleFromString( EDataType eDataType, String initialValue ) {
        CurveStyle result = CurveStyle.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertCurveStyleToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Currency createCurrencyFromString( EDataType eDataType, String initialValue ) {
        Currency result = Currency.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertCurrencyToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public WindingConnection createWindingConnectionFromString( EDataType eDataType, String initialValue ) {
        WindingConnection result = WindingConnection.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertWindingConnectionToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public WindGenUnitKind createWindGenUnitKindFromString( EDataType eDataType, String initialValue ) {
        WindGenUnitKind result = WindGenUnitKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertWindGenUnitKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public SinglePhaseKind createSinglePhaseKindFromString( EDataType eDataType, String initialValue ) {
        SinglePhaseKind result = SinglePhaseKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertSinglePhaseKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public PhaseCode createPhaseCodeFromString( EDataType eDataType, String initialValue ) {
        PhaseCode result = PhaseCode.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertPhaseCodeToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public BreakerConfiguration createBreakerConfigurationFromString( EDataType eDataType, String initialValue ) {
        BreakerConfiguration result = BreakerConfiguration.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertBreakerConfigurationToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public RegulatingControlModeKind createRegulatingControlModeKindFromString( EDataType eDataType,
            String initialValue ) {
        RegulatingControlModeKind result = RegulatingControlModeKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertRegulatingControlModeKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public DCConverterOperatingModeKind createDCConverterOperatingModeKindFromString( EDataType eDataType,
            String initialValue ) {
        DCConverterOperatingModeKind result = DCConverterOperatingModeKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertDCConverterOperatingModeKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public BoilerControlMode createBoilerControlModeFromString( EDataType eDataType, String initialValue ) {
        BoilerControlMode result = BoilerControlMode.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertBoilerControlModeToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public CoolantType createCoolantTypeFromString( EDataType eDataType, String initialValue ) {
        CoolantType result = CoolantType.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertCoolantTypeToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public HydroEnergyConversionKind createHydroEnergyConversionKindFromString( EDataType eDataType,
            String initialValue ) {
        HydroEnergyConversionKind result = HydroEnergyConversionKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertHydroEnergyConversionKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EmissionType createEmissionTypeFromString( EDataType eDataType, String initialValue ) {
        EmissionType result = EmissionType.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertEmissionTypeToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public PetersenCoilModeKind createPetersenCoilModeKindFromString( EDataType eDataType, String initialValue ) {
        PetersenCoilModeKind result = PetersenCoilModeKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertPetersenCoilModeKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public UnitMultiplier createUnitMultiplierFromString( EDataType eDataType, String initialValue ) {
        UnitMultiplier result = UnitMultiplier.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertUnitMultiplierToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ContingencyEquipmentStatusKind createContingencyEquipmentStatusKindFromString( EDataType eDataType,
            String initialValue ) {
        ContingencyEquipmentStatusKind result = ContingencyEquipmentStatusKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertContingencyEquipmentStatusKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Validity createValidityFromString( EDataType eDataType, String initialValue ) {
        Validity result = Validity.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertValidityToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ControlAreaTypeKind createControlAreaTypeKindFromString( EDataType eDataType, String initialValue ) {
        ControlAreaTypeKind result = ControlAreaTypeKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertControlAreaTypeKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public PotentialTransformerKind createPotentialTransformerKindFromString( EDataType eDataType,
            String initialValue ) {
        PotentialTransformerKind result = PotentialTransformerKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertPotentialTransformerKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public UnitSymbol createUnitSymbolFromString( EDataType eDataType, String initialValue ) {
        UnitSymbol result = UnitSymbol.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertUnitSymbolToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public BusbarConfiguration createBusbarConfigurationFromString( EDataType eDataType, String initialValue ) {
        BusbarConfiguration result = BusbarConfiguration.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertBusbarConfigurationToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public GeneratorControlSource createGeneratorControlSourceFromString( EDataType eDataType, String initialValue ) {
        GeneratorControlSource result = GeneratorControlSource.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertGeneratorControlSourceToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Source createSourceFromString( EDataType eDataType, String initialValue ) {
        Source result = Source.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertSourceToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public AsynchronousMachineKind createAsynchronousMachineKindFromString( EDataType eDataType, String initialValue ) {
        AsynchronousMachineKind result = AsynchronousMachineKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertAsynchronousMachineKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public SVCControlMode createSVCControlModeFromString( EDataType eDataType, String initialValue ) {
        SVCControlMode result = SVCControlMode.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertSVCControlModeToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public HydroPlantStorageKind createHydroPlantStorageKindFromString( EDataType eDataType, String initialValue ) {
        HydroPlantStorageKind result = HydroPlantStorageKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertHydroPlantStorageKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public RemoteUnitType createRemoteUnitTypeFromString( EDataType eDataType, String initialValue ) {
        RemoteUnitType result = RemoteUnitType.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertRemoteUnitTypeToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public OrientationKind createOrientationKindFromString( EDataType eDataType, String initialValue ) {
        OrientationKind result = OrientationKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertOrientationKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public SynchronousMachineKind createSynchronousMachineKindFromString( EDataType eDataType, String initialValue ) {
        SynchronousMachineKind result = SynchronousMachineKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertSynchronousMachineKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public CsPpccControlKind createCsPpccControlKindFromString( EDataType eDataType, String initialValue ) {
        CsPpccControlKind result = CsPpccControlKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertCsPpccControlKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public FuelType createFuelTypeFromString( EDataType eDataType, String initialValue ) {
        FuelType result = FuelType.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertFuelTypeToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public VsQpccControlKind createVsQpccControlKindFromString( EDataType eDataType, String initialValue ) {
        VsQpccControlKind result = VsQpccControlKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertVsQpccControlKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public DCPolarityKind createDCPolarityKindFromString( EDataType eDataType, String initialValue ) {
        DCPolarityKind result = DCPolarityKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertDCPolarityKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public SynchronousMachineOperatingMode createSynchronousMachineOperatingModeFromString( EDataType eDataType,
            String initialValue ) {
        SynchronousMachineOperatingMode result = SynchronousMachineOperatingMode.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertSynchronousMachineOperatingModeToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ShortCircuitRotorKind createShortCircuitRotorKindFromString( EDataType eDataType, String initialValue ) {
        ShortCircuitRotorKind result = ShortCircuitRotorKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertShortCircuitRotorKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public HydroTurbineKind createHydroTurbineKindFromString( EDataType eDataType, String initialValue ) {
        HydroTurbineKind result = HydroTurbineKind.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertHydroTurbineKindToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public GeneratorControlMode createGeneratorControlModeFromString( EDataType eDataType, String initialValue ) {
        GeneratorControlMode result = GeneratorControlMode.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertGeneratorControlModeToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EmissionValueSource createEmissionValueSourceFromString( EDataType eDataType, String initialValue ) {
        EmissionValueSource result = EmissionValueSource.get( initialValue );
        if( result == null ) throw new IllegalArgumentException(
                "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'" );
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public String convertEmissionValueSourceToString( EDataType eDataType, Object instanceValue ) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CimPackage getCimPackage() {
        return ( CimPackage ) getEPackage();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @deprecated
     * @generated
     */
    @Deprecated
    public static CimPackage getPackage() {
        return CimPackage.eINSTANCE;
    }

} //CimFactoryImpl
