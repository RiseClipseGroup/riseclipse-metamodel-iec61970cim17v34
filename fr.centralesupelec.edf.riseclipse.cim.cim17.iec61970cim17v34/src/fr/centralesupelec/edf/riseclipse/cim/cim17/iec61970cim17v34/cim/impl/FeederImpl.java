/*
*************************************************************************
**  Copyright (c) 2020 CentraleSupélec & EDF.
**  All rights reserved. This program and the accompanying materials
**  are made available under the terms of the Eclipse Public License v2.0
**  which accompanies this distribution, and is available at
**  https://www.eclipse.org/legal/epl-v20.html
** 
**  This file is part of the RiseClipse tool
**  
**  Contributors:
**      Computer Science Department, CentraleSupélec
**      EDF R&D
**  Contacts:
**      dominique.marcadet@centralesupelec.fr
**      aurelie.dehouck-neveu@edf.fr
**  Web site:
**      http://wdi.supelec.fr/software/RiseClipse/
*************************************************************************
*/
package fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.impl;

import fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage;
import fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder;
import fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation;
import fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Terminal;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feeder</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.impl.FeederImpl#getNamingSecondarySubstation <em>Naming Secondary Substation</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.impl.FeederImpl#getNormalEnergizingSubstation <em>Normal Energizing Substation</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.impl.FeederImpl#getNormalHeadTerminal <em>Normal Head Terminal</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.impl.FeederImpl#getNormalEnergizedSubstation <em>Normal Energized Substation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FeederImpl extends EquipmentContainerImpl implements Feeder {
    /**
     * The cached value of the '{@link #getNamingSecondarySubstation() <em>Naming Secondary Substation</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getNamingSecondarySubstation()
     * @generated
     * @ordered
     */
    protected EList< Substation > namingSecondarySubstation;

    /**
     * The cached value of the '{@link #getNormalEnergizingSubstation() <em>Normal Energizing Substation</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getNormalEnergizingSubstation()
     * @generated
     * @ordered
     */
    protected Substation normalEnergizingSubstation;

    /**
     * This is true if the Normal Energizing Substation reference has been set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     * @ordered
     */
    protected boolean normalEnergizingSubstationESet;

    /**
     * The cached value of the '{@link #getNormalHeadTerminal() <em>Normal Head Terminal</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getNormalHeadTerminal()
     * @generated
     * @ordered
     */
    protected EList< Terminal > normalHeadTerminal;

    /**
     * The cached value of the '{@link #getNormalEnergizedSubstation() <em>Normal Energized Substation</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getNormalEnergizedSubstation()
     * @generated
     * @ordered
     */
    protected EList< Substation > normalEnergizedSubstation;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected FeederImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return CimPackage.eINSTANCE.getFeeder();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList< Substation > getNormalEnergizedSubstation() {
        if( normalEnergizedSubstation == null ) {
            normalEnergizedSubstation = new EObjectWithInverseEList.Unsettable.ManyInverse< Substation >(
                    Substation.class, this, CimPackage.FEEDER__NORMAL_ENERGIZED_SUBSTATION,
                    CimPackage.SUBSTATION__NORMAL_ENERGIZING_FEEDER );
        }
        return normalEnergizedSubstation;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void unsetNormalEnergizedSubstation() {
        if( normalEnergizedSubstation != null ) ( ( InternalEList.Unsettable< ? > ) normalEnergizedSubstation ).unset();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isSetNormalEnergizedSubstation() {
        return normalEnergizedSubstation != null
                && ( ( InternalEList.Unsettable< ? > ) normalEnergizedSubstation ).isSet();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Substation getNormalEnergizingSubstation() {
        return normalEnergizingSubstation;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetNormalEnergizingSubstation( Substation newNormalEnergizingSubstation,
            NotificationChain msgs ) {
        Substation oldNormalEnergizingSubstation = normalEnergizingSubstation;
        normalEnergizingSubstation = newNormalEnergizingSubstation;
        boolean oldNormalEnergizingSubstationESet = normalEnergizingSubstationESet;
        normalEnergizingSubstationESet = true;
        if( eNotificationRequired() ) {
            ENotificationImpl notification = new ENotificationImpl( this, Notification.SET,
                    CimPackage.FEEDER__NORMAL_ENERGIZING_SUBSTATION, oldNormalEnergizingSubstation,
                    newNormalEnergizingSubstation, !oldNormalEnergizingSubstationESet );
            if( msgs == null )
                msgs = notification;
            else
                msgs.add( notification );
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void setNormalEnergizingSubstation( Substation newNormalEnergizingSubstation ) {
        if( newNormalEnergizingSubstation != normalEnergizingSubstation ) {
            NotificationChain msgs = null;
            if( normalEnergizingSubstation != null )
                msgs = ( ( InternalEObject ) normalEnergizingSubstation ).eInverseRemove( this,
                        CimPackage.SUBSTATION__NORMAL_ENERGIZED_FEEDER, Substation.class, msgs );
            if( newNormalEnergizingSubstation != null )
                msgs = ( ( InternalEObject ) newNormalEnergizingSubstation ).eInverseAdd( this,
                        CimPackage.SUBSTATION__NORMAL_ENERGIZED_FEEDER, Substation.class, msgs );
            msgs = basicSetNormalEnergizingSubstation( newNormalEnergizingSubstation, msgs );
            if( msgs != null ) msgs.dispatch();
        }
        else {
            boolean oldNormalEnergizingSubstationESet = normalEnergizingSubstationESet;
            normalEnergizingSubstationESet = true;
            if( eNotificationRequired() )
                eNotify( new ENotificationImpl( this, Notification.SET, CimPackage.FEEDER__NORMAL_ENERGIZING_SUBSTATION,
                        newNormalEnergizingSubstation, newNormalEnergizingSubstation,
                        !oldNormalEnergizingSubstationESet ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicUnsetNormalEnergizingSubstation( NotificationChain msgs ) {
        Substation oldNormalEnergizingSubstation = normalEnergizingSubstation;
        normalEnergizingSubstation = null;
        boolean oldNormalEnergizingSubstationESet = normalEnergizingSubstationESet;
        normalEnergizingSubstationESet = false;
        if( eNotificationRequired() ) {
            ENotificationImpl notification = new ENotificationImpl( this, Notification.UNSET,
                    CimPackage.FEEDER__NORMAL_ENERGIZING_SUBSTATION, oldNormalEnergizingSubstation, null,
                    oldNormalEnergizingSubstationESet );
            if( msgs == null )
                msgs = notification;
            else
                msgs.add( notification );
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void unsetNormalEnergizingSubstation() {
        if( normalEnergizingSubstation != null ) {
            NotificationChain msgs = null;
            msgs = ( ( InternalEObject ) normalEnergizingSubstation ).eInverseRemove( this,
                    CimPackage.SUBSTATION__NORMAL_ENERGIZED_FEEDER, Substation.class, msgs );
            msgs = basicUnsetNormalEnergizingSubstation( msgs );
            if( msgs != null ) msgs.dispatch();
        }
        else {
            boolean oldNormalEnergizingSubstationESet = normalEnergizingSubstationESet;
            normalEnergizingSubstationESet = false;
            if( eNotificationRequired() )
                eNotify( new ENotificationImpl( this, Notification.UNSET,
                        CimPackage.FEEDER__NORMAL_ENERGIZING_SUBSTATION, null, null,
                        oldNormalEnergizingSubstationESet ) );
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isSetNormalEnergizingSubstation() {
        return normalEnergizingSubstationESet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList< Terminal > getNormalHeadTerminal() {
        if( normalHeadTerminal == null ) {
            normalHeadTerminal = new EObjectWithInverseResolvingEList.Unsettable< Terminal >( Terminal.class, this,
                    CimPackage.FEEDER__NORMAL_HEAD_TERMINAL, CimPackage.TERMINAL__NORMAL_HEAD_FEEDER );
        }
        return normalHeadTerminal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void unsetNormalHeadTerminal() {
        if( normalHeadTerminal != null ) ( ( InternalEList.Unsettable< ? > ) normalHeadTerminal ).unset();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isSetNormalHeadTerminal() {
        return normalHeadTerminal != null && ( ( InternalEList.Unsettable< ? > ) normalHeadTerminal ).isSet();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EList< Substation > getNamingSecondarySubstation() {
        if( namingSecondarySubstation == null ) {
            namingSecondarySubstation = new EObjectWithInverseResolvingEList.Unsettable< Substation >( Substation.class,
                    this, CimPackage.FEEDER__NAMING_SECONDARY_SUBSTATION, CimPackage.SUBSTATION__NAMING_FEEDER );
        }
        return namingSecondarySubstation;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void unsetNamingSecondarySubstation() {
        if( namingSecondarySubstation != null ) ( ( InternalEList.Unsettable< ? > ) namingSecondarySubstation ).unset();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean isSetNamingSecondarySubstation() {
        return namingSecondarySubstation != null
                && ( ( InternalEList.Unsettable< ? > ) namingSecondarySubstation ).isSet();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings( "unchecked" )
    @Override
    public NotificationChain eInverseAdd( InternalEObject otherEnd, int featureID, NotificationChain msgs ) {
        switch( featureID ) {
        case CimPackage.FEEDER__NAMING_SECONDARY_SUBSTATION:
            return ( ( InternalEList< InternalEObject > ) ( InternalEList< ? > ) getNamingSecondarySubstation() )
                    .basicAdd( otherEnd, msgs );
        case CimPackage.FEEDER__NORMAL_ENERGIZING_SUBSTATION:
            if( normalEnergizingSubstation != null )
                msgs = ( ( InternalEObject ) normalEnergizingSubstation ).eInverseRemove( this,
                        CimPackage.SUBSTATION__NORMAL_ENERGIZED_FEEDER, Substation.class, msgs );
            return basicSetNormalEnergizingSubstation( ( Substation ) otherEnd, msgs );
        case CimPackage.FEEDER__NORMAL_HEAD_TERMINAL:
            return ( ( InternalEList< InternalEObject > ) ( InternalEList< ? > ) getNormalHeadTerminal() )
                    .basicAdd( otherEnd, msgs );
        case CimPackage.FEEDER__NORMAL_ENERGIZED_SUBSTATION:
            return ( ( InternalEList< InternalEObject > ) ( InternalEList< ? > ) getNormalEnergizedSubstation() )
                    .basicAdd( otherEnd, msgs );
        }
        return super.eInverseAdd( otherEnd, featureID, msgs );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove( InternalEObject otherEnd, int featureID, NotificationChain msgs ) {
        switch( featureID ) {
        case CimPackage.FEEDER__NAMING_SECONDARY_SUBSTATION:
            return ( ( InternalEList< ? > ) getNamingSecondarySubstation() ).basicRemove( otherEnd, msgs );
        case CimPackage.FEEDER__NORMAL_ENERGIZING_SUBSTATION:
            return basicUnsetNormalEnergizingSubstation( msgs );
        case CimPackage.FEEDER__NORMAL_HEAD_TERMINAL:
            return ( ( InternalEList< ? > ) getNormalHeadTerminal() ).basicRemove( otherEnd, msgs );
        case CimPackage.FEEDER__NORMAL_ENERGIZED_SUBSTATION:
            return ( ( InternalEList< ? > ) getNormalEnergizedSubstation() ).basicRemove( otherEnd, msgs );
        }
        return super.eInverseRemove( otherEnd, featureID, msgs );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet( int featureID, boolean resolve, boolean coreType ) {
        switch( featureID ) {
        case CimPackage.FEEDER__NAMING_SECONDARY_SUBSTATION:
            return getNamingSecondarySubstation();
        case CimPackage.FEEDER__NORMAL_ENERGIZING_SUBSTATION:
            return getNormalEnergizingSubstation();
        case CimPackage.FEEDER__NORMAL_HEAD_TERMINAL:
            return getNormalHeadTerminal();
        case CimPackage.FEEDER__NORMAL_ENERGIZED_SUBSTATION:
            return getNormalEnergizedSubstation();
        }
        return super.eGet( featureID, resolve, coreType );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings( "unchecked" )
    @Override
    public void eSet( int featureID, Object newValue ) {
        switch( featureID ) {
        case CimPackage.FEEDER__NAMING_SECONDARY_SUBSTATION:
            getNamingSecondarySubstation().clear();
            getNamingSecondarySubstation().addAll( ( Collection< ? extends Substation > ) newValue );
            return;
        case CimPackage.FEEDER__NORMAL_ENERGIZING_SUBSTATION:
            setNormalEnergizingSubstation( ( Substation ) newValue );
            return;
        case CimPackage.FEEDER__NORMAL_HEAD_TERMINAL:
            getNormalHeadTerminal().clear();
            getNormalHeadTerminal().addAll( ( Collection< ? extends Terminal > ) newValue );
            return;
        case CimPackage.FEEDER__NORMAL_ENERGIZED_SUBSTATION:
            getNormalEnergizedSubstation().clear();
            getNormalEnergizedSubstation().addAll( ( Collection< ? extends Substation > ) newValue );
            return;
        }
        super.eSet( featureID, newValue );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset( int featureID ) {
        switch( featureID ) {
        case CimPackage.FEEDER__NAMING_SECONDARY_SUBSTATION:
            unsetNamingSecondarySubstation();
            return;
        case CimPackage.FEEDER__NORMAL_ENERGIZING_SUBSTATION:
            unsetNormalEnergizingSubstation();
            return;
        case CimPackage.FEEDER__NORMAL_HEAD_TERMINAL:
            unsetNormalHeadTerminal();
            return;
        case CimPackage.FEEDER__NORMAL_ENERGIZED_SUBSTATION:
            unsetNormalEnergizedSubstation();
            return;
        }
        super.eUnset( featureID );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet( int featureID ) {
        switch( featureID ) {
        case CimPackage.FEEDER__NAMING_SECONDARY_SUBSTATION:
            return isSetNamingSecondarySubstation();
        case CimPackage.FEEDER__NORMAL_ENERGIZING_SUBSTATION:
            return isSetNormalEnergizingSubstation();
        case CimPackage.FEEDER__NORMAL_HEAD_TERMINAL:
            return isSetNormalHeadTerminal();
        case CimPackage.FEEDER__NORMAL_ENERGIZED_SUBSTATION:
            return isSetNormalEnergizedSubstation();
        }
        return super.eIsSet( featureID );
    }

} //FeederImpl
