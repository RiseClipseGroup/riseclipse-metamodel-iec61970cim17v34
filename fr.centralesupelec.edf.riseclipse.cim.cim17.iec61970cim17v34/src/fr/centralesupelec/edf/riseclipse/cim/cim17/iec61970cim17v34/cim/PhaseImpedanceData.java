/*
*************************************************************************
**  Copyright (c) 2020 CentraleSupélec & EDF.
**  All rights reserved. This program and the accompanying materials
**  are made available under the terms of the Eclipse Public License v2.0
**  which accompanies this distribution, and is available at
**  https://www.eclipse.org/legal/epl-v20.html
** 
**  This file is part of the RiseClipse tool
**  
**  Contributors:
**      Computer Science Department, CentraleSupélec
**      EDF R&D
**  Contacts:
**      dominique.marcadet@centralesupelec.fr
**      aurelie.dehouck-neveu@edf.fr
**  Web site:
**      http://wdi.supelec.fr/software/RiseClipse/
*************************************************************************
*/
package fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Phase Impedance Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getB <em>B</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getColumn <em>Column</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getFromPhase <em>From Phase</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getG <em>G</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getR <em>R</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getRow <em>Row</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getToPhase <em>To Phase</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getX <em>X</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getPhaseImpedance <em>Phase Impedance</em>}</li>
 * </ul>
 *
 * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getPhaseImpedanceData()
 * @model
 * @generated
 */
public interface PhaseImpedanceData extends CimObjectWithID {
    /**
     * Returns the value of the '<em><b>B</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>B</em>' attribute.
     * @see #isSetB()
     * @see #unsetB()
     * @see #setB(Float)
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getPhaseImpedanceData_B()
     * @model unsettable="true"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='PhaseImpedanceData.b' kind='element'"
     * @generated
     */
    Float getB();

    /**
     * Sets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getB <em>B</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>B</em>' attribute.
     * @see #isSetB()
     * @see #unsetB()
     * @see #getB()
     * @generated
     */
    void setB( Float value );

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getB <em>B</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetB()
     * @see #getB()
     * @see #setB(Float)
     * @generated
     */
    void unsetB();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getB <em>B</em>}' attribute is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>B</em>' attribute is set.
     * @see #unsetB()
     * @see #getB()
     * @see #setB(Float)
     * @generated
     */
    boolean isSetB();

    /**
     * Returns the value of the '<em><b>Column</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Column</em>' attribute.
     * @see #isSetColumn()
     * @see #unsetColumn()
     * @see #setColumn(Integer)
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getPhaseImpedanceData_Column()
     * @model unsettable="true"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='PhaseImpedanceData.column' kind='element'"
     * @generated
     */
    Integer getColumn();

    /**
     * Sets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getColumn <em>Column</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Column</em>' attribute.
     * @see #isSetColumn()
     * @see #unsetColumn()
     * @see #getColumn()
     * @generated
     */
    void setColumn( Integer value );

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getColumn <em>Column</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetColumn()
     * @see #getColumn()
     * @see #setColumn(Integer)
     * @generated
     */
    void unsetColumn();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getColumn <em>Column</em>}' attribute is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>Column</em>' attribute is set.
     * @see #unsetColumn()
     * @see #getColumn()
     * @see #setColumn(Integer)
     * @generated
     */
    boolean isSetColumn();

    /**
     * Returns the value of the '<em><b>From Phase</b></em>' attribute.
     * The literals are from the enumeration {@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.SinglePhaseKind}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>From Phase</em>' attribute.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.SinglePhaseKind
     * @see #isSetFromPhase()
     * @see #unsetFromPhase()
     * @see #setFromPhase(SinglePhaseKind)
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getPhaseImpedanceData_FromPhase()
     * @model unsettable="true"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='PhaseImpedanceData.fromPhase' kind='element'"
     * @generated
     */
    SinglePhaseKind getFromPhase();

    /**
     * Sets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getFromPhase <em>From Phase</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>From Phase</em>' attribute.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.SinglePhaseKind
     * @see #isSetFromPhase()
     * @see #unsetFromPhase()
     * @see #getFromPhase()
     * @generated
     */
    void setFromPhase( SinglePhaseKind value );

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getFromPhase <em>From Phase</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetFromPhase()
     * @see #getFromPhase()
     * @see #setFromPhase(SinglePhaseKind)
     * @generated
     */
    void unsetFromPhase();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getFromPhase <em>From Phase</em>}' attribute is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>From Phase</em>' attribute is set.
     * @see #unsetFromPhase()
     * @see #getFromPhase()
     * @see #setFromPhase(SinglePhaseKind)
     * @generated
     */
    boolean isSetFromPhase();

    /**
     * Returns the value of the '<em><b>G</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>G</em>' attribute.
     * @see #isSetG()
     * @see #unsetG()
     * @see #setG(Float)
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getPhaseImpedanceData_G()
     * @model unsettable="true"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='PhaseImpedanceData.g' kind='element'"
     * @generated
     */
    Float getG();

    /**
     * Sets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getG <em>G</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>G</em>' attribute.
     * @see #isSetG()
     * @see #unsetG()
     * @see #getG()
     * @generated
     */
    void setG( Float value );

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getG <em>G</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetG()
     * @see #getG()
     * @see #setG(Float)
     * @generated
     */
    void unsetG();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getG <em>G</em>}' attribute is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>G</em>' attribute is set.
     * @see #unsetG()
     * @see #getG()
     * @see #setG(Float)
     * @generated
     */
    boolean isSetG();

    /**
     * Returns the value of the '<em><b>R</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>R</em>' attribute.
     * @see #isSetR()
     * @see #unsetR()
     * @see #setR(Float)
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getPhaseImpedanceData_R()
     * @model unsettable="true"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='PhaseImpedanceData.r' kind='element'"
     * @generated
     */
    Float getR();

    /**
     * Sets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getR <em>R</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>R</em>' attribute.
     * @see #isSetR()
     * @see #unsetR()
     * @see #getR()
     * @generated
     */
    void setR( Float value );

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getR <em>R</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetR()
     * @see #getR()
     * @see #setR(Float)
     * @generated
     */
    void unsetR();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getR <em>R</em>}' attribute is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>R</em>' attribute is set.
     * @see #unsetR()
     * @see #getR()
     * @see #setR(Float)
     * @generated
     */
    boolean isSetR();

    /**
     * Returns the value of the '<em><b>Row</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Row</em>' attribute.
     * @see #isSetRow()
     * @see #unsetRow()
     * @see #setRow(Integer)
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getPhaseImpedanceData_Row()
     * @model unsettable="true"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='PhaseImpedanceData.row' kind='element'"
     * @generated
     */
    Integer getRow();

    /**
     * Sets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getRow <em>Row</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Row</em>' attribute.
     * @see #isSetRow()
     * @see #unsetRow()
     * @see #getRow()
     * @generated
     */
    void setRow( Integer value );

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getRow <em>Row</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetRow()
     * @see #getRow()
     * @see #setRow(Integer)
     * @generated
     */
    void unsetRow();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getRow <em>Row</em>}' attribute is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>Row</em>' attribute is set.
     * @see #unsetRow()
     * @see #getRow()
     * @see #setRow(Integer)
     * @generated
     */
    boolean isSetRow();

    /**
     * Returns the value of the '<em><b>To Phase</b></em>' attribute.
     * The literals are from the enumeration {@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.SinglePhaseKind}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>To Phase</em>' attribute.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.SinglePhaseKind
     * @see #isSetToPhase()
     * @see #unsetToPhase()
     * @see #setToPhase(SinglePhaseKind)
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getPhaseImpedanceData_ToPhase()
     * @model unsettable="true"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='PhaseImpedanceData.toPhase' kind='element'"
     * @generated
     */
    SinglePhaseKind getToPhase();

    /**
     * Sets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getToPhase <em>To Phase</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>To Phase</em>' attribute.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.SinglePhaseKind
     * @see #isSetToPhase()
     * @see #unsetToPhase()
     * @see #getToPhase()
     * @generated
     */
    void setToPhase( SinglePhaseKind value );

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getToPhase <em>To Phase</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetToPhase()
     * @see #getToPhase()
     * @see #setToPhase(SinglePhaseKind)
     * @generated
     */
    void unsetToPhase();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getToPhase <em>To Phase</em>}' attribute is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>To Phase</em>' attribute is set.
     * @see #unsetToPhase()
     * @see #getToPhase()
     * @see #setToPhase(SinglePhaseKind)
     * @generated
     */
    boolean isSetToPhase();

    /**
     * Returns the value of the '<em><b>X</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>X</em>' attribute.
     * @see #isSetX()
     * @see #unsetX()
     * @see #setX(Float)
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getPhaseImpedanceData_X()
     * @model unsettable="true"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='PhaseImpedanceData.x' kind='element'"
     * @generated
     */
    Float getX();

    /**
     * Sets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getX <em>X</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>X</em>' attribute.
     * @see #isSetX()
     * @see #unsetX()
     * @see #getX()
     * @generated
     */
    void setX( Float value );

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getX <em>X</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetX()
     * @see #getX()
     * @see #setX(Float)
     * @generated
     */
    void unsetX();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getX <em>X</em>}' attribute is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>X</em>' attribute is set.
     * @see #unsetX()
     * @see #getX()
     * @see #setX(Float)
     * @generated
     */
    boolean isSetX();

    /**
     * Returns the value of the '<em><b>Phase Impedance</b></em>' reference.
     * It is bidirectional and its opposite is '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PerLengthPhaseImpedance#getPhaseImpedanceData <em>Phase Impedance Data</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Phase Impedance</em>' reference.
     * @see #isSetPhaseImpedance()
     * @see #unsetPhaseImpedance()
     * @see #setPhaseImpedance(PerLengthPhaseImpedance)
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getPhaseImpedanceData_PhaseImpedance()
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PerLengthPhaseImpedance#getPhaseImpedanceData
     * @model opposite="PhaseImpedanceData" resolveProxies="false" unsettable="true" ordered="false"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='PhaseImpedanceData.PhaseImpedance' kind='element'"
     * @generated
     */
    PerLengthPhaseImpedance getPhaseImpedance();

    /**
     * Sets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getPhaseImpedance <em>Phase Impedance</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Phase Impedance</em>' reference.
     * @see #isSetPhaseImpedance()
     * @see #unsetPhaseImpedance()
     * @see #getPhaseImpedance()
     * @generated
     */
    void setPhaseImpedance( PerLengthPhaseImpedance value );

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getPhaseImpedance <em>Phase Impedance</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetPhaseImpedance()
     * @see #getPhaseImpedance()
     * @see #setPhaseImpedance(PerLengthPhaseImpedance)
     * @generated
     */
    void unsetPhaseImpedance();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.PhaseImpedanceData#getPhaseImpedance <em>Phase Impedance</em>}' reference is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>Phase Impedance</em>' reference is set.
     * @see #unsetPhaseImpedance()
     * @see #getPhaseImpedance()
     * @see #setPhaseImpedance(PerLengthPhaseImpedance)
     * @generated
     */
    boolean isSetPhaseImpedance();

} // PhaseImpedanceData
