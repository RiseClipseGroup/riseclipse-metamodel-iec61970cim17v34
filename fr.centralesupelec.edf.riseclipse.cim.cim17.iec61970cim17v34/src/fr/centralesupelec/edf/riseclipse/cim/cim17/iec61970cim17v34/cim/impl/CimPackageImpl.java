/*
*************************************************************************
**  Copyright (c) 2020 CentraleSupélec & EDF.
**  All rights reserved. This program and the accompanying materials
**  are made available under the terms of the Eclipse Public License v2.0
**  which accompanies this distribution, and is available at
**  https://www.eclipse.org/legal/epl-v20.html
** 
**  This file is part of the RiseClipse tool
**  
**  Contributors:
**      Computer Science Department, CentraleSupélec
**      EDF R&D
**  Contacts:
**      dominique.marcadet@centralesupelec.fr
**      aurelie.dehouck-neveu@edf.fr
**  Web site:
**      http://wdi.supelec.fr/software/RiseClipse/
*************************************************************************
*/
package fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.impl;

import fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimFactory;
import fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage;

import java.io.IOException;

import java.net.URL;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CimPackageImpl extends EPackageImpl implements CimPackage {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected String packageFilename = "cim.ecore";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass cimObjectWithIDEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dateIntervalEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass monthDayIntervalEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass stringQuantityEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass timeIntervalEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass faultImpedanceEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass decimalQuantityEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dateTimeIntervalEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass floatQuantityEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass integerQuantityEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass compositeSwitchEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dcLineEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass penstockLossCurveEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass iec61970CIMVersionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dcBusbarEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass regulatingCondEqEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dcDisconnectorEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass reactiveCapabilityCurveEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass recloserEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass auxiliaryEquipmentEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass discreteEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass rotatingMachineEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass nonlinearShuntCompensatorPhasePointEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass operatingShareEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass frequencyConverterEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass analogControlEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass switchEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass inflowForecastEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass protectedSwitchEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass emissionAccountEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass loadBreakSwitchEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass currentRelayEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass remoteControlEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass steamSendoutScheduleEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass startIgnFuelCurveEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass staticVarCompensatorEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dcSwitchEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass bwrSteamSupplyEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass controlEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dayTypeEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass connectivityNodeEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass combustionTurbineEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass ratioTapChangerTableEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass valueToAliasEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass genUnitOpCostCurveEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass phaseTapChangerNonLinearEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass nameTypeAuthorityEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass acdcTerminalEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass textDiagramObjectEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass nuclearGeneratingUnitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass clampEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass apparentPowerLimitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass stringMeasurementValueEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass equipmentContainerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass subcriticalEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass tapChangerControlEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass hydroPumpEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass asynchronousMachineEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass energyConsumerPhaseEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass contingencyElementEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass powerElectronicsConnectionPhaseEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass seriesCompensatorEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass diagramObjectPointEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass equivalentEquipmentEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass equivalentShuntEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass energyConsumerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass operationalLimitSetEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass powerCutZoneEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass curveEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass nonConformLoadScheduleEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass transformerEndEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass targetLevelScheduleEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass voltageLimitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass operatingParticipantEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass currentLimitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass altGeneratingUnitMeasEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass primeMoverEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass phaseTapChangerTabularEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass topologicalNodeEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass stringMeasurementEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass energyAreaEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass cogenerationPlantEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass genUnitOpScheduleEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass nonConformLoadGroupEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass raiseLowerCommandEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass basicIntervalScheduleEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass operationalLimitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass stationSupplyEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass subGeographicalRegionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass hydroTurbineEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass acdcConverterDCTerminalEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass transformerTankEndEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass nameTypeEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass regulatingControlEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass reportingGroupEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass altTieMeasEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass energySourcePhaseEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass communicationLinkEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass nonlinearShuntCompensatorPointEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass synchronousMachineEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass heatRateCurveEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass baseVoltageEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass vsConverterEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass energyConnectionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass switchPhaseEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass conformLoadGroupEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass diagramObjectEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass hydroGeneratingUnitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass disconnectorEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass perLengthImpedanceEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass substationEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass diagramObjectStyleEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass commandEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass shuntCompensatorEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass shuntCompensatorPhaseEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass recloseSequenceEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass voltageControlZoneEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass steamSupplyEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass measurementValueQualityEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass equipmentFaultEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dcEquipmentContainerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass heatRecoveryBoilerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass energySchedulingTypeEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass contingencyEquipmentEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass conformLoadScheduleEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass regulationScheduleEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass tailbayLossCurveEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass identifiedObjectEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass groundingImpedanceEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass powerElectronicsConnectionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass groundEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass diagramEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass svVoltageEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass transformerMeshImpedanceEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dcNodeEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass surgeArresterEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass irregularIntervalScheduleEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass ratioTapChangerTablePointEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass csConverterEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass seasonDayTypeScheduleEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass powerSystemResourceEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass junctionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass lineEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass jumperEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass nonConformLoadEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass busbarSectionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass ioPointEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass busNameMarkerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dcTopologicalNodeEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dcShuntEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass bayEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass perLengthPhaseImpedanceEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass mutualCouplingEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass loadGroupEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass supercriticalEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass analogEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass controlAreaEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass startMainFuelCurveEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass measurementValueEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass powerElectronicsWindUnitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass tapChangerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass connectorEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass currentTransformerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass drumBoilerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dcTopologicalIslandEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass pwrSteamSupplyEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass diagramObjectGluePointEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass regularIntervalScheduleEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass subLoadAreaEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass curveDataEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass windGeneratingUnitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass transformerTankEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass equipmentEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass combinedCyclePlantEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass branchGroupEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass setPointEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass accumulatorLimitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass sectionaliserEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass tapScheduleEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass lineFaultEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass generatingUnitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass hydroGeneratingEfficiencyCurveEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass remoteUnitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass svPowerFlowEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass fossilSteamSupplyEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass basePowerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dcBaseTerminalEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass operationalLimitTypeEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass svInjectionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass activePowerLimitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass conformLoadEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dcTerminalEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass synchrocheckRelayEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass startRampCurveEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass psrTypeEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass plantEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass measurementValueSourceEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass feederEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass startupModelEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass faultIndicatorEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass flowSensorEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass acdcConverterEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass energySourceEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass groundDisconnectorEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass phaseImpedanceDataEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass analogLimitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass regularTimePointEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass fuseEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass cutEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass measurementEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass levelVsVolumeCurveEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dcGroundEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass batteryUnitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass accumulatorLimitSetEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass photoVoltaicUnitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dcChopperEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass remoteSourceEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass phaseTapChangerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass heatInputCurveEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass thermalGeneratingUnitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass grossToNetActivePowerCurveEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass phaseTapChangerSymmetricalEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass tieFlowEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass accumulatorEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass nonlinearShuntCompensatorEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass faultEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass contingencyEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass breakerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass caesPlantEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass tapChangerTablePointEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass faultCauseTypeEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dcBreakerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass ratioTapChangerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass loadAreaEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass fuelAllocationScheduleEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass acLineSegmentEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass limitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass valueAliasSetEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass limitSetEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass stateVariableEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass discreteValueEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass accumulatorValueEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass transformerCoreAdmittanceEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass transformerStarImpedanceEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass voltageLevelEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass accumulatorResetEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass phaseTapChangerTablePointEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass svSwitchEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass topologicalIslandEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dcConductingEquipmentEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass protectionEquipmentEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass loadResponseCharacteristicEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass powerElectronicsUnitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass analogLimitSetEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass postLineSensorEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass connectivityNodeContainerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass solarGeneratingUnitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass airCompressorEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass phaseTapChangerAsymmetricalEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass earthFaultCompensatorEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass svShuntCompensatorSectionsEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass acLineSegmentPhaseEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass linearShuntCompensatorPhaseEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass petersenCoilEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dcSeriesDeviceEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass conductorEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass equivalentInjectionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass fossilFuelEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dcLineSegmentEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass analogValueEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass reportingSuperGroupEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass perLengthSequenceImpedanceEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass seasonEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass diagramStyleEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass perLengthLineParameterEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass steamTurbineEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass powerTransformerEndEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass controlAreaGeneratingUnitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass linearShuntCompensatorEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass ctTempActivePowerCurveEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass phaseTapChangerLinearEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass hydroPowerPlantEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass svStatusEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass hydroPumpOpScheduleEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass potentialTransformerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass visibilityLayerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass terminalEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass nameEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass branchGroupTerminalEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass sensorEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass conductingEquipmentEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass baseFrequencyEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass svTapStepEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass perLengthDCLineParameterEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass equivalentBranchEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass dcConverterUnitEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass powerTransformerEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass geographicalRegionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass phaseTapChangerTableEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass incrementalHeatRateCurveEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass emissionCurveEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass nonlinearShuntCompensatorPhaseEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass externalNetworkInjectionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass waveTrapEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass irregularTimePointEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass quality61850EClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass equivalentNetworkEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass vsCapabilityCurveEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass reservoirEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass switchScheduleEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass shutdownCurveEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EClass remotePointEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum vsPpccControlKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum csOperatingModeKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum transformerControlModeEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum phaseShuntConnectionKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum operationalLimitDirectionKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum phaseConnectedFaultKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum batteryStateKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum curveStyleEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum currencyEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum windingConnectionEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum windGenUnitKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum singlePhaseKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum phaseCodeEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum breakerConfigurationEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum regulatingControlModeKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum dcConverterOperatingModeKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum boilerControlModeEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum coolantTypeEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum hydroEnergyConversionKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum emissionTypeEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum petersenCoilModeKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum unitMultiplierEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum contingencyEquipmentStatusKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum validityEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum controlAreaTypeKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum potentialTransformerKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum unitSymbolEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum busbarConfigurationEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum generatorControlSourceEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum sourceEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum asynchronousMachineKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum svcControlModeEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum hydroPlantStorageKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum remoteUnitTypeEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum orientationKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum synchronousMachineKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum csPpccControlKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum fuelTypeEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum vsQpccControlKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum dcPolarityKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum synchronousMachineOperatingModeEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum shortCircuitRotorKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum hydroTurbineKindEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum generatorControlModeEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private EEnum emissionValueSourceEEnum = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#eNS_URI
     * @see #init()
     * @generated
     */
    private CimPackageImpl() {
        super( eNS_URI, CimFactory.eINSTANCE );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     *
     * <p>This method is used to initialize {@link CimPackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #eNS_URI
     * @generated
     */
    public static CimPackage init() {
        if( isInited ) return ( CimPackage ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI );

        // Obtain or create and register package
        Object registeredCimPackage = EPackage.Registry.INSTANCE.get( eNS_URI );
        CimPackageImpl theCimPackage = registeredCimPackage instanceof CimPackageImpl
                ? ( CimPackageImpl ) registeredCimPackage
                : new CimPackageImpl();

        isInited = true;

        // Load packages
        theCimPackage.loadPackage();

        // Fix loaded packages
        theCimPackage.fixPackageContents();

        // Mark meta-data to indicate it can't be changed
        theCimPackage.freeze();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put( CimPackage.eNS_URI, theCimPackage );
        return theCimPackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCimObjectWithID() {
        if( cimObjectWithIDEClass == null ) {
            cimObjectWithIDEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 0 );
        }
        return cimObjectWithIDEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCimObjectWithID_ID() {
        return ( EAttribute ) getCimObjectWithID().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDateInterval() {
        if( dateIntervalEClass == null ) {
            dateIntervalEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 47 );
        }
        return dateIntervalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDateInterval_End() {
        return ( EAttribute ) getDateInterval().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDateInterval_Start() {
        return ( EAttribute ) getDateInterval().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getMonthDayInterval() {
        if( monthDayIntervalEClass == null ) {
            monthDayIntervalEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 46 );
        }
        return monthDayIntervalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getMonthDayInterval_End() {
        return ( EAttribute ) getMonthDayInterval().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getMonthDayInterval_Start() {
        return ( EAttribute ) getMonthDayInterval().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getStringQuantity() {
        if( stringQuantityEClass == null ) {
            stringQuantityEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 53 );
        }
        return stringQuantityEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStringQuantity_Multiplier() {
        return ( EAttribute ) getStringQuantity().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStringQuantity_Unit() {
        return ( EAttribute ) getStringQuantity().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStringQuantity_Value() {
        return ( EAttribute ) getStringQuantity().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getTimeInterval() {
        if( timeIntervalEClass == null ) {
            timeIntervalEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 54 );
        }
        return timeIntervalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTimeInterval_End() {
        return ( EAttribute ) getTimeInterval().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTimeInterval_Start() {
        return ( EAttribute ) getTimeInterval().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getFaultImpedance() {
        if( faultImpedanceEClass == null ) {
            faultImpedanceEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 52 );
        }
        return faultImpedanceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFaultImpedance_RGround() {
        return ( EAttribute ) getFaultImpedance().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFaultImpedance_RLineToLine() {
        return ( EAttribute ) getFaultImpedance().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFaultImpedance_XGround() {
        return ( EAttribute ) getFaultImpedance().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFaultImpedance_XLineToLine() {
        return ( EAttribute ) getFaultImpedance().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDecimalQuantity() {
        if( decimalQuantityEClass == null ) {
            decimalQuantityEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 50 );
        }
        return decimalQuantityEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDecimalQuantity_Currency() {
        return ( EAttribute ) getDecimalQuantity().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDecimalQuantity_Multiplier() {
        return ( EAttribute ) getDecimalQuantity().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDecimalQuantity_Unit() {
        return ( EAttribute ) getDecimalQuantity().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDecimalQuantity_Value() {
        return ( EAttribute ) getDecimalQuantity().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDateTimeInterval() {
        if( dateTimeIntervalEClass == null ) {
            dateTimeIntervalEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 51 );
        }
        return dateTimeIntervalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDateTimeInterval_End() {
        return ( EAttribute ) getDateTimeInterval().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDateTimeInterval_Start() {
        return ( EAttribute ) getDateTimeInterval().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getFloatQuantity() {
        if( floatQuantityEClass == null ) {
            floatQuantityEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 49 );
        }
        return floatQuantityEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFloatQuantity_Multiplier() {
        return ( EAttribute ) getFloatQuantity().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFloatQuantity_Unit() {
        return ( EAttribute ) getFloatQuantity().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFloatQuantity_Value() {
        return ( EAttribute ) getFloatQuantity().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getIntegerQuantity() {
        if( integerQuantityEClass == null ) {
            integerQuantityEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 48 );
        }
        return integerQuantityEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getIntegerQuantity_Multiplier() {
        return ( EAttribute ) getIntegerQuantity().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getIntegerQuantity_Unit() {
        return ( EAttribute ) getIntegerQuantity().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getIntegerQuantity_Value() {
        return ( EAttribute ) getIntegerQuantity().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCompositeSwitch() {
        if( compositeSwitchEClass == null ) {
            compositeSwitchEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 160 );
        }
        return compositeSwitchEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCompositeSwitch_CompositeSwitchType() {
        return ( EAttribute ) getCompositeSwitch().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCompositeSwitch_Switches() {
        return ( EReference ) getCompositeSwitch().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDCLine() {
        if( dcLineEClass == null ) {
            dcLineEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 354 );
        }
        return dcLineEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDCLine_Region() {
        return ( EReference ) getDCLine().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPenstockLossCurve() {
        if( penstockLossCurveEClass == null ) {
            penstockLossCurveEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 57 );
        }
        return penstockLossCurveEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPenstockLossCurve_HydroGeneratingUnit() {
        return ( EReference ) getPenstockLossCurve().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getIEC61970CIMVersion() {
        if( iec61970CIMVersionEClass == null ) {
            iec61970CIMVersionEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 329 );
        }
        return iec61970CIMVersionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getIEC61970CIMVersion_Date() {
        return ( EAttribute ) getIEC61970CIMVersion().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getIEC61970CIMVersion_Version() {
        return ( EAttribute ) getIEC61970CIMVersion().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDCBusbar() {
        if( dcBusbarEClass == null ) {
            dcBusbarEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 274 );
        }
        return dcBusbarEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getRegulatingCondEq() {
        if( regulatingCondEqEClass == null ) {
            regulatingCondEqEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 202 );
        }
        return regulatingCondEqEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRegulatingCondEq_ControlEnabled() {
        return ( EAttribute ) getRegulatingCondEq().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRegulatingCondEq_RegulatingControl() {
        return ( EReference ) getRegulatingCondEq().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDCDisconnector() {
        if( dcDisconnectorEClass == null ) {
            dcDisconnectorEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 65 );
        }
        return dcDisconnectorEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getReactiveCapabilityCurve() {
        if( reactiveCapabilityCurveEClass == null ) {
            reactiveCapabilityCurveEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 166 );
        }
        return reactiveCapabilityCurveEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getReactiveCapabilityCurve_CoolantTemperature() {
        return ( EAttribute ) getReactiveCapabilityCurve().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getReactiveCapabilityCurve_HydrogenPressure() {
        return ( EAttribute ) getReactiveCapabilityCurve().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getReactiveCapabilityCurve_EquivalentInjection() {
        return ( EReference ) getReactiveCapabilityCurve().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getReactiveCapabilityCurve_InitiallyUsedBySynchronousMachines() {
        return ( EReference ) getReactiveCapabilityCurve().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getReactiveCapabilityCurve_SynchronousMachines() {
        return ( EReference ) getReactiveCapabilityCurve().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getRecloser() {
        if( recloserEClass == null ) {
            recloserEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 334 );
        }
        return recloserEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getAuxiliaryEquipment() {
        if( auxiliaryEquipmentEClass == null ) {
            auxiliaryEquipmentEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 270 );
        }
        return auxiliaryEquipmentEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAuxiliaryEquipment_Terminal() {
        return ( EReference ) getAuxiliaryEquipment().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDiscrete() {
        if( discreteEClass == null ) {
            discreteEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 154 );
        }
        return discreteEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDiscrete_MaxValue() {
        return ( EAttribute ) getDiscrete().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDiscrete_MinValue() {
        return ( EAttribute ) getDiscrete().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDiscrete_NormalValue() {
        return ( EAttribute ) getDiscrete().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDiscrete_DiscreteValues() {
        return ( EReference ) getDiscrete().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDiscrete_ValueAliasSet() {
        return ( EReference ) getDiscrete().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getRotatingMachine() {
        if( rotatingMachineEClass == null ) {
            rotatingMachineEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 106 );
        }
        return rotatingMachineEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRotatingMachine_P() {
        return ( EAttribute ) getRotatingMachine().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRotatingMachine_Q() {
        return ( EAttribute ) getRotatingMachine().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRotatingMachine_RatedPowerFactor() {
        return ( EAttribute ) getRotatingMachine().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRotatingMachine_RatedS() {
        return ( EAttribute ) getRotatingMachine().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRotatingMachine_RatedU() {
        return ( EAttribute ) getRotatingMachine().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRotatingMachine_GeneratingUnit() {
        return ( EReference ) getRotatingMachine().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRotatingMachine_HydroPump() {
        return ( EReference ) getRotatingMachine().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getNonlinearShuntCompensatorPhasePoint() {
        if( nonlinearShuntCompensatorPhasePointEClass == null ) {
            nonlinearShuntCompensatorPhasePointEClass = ( EClass ) EPackage.Registry.INSTANCE
                    .getEPackage( CimPackage.eNS_URI ).getEClassifiers().get( 97 );
        }
        return nonlinearShuntCompensatorPhasePointEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getNonlinearShuntCompensatorPhasePoint_B() {
        return ( EAttribute ) getNonlinearShuntCompensatorPhasePoint().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getNonlinearShuntCompensatorPhasePoint_G() {
        return ( EAttribute ) getNonlinearShuntCompensatorPhasePoint().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getNonlinearShuntCompensatorPhasePoint_SectionNumber() {
        return ( EAttribute ) getNonlinearShuntCompensatorPhasePoint().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getNonlinearShuntCompensatorPhasePoint_NonlinearShuntCompensatorPhase() {
        return ( EReference ) getNonlinearShuntCompensatorPhasePoint().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getOperatingShare() {
        if( operatingShareEClass == null ) {
            operatingShareEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 175 );
        }
        return operatingShareEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getOperatingShare_Percentage() {
        return ( EAttribute ) getOperatingShare().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getOperatingShare_PowerSystemResource() {
        return ( EReference ) getOperatingShare().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getOperatingShare_OperatingParticipant() {
        return ( EReference ) getOperatingShare().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getFrequencyConverter() {
        if( frequencyConverterEClass == null ) {
            frequencyConverterEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 140 );
        }
        return frequencyConverterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFrequencyConverter_Frequency() {
        return ( EAttribute ) getFrequencyConverter().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFrequencyConverter_MaxP() {
        return ( EAttribute ) getFrequencyConverter().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFrequencyConverter_MaxU() {
        return ( EAttribute ) getFrequencyConverter().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFrequencyConverter_MinP() {
        return ( EAttribute ) getFrequencyConverter().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFrequencyConverter_MinU() {
        return ( EAttribute ) getFrequencyConverter().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getAnalogControl() {
        if( analogControlEClass == null ) {
            analogControlEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 288 );
        }
        return analogControlEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAnalogControl_MaxValue() {
        return ( EAttribute ) getAnalogControl().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAnalogControl_MinValue() {
        return ( EAttribute ) getAnalogControl().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAnalogControl_AnalogValue() {
        return ( EReference ) getAnalogControl().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSwitch() {
        if( switchEClass == null ) {
            switchEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 348 );
        }
        return switchEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSwitch_NormalOpen() {
        return ( EAttribute ) getSwitch().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSwitch_Open() {
        return ( EAttribute ) getSwitch().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSwitch_RatedCurrent() {
        return ( EAttribute ) getSwitch().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSwitch_Retained() {
        return ( EAttribute ) getSwitch().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSwitch_SwitchOnCount() {
        return ( EAttribute ) getSwitch().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSwitch_SwitchOnDate() {
        return ( EAttribute ) getSwitch().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSwitch_SwitchPhase() {
        return ( EReference ) getSwitch().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSwitch_CompositeSwitch() {
        return ( EReference ) getSwitch().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSwitch_SvSwitch() {
        return ( EReference ) getSwitch().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSwitch_SwitchSchedules() {
        return ( EReference ) getSwitch().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getInflowForecast() {
        if( inflowForecastEClass == null ) {
            inflowForecastEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 321 );
        }
        return inflowForecastEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getInflowForecast_Reservoir() {
        return ( EReference ) getInflowForecast().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getProtectedSwitch() {
        if( protectedSwitchEClass == null ) {
            protectedSwitchEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 120 );
        }
        return protectedSwitchEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getProtectedSwitch_BreakingCapacity() {
        return ( EAttribute ) getProtectedSwitch().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getProtectedSwitch_RecloseSequences() {
        return ( EReference ) getProtectedSwitch().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getProtectedSwitch_OperatedByProtectionEquipment() {
        return ( EReference ) getProtectedSwitch().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEmissionAccount() {
        if( emissionAccountEClass == null ) {
            emissionAccountEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 204 );
        }
        return emissionAccountEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEmissionAccount_EmissionType() {
        return ( EAttribute ) getEmissionAccount().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEmissionAccount_EmissionValueSource() {
        return ( EAttribute ) getEmissionAccount().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEmissionAccount_ThermalGeneratingUnit() {
        return ( EReference ) getEmissionAccount().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getLoadBreakSwitch() {
        if( loadBreakSwitchEClass == null ) {
            loadBreakSwitchEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 215 );
        }
        return loadBreakSwitchEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCurrentRelay() {
        if( currentRelayEClass == null ) {
            currentRelayEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 113 );
        }
        return currentRelayEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurrentRelay_CurrentLimit1() {
        return ( EAttribute ) getCurrentRelay().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurrentRelay_CurrentLimit2() {
        return ( EAttribute ) getCurrentRelay().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurrentRelay_CurrentLimit3() {
        return ( EAttribute ) getCurrentRelay().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurrentRelay_InverseTimeFlag() {
        return ( EAttribute ) getCurrentRelay().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurrentRelay_TimeDelay1() {
        return ( EAttribute ) getCurrentRelay().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurrentRelay_TimeDelay2() {
        return ( EAttribute ) getCurrentRelay().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurrentRelay_TimeDelay3() {
        return ( EAttribute ) getCurrentRelay().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getRemoteControl() {
        if( remoteControlEClass == null ) {
            remoteControlEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 252 );
        }
        return remoteControlEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRemoteControl_ActuatorMaximum() {
        return ( EAttribute ) getRemoteControl().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRemoteControl_ActuatorMinimum() {
        return ( EAttribute ) getRemoteControl().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRemoteControl_RemoteControlled() {
        return ( EAttribute ) getRemoteControl().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRemoteControl_Control() {
        return ( EReference ) getRemoteControl().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSteamSendoutSchedule() {
        if( steamSendoutScheduleEClass == null ) {
            steamSendoutScheduleEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 78 );
        }
        return steamSendoutScheduleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSteamSendoutSchedule_CogenerationPlant() {
        return ( EReference ) getSteamSendoutSchedule().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getStartIgnFuelCurve() {
        if( startIgnFuelCurveEClass == null ) {
            startIgnFuelCurveEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 229 );
        }
        return startIgnFuelCurveEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStartIgnFuelCurve_IgnitionFuelType() {
        return ( EAttribute ) getStartIgnFuelCurve().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getStartIgnFuelCurve_StartupModel() {
        return ( EReference ) getStartIgnFuelCurve().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getStaticVarCompensator() {
        if( staticVarCompensatorEClass == null ) {
            staticVarCompensatorEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 241 );
        }
        return staticVarCompensatorEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStaticVarCompensator_CapacitiveRating() {
        return ( EAttribute ) getStaticVarCompensator().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStaticVarCompensator_InductiveRating() {
        return ( EAttribute ) getStaticVarCompensator().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStaticVarCompensator_Q() {
        return ( EAttribute ) getStaticVarCompensator().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStaticVarCompensator_Slope() {
        return ( EAttribute ) getStaticVarCompensator().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStaticVarCompensator_SVCControlMode() {
        return ( EAttribute ) getStaticVarCompensator().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStaticVarCompensator_VoltageSetPoint() {
        return ( EAttribute ) getStaticVarCompensator().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDCSwitch() {
        if( dcSwitchEClass == null ) {
            dcSwitchEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 242 );
        }
        return dcSwitchEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getBWRSteamSupply() {
        if( bwrSteamSupplyEClass == null ) {
            bwrSteamSupplyEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 246 );
        }
        return bwrSteamSupplyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_HighPowerLimit() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_InCoreThermalTC() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_IntegralGain() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_LowerLimit() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_LowPowerLimit() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_PressureLimit() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_PressureSetpointGA() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_PressureSetpointTC1() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_PressureSetpointTC2() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_ProportionalGain() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_RfAux1() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_RfAux2() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_RfAux3() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_RfAux4() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 13 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_RfAux5() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 14 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_RfAux6() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 15 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_RfAux7() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 16 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_RfAux8() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 17 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_RodPattern() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 18 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_RodPatternConstant() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 19 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBWRSteamSupply_UpperLimit() {
        return ( EAttribute ) getBWRSteamSupply().getEStructuralFeatures().get( 20 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getControl() {
        if( controlEClass == null ) {
            controlEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 223 );
        }
        return controlEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getControl_ControlType() {
        return ( EAttribute ) getControl().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getControl_OperationInProgress() {
        return ( EAttribute ) getControl().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getControl_TimeStamp() {
        return ( EAttribute ) getControl().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getControl_UnitMultiplier() {
        return ( EAttribute ) getControl().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getControl_UnitSymbol() {
        return ( EAttribute ) getControl().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getControl_PowerSystemResource() {
        return ( EReference ) getControl().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getControl_RemoteControl() {
        return ( EReference ) getControl().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDayType() {
        if( dayTypeEClass == null ) {
            dayTypeEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 249 );
        }
        return dayTypeEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDayType_SeasonDayTypeSchedules() {
        return ( EReference ) getDayType().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getConnectivityNode() {
        if( connectivityNodeEClass == null ) {
            connectivityNodeEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 92 );
        }
        return connectivityNodeEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getConnectivityNode_TopologicalNode() {
        return ( EReference ) getConnectivityNode().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getConnectivityNode_Terminals() {
        return ( EReference ) getConnectivityNode().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getConnectivityNode_ConnectivityNodeContainer() {
        return ( EReference ) getConnectivityNode().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCombustionTurbine() {
        if( combustionTurbineEClass == null ) {
            combustionTurbineEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 185 );
        }
        return combustionTurbineEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCombustionTurbine_AmbientTemp() {
        return ( EAttribute ) getCombustionTurbine().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCombustionTurbine_AuxPowerVersusFrequency() {
        return ( EAttribute ) getCombustionTurbine().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCombustionTurbine_AuxPowerVersusVoltage() {
        return ( EAttribute ) getCombustionTurbine().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCombustionTurbine_CapabilityVersusFrequency() {
        return ( EAttribute ) getCombustionTurbine().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCombustionTurbine_HeatRecoveryFlag() {
        return ( EAttribute ) getCombustionTurbine().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCombustionTurbine_PowerVariationByTemp() {
        return ( EAttribute ) getCombustionTurbine().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCombustionTurbine_ReferenceTemp() {
        return ( EAttribute ) getCombustionTurbine().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCombustionTurbine_TimeConstant() {
        return ( EAttribute ) getCombustionTurbine().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCombustionTurbine_CTTempActivePowerCurve() {
        return ( EReference ) getCombustionTurbine().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCombustionTurbine_AirCompressor() {
        return ( EReference ) getCombustionTurbine().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCombustionTurbine_HeatRecoveryBoiler() {
        return ( EReference ) getCombustionTurbine().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getRatioTapChangerTable() {
        if( ratioTapChangerTableEClass == null ) {
            ratioTapChangerTableEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 102 );
        }
        return ratioTapChangerTableEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRatioTapChangerTable_RatioTapChanger() {
        return ( EReference ) getRatioTapChangerTable().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRatioTapChangerTable_RatioTapChangerTablePoint() {
        return ( EReference ) getRatioTapChangerTable().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getValueToAlias() {
        if( valueToAliasEClass == null ) {
            valueToAliasEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 238 );
        }
        return valueToAliasEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getValueToAlias_Value() {
        return ( EAttribute ) getValueToAlias().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getValueToAlias_ValueAliasSet() {
        return ( EReference ) getValueToAlias().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getGenUnitOpCostCurve() {
        if( genUnitOpCostCurveEClass == null ) {
            genUnitOpCostCurveEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 276 );
        }
        return genUnitOpCostCurveEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGenUnitOpCostCurve_IsNetGrossP() {
        return ( EAttribute ) getGenUnitOpCostCurve().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getGenUnitOpCostCurve_GeneratingUnit() {
        return ( EReference ) getGenUnitOpCostCurve().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPhaseTapChangerNonLinear() {
        if( phaseTapChangerNonLinearEClass == null ) {
            phaseTapChangerNonLinearEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 351 );
        }
        return phaseTapChangerNonLinearEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPhaseTapChangerNonLinear_VoltageStepIncrement() {
        return ( EAttribute ) getPhaseTapChangerNonLinear().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPhaseTapChangerNonLinear_XMax() {
        return ( EAttribute ) getPhaseTapChangerNonLinear().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPhaseTapChangerNonLinear_XMin() {
        return ( EAttribute ) getPhaseTapChangerNonLinear().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getNameTypeAuthority() {
        if( nameTypeAuthorityEClass == null ) {
            nameTypeAuthorityEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 335 );
        }
        return nameTypeAuthorityEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getNameTypeAuthority_Description() {
        return ( EAttribute ) getNameTypeAuthority().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getNameTypeAuthority_Name() {
        return ( EAttribute ) getNameTypeAuthority().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getNameTypeAuthority_NameTypes() {
        return ( EReference ) getNameTypeAuthority().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getACDCTerminal() {
        if( acdcTerminalEClass == null ) {
            acdcTerminalEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 258 );
        }
        return acdcTerminalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACDCTerminal_Connected() {
        return ( EAttribute ) getACDCTerminal().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACDCTerminal_SequenceNumber() {
        return ( EAttribute ) getACDCTerminal().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getACDCTerminal_OperationalLimitSet() {
        return ( EReference ) getACDCTerminal().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getACDCTerminal_BusNameMarker() {
        return ( EReference ) getACDCTerminal().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getACDCTerminal_Measurements() {
        return ( EReference ) getACDCTerminal().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getTextDiagramObject() {
        if( textDiagramObjectEClass == null ) {
            textDiagramObjectEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 64 );
        }
        return textDiagramObjectEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTextDiagramObject_Text() {
        return ( EAttribute ) getTextDiagramObject().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getNuclearGeneratingUnit() {
        if( nuclearGeneratingUnitEClass == null ) {
            nuclearGeneratingUnitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 268 );
        }
        return nuclearGeneratingUnitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getClamp() {
        if( clampEClass == null ) {
            clampEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 341 );
        }
        return clampEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getClamp_LengthFromTerminal1() {
        return ( EAttribute ) getClamp().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getClamp_ACLineSegment() {
        return ( EReference ) getClamp().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getApparentPowerLimit() {
        if( apparentPowerLimitEClass == null ) {
            apparentPowerLimitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 81 );
        }
        return apparentPowerLimitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getApparentPowerLimit_NormalValue() {
        return ( EAttribute ) getApparentPowerLimit().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getApparentPowerLimit_Value() {
        return ( EAttribute ) getApparentPowerLimit().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getStringMeasurementValue() {
        if( stringMeasurementValueEClass == null ) {
            stringMeasurementValueEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 221 );
        }
        return stringMeasurementValueEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStringMeasurementValue_Value() {
        return ( EAttribute ) getStringMeasurementValue().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getStringMeasurementValue_StringMeasurement() {
        return ( EReference ) getStringMeasurementValue().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEquipmentContainer() {
        if( equipmentContainerEClass == null ) {
            equipmentContainerEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 119 );
        }
        return equipmentContainerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEquipmentContainer_Equipments() {
        return ( EReference ) getEquipmentContainer().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEquipmentContainer_AdditionalGroupedEquipment() {
        return ( EReference ) getEquipmentContainer().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSubcritical() {
        if( subcriticalEClass == null ) {
            subcriticalEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 73 );
        }
        return subcriticalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getTapChangerControl() {
        if( tapChangerControlEClass == null ) {
            tapChangerControlEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 291 );
        }
        return tapChangerControlEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChangerControl_LimitVoltage() {
        return ( EAttribute ) getTapChangerControl().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChangerControl_LineDropCompensation() {
        return ( EAttribute ) getTapChangerControl().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChangerControl_LineDropR() {
        return ( EAttribute ) getTapChangerControl().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChangerControl_LineDropX() {
        return ( EAttribute ) getTapChangerControl().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChangerControl_ReverseLineDropR() {
        return ( EAttribute ) getTapChangerControl().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChangerControl_ReverseLineDropX() {
        return ( EAttribute ) getTapChangerControl().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTapChangerControl_TapChanger() {
        return ( EReference ) getTapChangerControl().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getHydroPump() {
        if( hydroPumpEClass == null ) {
            hydroPumpEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 350 );
        }
        return hydroPumpEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroPump_PumpDischAtMaxHead() {
        return ( EAttribute ) getHydroPump().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroPump_PumpDischAtMinHead() {
        return ( EAttribute ) getHydroPump().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroPump_PumpPowerAtMaxHead() {
        return ( EAttribute ) getHydroPump().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroPump_PumpPowerAtMinHead() {
        return ( EAttribute ) getHydroPump().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getHydroPump_HydroPumpOpSchedule() {
        return ( EReference ) getHydroPump().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getHydroPump_HydroPowerPlant() {
        return ( EReference ) getHydroPump().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getHydroPump_RotatingMachine() {
        return ( EReference ) getHydroPump().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getAsynchronousMachine() {
        if( asynchronousMachineEClass == null ) {
            asynchronousMachineEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 165 );
        }
        return asynchronousMachineEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAsynchronousMachine_AsynchronousMachineType() {
        return ( EAttribute ) getAsynchronousMachine().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAsynchronousMachine_ConverterFedDrive() {
        return ( EAttribute ) getAsynchronousMachine().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAsynchronousMachine_Efficiency() {
        return ( EAttribute ) getAsynchronousMachine().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAsynchronousMachine_IaIrRatio() {
        return ( EAttribute ) getAsynchronousMachine().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAsynchronousMachine_NominalFrequency() {
        return ( EAttribute ) getAsynchronousMachine().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAsynchronousMachine_NominalSpeed() {
        return ( EAttribute ) getAsynchronousMachine().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAsynchronousMachine_PolePairNumber() {
        return ( EAttribute ) getAsynchronousMachine().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAsynchronousMachine_RatedMechanicalPower() {
        return ( EAttribute ) getAsynchronousMachine().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAsynchronousMachine_Reversible() {
        return ( EAttribute ) getAsynchronousMachine().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAsynchronousMachine_Rr1() {
        return ( EAttribute ) getAsynchronousMachine().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAsynchronousMachine_Rr2() {
        return ( EAttribute ) getAsynchronousMachine().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAsynchronousMachine_RxLockedRotorRatio() {
        return ( EAttribute ) getAsynchronousMachine().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAsynchronousMachine_Tpo() {
        return ( EAttribute ) getAsynchronousMachine().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAsynchronousMachine_Tppo() {
        return ( EAttribute ) getAsynchronousMachine().getEStructuralFeatures().get( 13 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAsynchronousMachine_Xlr1() {
        return ( EAttribute ) getAsynchronousMachine().getEStructuralFeatures().get( 14 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAsynchronousMachine_Xlr2() {
        return ( EAttribute ) getAsynchronousMachine().getEStructuralFeatures().get( 15 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAsynchronousMachine_Xm() {
        return ( EAttribute ) getAsynchronousMachine().getEStructuralFeatures().get( 16 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAsynchronousMachine_Xp() {
        return ( EAttribute ) getAsynchronousMachine().getEStructuralFeatures().get( 17 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAsynchronousMachine_Xpp() {
        return ( EAttribute ) getAsynchronousMachine().getEStructuralFeatures().get( 18 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAsynchronousMachine_Xs() {
        return ( EAttribute ) getAsynchronousMachine().getEStructuralFeatures().get( 19 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEnergyConsumerPhase() {
        if( energyConsumerPhaseEClass == null ) {
            energyConsumerPhaseEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 323 );
        }
        return energyConsumerPhaseEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergyConsumerPhase_P() {
        return ( EAttribute ) getEnergyConsumerPhase().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergyConsumerPhase_Pfixed() {
        return ( EAttribute ) getEnergyConsumerPhase().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergyConsumerPhase_PfixedPct() {
        return ( EAttribute ) getEnergyConsumerPhase().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergyConsumerPhase_Phase() {
        return ( EAttribute ) getEnergyConsumerPhase().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergyConsumerPhase_Q() {
        return ( EAttribute ) getEnergyConsumerPhase().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergyConsumerPhase_Qfixed() {
        return ( EAttribute ) getEnergyConsumerPhase().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergyConsumerPhase_QfixedPct() {
        return ( EAttribute ) getEnergyConsumerPhase().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEnergyConsumerPhase_EnergyConsumer() {
        return ( EReference ) getEnergyConsumerPhase().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getContingencyElement() {
        if( contingencyElementEClass == null ) {
            contingencyElementEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 174 );
        }
        return contingencyElementEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getContingencyElement_Contingency() {
        return ( EReference ) getContingencyElement().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPowerElectronicsConnectionPhase() {
        if( powerElectronicsConnectionPhaseEClass == null ) {
            powerElectronicsConnectionPhaseEClass = ( EClass ) EPackage.Registry.INSTANCE
                    .getEPackage( CimPackage.eNS_URI ).getEClassifiers().get( 164 );
        }
        return powerElectronicsConnectionPhaseEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerElectronicsConnectionPhase_P() {
        return ( EAttribute ) getPowerElectronicsConnectionPhase().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerElectronicsConnectionPhase_Phase() {
        return ( EAttribute ) getPowerElectronicsConnectionPhase().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerElectronicsConnectionPhase_Q() {
        return ( EAttribute ) getPowerElectronicsConnectionPhase().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPowerElectronicsConnectionPhase_PowerElectronicsConnection() {
        return ( EReference ) getPowerElectronicsConnectionPhase().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSeriesCompensator() {
        if( seriesCompensatorEClass == null ) {
            seriesCompensatorEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 61 );
        }
        return seriesCompensatorEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSeriesCompensator_R() {
        return ( EAttribute ) getSeriesCompensator().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSeriesCompensator_R0() {
        return ( EAttribute ) getSeriesCompensator().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSeriesCompensator_VaristorPresent() {
        return ( EAttribute ) getSeriesCompensator().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSeriesCompensator_VaristorRatedCurrent() {
        return ( EAttribute ) getSeriesCompensator().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSeriesCompensator_VaristorVoltageThreshold() {
        return ( EAttribute ) getSeriesCompensator().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSeriesCompensator_X() {
        return ( EAttribute ) getSeriesCompensator().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSeriesCompensator_X0() {
        return ( EAttribute ) getSeriesCompensator().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDiagramObjectPoint() {
        if( diagramObjectPointEClass == null ) {
            diagramObjectPointEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 141 );
        }
        return diagramObjectPointEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDiagramObjectPoint_SequenceNumber() {
        return ( EAttribute ) getDiagramObjectPoint().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDiagramObjectPoint_XPosition() {
        return ( EAttribute ) getDiagramObjectPoint().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDiagramObjectPoint_YPosition() {
        return ( EAttribute ) getDiagramObjectPoint().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDiagramObjectPoint_ZPosition() {
        return ( EAttribute ) getDiagramObjectPoint().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDiagramObjectPoint_DiagramObjectGluePoint() {
        return ( EReference ) getDiagramObjectPoint().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDiagramObjectPoint_DiagramObject() {
        return ( EReference ) getDiagramObjectPoint().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEquivalentEquipment() {
        if( equivalentEquipmentEClass == null ) {
            equivalentEquipmentEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 125 );
        }
        return equivalentEquipmentEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEquivalentEquipment_EquivalentNetwork() {
        return ( EReference ) getEquivalentEquipment().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEquivalentShunt() {
        if( equivalentShuntEClass == null ) {
            equivalentShuntEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 232 );
        }
        return equivalentShuntEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentShunt_B() {
        return ( EAttribute ) getEquivalentShunt().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentShunt_G() {
        return ( EAttribute ) getEquivalentShunt().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEnergyConsumer() {
        if( energyConsumerEClass == null ) {
            energyConsumerEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 293 );
        }
        return energyConsumerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergyConsumer_CustomerCount() {
        return ( EAttribute ) getEnergyConsumer().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergyConsumer_Grounded() {
        return ( EAttribute ) getEnergyConsumer().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergyConsumer_P() {
        return ( EAttribute ) getEnergyConsumer().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergyConsumer_Pfixed() {
        return ( EAttribute ) getEnergyConsumer().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergyConsumer_PfixedPct() {
        return ( EAttribute ) getEnergyConsumer().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergyConsumer_PhaseConnection() {
        return ( EAttribute ) getEnergyConsumer().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergyConsumer_Q() {
        return ( EAttribute ) getEnergyConsumer().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergyConsumer_Qfixed() {
        return ( EAttribute ) getEnergyConsumer().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergyConsumer_QfixedPct() {
        return ( EAttribute ) getEnergyConsumer().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEnergyConsumer_LoadResponse() {
        return ( EReference ) getEnergyConsumer().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEnergyConsumer_EnergyConsumerPhase() {
        return ( EReference ) getEnergyConsumer().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEnergyConsumer_PowerCutZone() {
        return ( EReference ) getEnergyConsumer().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getOperationalLimitSet() {
        if( operationalLimitSetEClass == null ) {
            operationalLimitSetEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 277 );
        }
        return operationalLimitSetEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getOperationalLimitSet_Terminal() {
        return ( EReference ) getOperationalLimitSet().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getOperationalLimitSet_Equipment() {
        return ( EReference ) getOperationalLimitSet().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getOperationalLimitSet_OperationalLimitValue() {
        return ( EReference ) getOperationalLimitSet().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPowerCutZone() {
        if( powerCutZoneEClass == null ) {
            powerCutZoneEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 93 );
        }
        return powerCutZoneEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerCutZone_CutLevel1() {
        return ( EAttribute ) getPowerCutZone().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerCutZone_CutLevel2() {
        return ( EAttribute ) getPowerCutZone().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPowerCutZone_EnergyConsumers() {
        return ( EReference ) getPowerCutZone().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCurve() {
        if( curveEClass == null ) {
            curveEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 328 );
        }
        return curveEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurve_CurveStyle() {
        return ( EAttribute ) getCurve().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurve_XMultiplier() {
        return ( EAttribute ) getCurve().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurve_XUnit() {
        return ( EAttribute ) getCurve().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurve_Y1Multiplier() {
        return ( EAttribute ) getCurve().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurve_Y1Unit() {
        return ( EAttribute ) getCurve().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurve_Y2Multiplier() {
        return ( EAttribute ) getCurve().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurve_Y2Unit() {
        return ( EAttribute ) getCurve().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurve_Y3Multiplier() {
        return ( EAttribute ) getCurve().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurve_Y3Unit() {
        return ( EAttribute ) getCurve().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCurve_CurveDatas() {
        return ( EReference ) getCurve().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getNonConformLoadSchedule() {
        if( nonConformLoadScheduleEClass == null ) {
            nonConformLoadScheduleEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 219 );
        }
        return nonConformLoadScheduleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getNonConformLoadSchedule_NonConformLoadGroup() {
        return ( EReference ) getNonConformLoadSchedule().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getTransformerEnd() {
        if( transformerEndEClass == null ) {
            transformerEndEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 322 );
        }
        return transformerEndEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTransformerEnd_BmagSat() {
        return ( EAttribute ) getTransformerEnd().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTransformerEnd_EndNumber() {
        return ( EAttribute ) getTransformerEnd().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTransformerEnd_Grounded() {
        return ( EAttribute ) getTransformerEnd().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTransformerEnd_MagBaseU() {
        return ( EAttribute ) getTransformerEnd().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTransformerEnd_MagSatFlux() {
        return ( EAttribute ) getTransformerEnd().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTransformerEnd_Rground() {
        return ( EAttribute ) getTransformerEnd().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTransformerEnd_Xground() {
        return ( EAttribute ) getTransformerEnd().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTransformerEnd_RatioTapChanger() {
        return ( EReference ) getTransformerEnd().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTransformerEnd_ToMeshImpedance() {
        return ( EReference ) getTransformerEnd().getEStructuralFeatures().get( 14 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTransformerEnd_PhaseTapChanger() {
        return ( EReference ) getTransformerEnd().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTransformerEnd_CoreAdmittance() {
        return ( EReference ) getTransformerEnd().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTransformerEnd_StarImpedance() {
        return ( EReference ) getTransformerEnd().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTransformerEnd_Terminal() {
        return ( EReference ) getTransformerEnd().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTransformerEnd_FromMeshImpedance() {
        return ( EReference ) getTransformerEnd().getEStructuralFeatures().get( 13 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTransformerEnd_BaseVoltage() {
        return ( EReference ) getTransformerEnd().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getTargetLevelSchedule() {
        if( targetLevelScheduleEClass == null ) {
            targetLevelScheduleEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 90 );
        }
        return targetLevelScheduleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTargetLevelSchedule_HighLevelLimit() {
        return ( EAttribute ) getTargetLevelSchedule().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTargetLevelSchedule_LowLevelLimit() {
        return ( EAttribute ) getTargetLevelSchedule().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTargetLevelSchedule_Reservoir() {
        return ( EReference ) getTargetLevelSchedule().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getVoltageLimit() {
        if( voltageLimitEClass == null ) {
            voltageLimitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 138 );
        }
        return voltageLimitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getVoltageLimit_NormalValue() {
        return ( EAttribute ) getVoltageLimit().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getVoltageLimit_Value() {
        return ( EAttribute ) getVoltageLimit().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getOperatingParticipant() {
        if( operatingParticipantEClass == null ) {
            operatingParticipantEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 71 );
        }
        return operatingParticipantEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getOperatingParticipant_OperatingShare() {
        return ( EReference ) getOperatingParticipant().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCurrentLimit() {
        if( currentLimitEClass == null ) {
            currentLimitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 76 );
        }
        return currentLimitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurrentLimit_NormalValue() {
        return ( EAttribute ) getCurrentLimit().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurrentLimit_Value() {
        return ( EAttribute ) getCurrentLimit().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getAltGeneratingUnitMeas() {
        if( altGeneratingUnitMeasEClass == null ) {
            altGeneratingUnitMeasEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 162 );
        }
        return altGeneratingUnitMeasEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAltGeneratingUnitMeas_Priority() {
        return ( EAttribute ) getAltGeneratingUnitMeas().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAltGeneratingUnitMeas_ControlAreaGeneratingUnit() {
        return ( EReference ) getAltGeneratingUnitMeas().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAltGeneratingUnitMeas_AnalogValue() {
        return ( EReference ) getAltGeneratingUnitMeas().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPrimeMover() {
        if( primeMoverEClass == null ) {
            primeMoverEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 342 );
        }
        return primeMoverEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPrimeMover_PrimeMoverRating() {
        return ( EAttribute ) getPrimeMover().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPrimeMover_SynchronousMachines() {
        return ( EReference ) getPrimeMover().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPhaseTapChangerTabular() {
        if( phaseTapChangerTabularEClass == null ) {
            phaseTapChangerTabularEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 130 );
        }
        return phaseTapChangerTabularEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPhaseTapChangerTabular_PhaseTapChangerTable() {
        return ( EReference ) getPhaseTapChangerTabular().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getTopologicalNode() {
        if( topologicalNodeEClass == null ) {
            topologicalNodeEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 216 );
        }
        return topologicalNodeEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTopologicalNode_PInjection() {
        return ( EAttribute ) getTopologicalNode().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTopologicalNode_QInjection() {
        return ( EAttribute ) getTopologicalNode().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTopologicalNode_SvInjection() {
        return ( EReference ) getTopologicalNode().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTopologicalNode_ReportingGroup() {
        return ( EReference ) getTopologicalNode().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTopologicalNode_TopologicalIsland() {
        return ( EReference ) getTopologicalNode().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTopologicalNode_AngleRefTopologicalIsland() {
        return ( EReference ) getTopologicalNode().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTopologicalNode_ConnectivityNodes() {
        return ( EReference ) getTopologicalNode().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTopologicalNode_ConnectivityNodeContainer() {
        return ( EReference ) getTopologicalNode().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTopologicalNode_Terminal() {
        return ( EReference ) getTopologicalNode().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTopologicalNode_SvVoltage() {
        return ( EReference ) getTopologicalNode().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTopologicalNode_BusNameMarker() {
        return ( EReference ) getTopologicalNode().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTopologicalNode_BaseVoltage() {
        return ( EReference ) getTopologicalNode().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getStringMeasurement() {
        if( stringMeasurementEClass == null ) {
            stringMeasurementEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 69 );
        }
        return stringMeasurementEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getStringMeasurement_StringMeasurementValues() {
        return ( EReference ) getStringMeasurement().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEnergyArea() {
        if( energyAreaEClass == null ) {
            energyAreaEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 151 );
        }
        return energyAreaEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEnergyArea_ControlArea() {
        return ( EReference ) getEnergyArea().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCogenerationPlant() {
        if( cogenerationPlantEClass == null ) {
            cogenerationPlantEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 178 );
        }
        return cogenerationPlantEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCogenerationPlant_CogenHPSendoutRating() {
        return ( EAttribute ) getCogenerationPlant().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCogenerationPlant_CogenHPSteamRating() {
        return ( EAttribute ) getCogenerationPlant().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCogenerationPlant_CogenLPSendoutRating() {
        return ( EAttribute ) getCogenerationPlant().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCogenerationPlant_CogenLPSteamRating() {
        return ( EAttribute ) getCogenerationPlant().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCogenerationPlant_RatedP() {
        return ( EAttribute ) getCogenerationPlant().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCogenerationPlant_SteamSendoutSchedule() {
        return ( EReference ) getCogenerationPlant().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCogenerationPlant_ThermalGeneratingUnits() {
        return ( EReference ) getCogenerationPlant().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getGenUnitOpSchedule() {
        if( genUnitOpScheduleEClass == null ) {
            genUnitOpScheduleEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 261 );
        }
        return genUnitOpScheduleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getGenUnitOpSchedule_GeneratingUnit() {
        return ( EReference ) getGenUnitOpSchedule().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getNonConformLoadGroup() {
        if( nonConformLoadGroupEClass == null ) {
            nonConformLoadGroupEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 222 );
        }
        return nonConformLoadGroupEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getNonConformLoadGroup_NonConformLoadSchedules() {
        return ( EReference ) getNonConformLoadGroup().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getNonConformLoadGroup_EnergyConsumers() {
        return ( EReference ) getNonConformLoadGroup().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getRaiseLowerCommand() {
        if( raiseLowerCommandEClass == null ) {
            raiseLowerCommandEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 87 );
        }
        return raiseLowerCommandEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRaiseLowerCommand_ValueAliasSet() {
        return ( EReference ) getRaiseLowerCommand().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getBasicIntervalSchedule() {
        if( basicIntervalScheduleEClass == null ) {
            basicIntervalScheduleEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 155 );
        }
        return basicIntervalScheduleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBasicIntervalSchedule_StartTime() {
        return ( EAttribute ) getBasicIntervalSchedule().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBasicIntervalSchedule_Value1Multiplier() {
        return ( EAttribute ) getBasicIntervalSchedule().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBasicIntervalSchedule_Value1Unit() {
        return ( EAttribute ) getBasicIntervalSchedule().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBasicIntervalSchedule_Value2Multiplier() {
        return ( EAttribute ) getBasicIntervalSchedule().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBasicIntervalSchedule_Value2Unit() {
        return ( EAttribute ) getBasicIntervalSchedule().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getOperationalLimit() {
        if( operationalLimitEClass == null ) {
            operationalLimitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 327 );
        }
        return operationalLimitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getOperationalLimit_OperationalLimitSet() {
        return ( EReference ) getOperationalLimit().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getOperationalLimit_OperationalLimitType() {
        return ( EReference ) getOperationalLimit().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getStationSupply() {
        if( stationSupplyEClass == null ) {
            stationSupplyEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 193 );
        }
        return stationSupplyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSubGeographicalRegion() {
        if( subGeographicalRegionEClass == null ) {
            subGeographicalRegionEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 294 );
        }
        return subGeographicalRegionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSubGeographicalRegion_Region() {
        return ( EReference ) getSubGeographicalRegion().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSubGeographicalRegion_Substations() {
        return ( EReference ) getSubGeographicalRegion().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSubGeographicalRegion_Lines() {
        return ( EReference ) getSubGeographicalRegion().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSubGeographicalRegion_DCLines() {
        return ( EReference ) getSubGeographicalRegion().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getHydroTurbine() {
        if( hydroTurbineEClass == null ) {
            hydroTurbineEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 187 );
        }
        return hydroTurbineEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroTurbine_GateRateLimit() {
        return ( EAttribute ) getHydroTurbine().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroTurbine_GateUpperLimit() {
        return ( EAttribute ) getHydroTurbine().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroTurbine_MaxHeadMaxP() {
        return ( EAttribute ) getHydroTurbine().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroTurbine_MinHeadMaxP() {
        return ( EAttribute ) getHydroTurbine().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroTurbine_SpeedRating() {
        return ( EAttribute ) getHydroTurbine().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroTurbine_SpeedRegulation() {
        return ( EAttribute ) getHydroTurbine().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroTurbine_TransientDroopTime() {
        return ( EAttribute ) getHydroTurbine().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroTurbine_TransientRegulation() {
        return ( EAttribute ) getHydroTurbine().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroTurbine_TurbineRating() {
        return ( EAttribute ) getHydroTurbine().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroTurbine_TurbineType() {
        return ( EAttribute ) getHydroTurbine().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroTurbine_WaterStartingTime() {
        return ( EAttribute ) getHydroTurbine().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getACDCConverterDCTerminal() {
        if( acdcConverterDCTerminalEClass == null ) {
            acdcConverterDCTerminalEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 239 );
        }
        return acdcConverterDCTerminalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACDCConverterDCTerminal_Polarity() {
        return ( EAttribute ) getACDCConverterDCTerminal().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getACDCConverterDCTerminal_DCConductingEquipment() {
        return ( EReference ) getACDCConverterDCTerminal().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getTransformerTankEnd() {
        if( transformerTankEndEClass == null ) {
            transformerTankEndEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 203 );
        }
        return transformerTankEndEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTransformerTankEnd_Phases() {
        return ( EAttribute ) getTransformerTankEnd().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTransformerTankEnd_TransformerTank() {
        return ( EReference ) getTransformerTankEnd().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getNameType() {
        if( nameTypeEClass == null ) {
            nameTypeEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 310 );
        }
        return nameTypeEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getNameType_Description() {
        return ( EAttribute ) getNameType().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getNameType_Name() {
        return ( EAttribute ) getNameType().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getNameType_NameTypeAuthority() {
        return ( EReference ) getNameType().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getNameType_Names() {
        return ( EReference ) getNameType().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getRegulatingControl() {
        if( regulatingControlEClass == null ) {
            regulatingControlEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 320 );
        }
        return regulatingControlEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRegulatingControl_Discrete() {
        return ( EAttribute ) getRegulatingControl().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRegulatingControl_Enabled() {
        return ( EAttribute ) getRegulatingControl().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRegulatingControl_Mode() {
        return ( EAttribute ) getRegulatingControl().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRegulatingControl_MonitoredPhase() {
        return ( EAttribute ) getRegulatingControl().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRegulatingControl_TargetDeadband() {
        return ( EAttribute ) getRegulatingControl().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRegulatingControl_TargetValue() {
        return ( EAttribute ) getRegulatingControl().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRegulatingControl_TargetValueUnitMultiplier() {
        return ( EAttribute ) getRegulatingControl().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRegulatingControl_RegulatingCondEq() {
        return ( EReference ) getRegulatingControl().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRegulatingControl_Terminal() {
        return ( EReference ) getRegulatingControl().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRegulatingControl_RegulationSchedule() {
        return ( EReference ) getRegulatingControl().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getReportingGroup() {
        if( reportingGroupEClass == null ) {
            reportingGroupEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 240 );
        }
        return reportingGroupEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getReportingGroup_ReportingSuperGroup() {
        return ( EReference ) getReportingGroup().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getReportingGroup_PowerSystemResource() {
        return ( EReference ) getReportingGroup().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getReportingGroup_BusNameMarker() {
        return ( EReference ) getReportingGroup().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getReportingGroup_TopologicalNode() {
        return ( EReference ) getReportingGroup().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getAltTieMeas() {
        if( altTieMeasEClass == null ) {
            altTieMeasEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 303 );
        }
        return altTieMeasEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAltTieMeas_Priority() {
        return ( EAttribute ) getAltTieMeas().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAltTieMeas_TieFlow() {
        return ( EReference ) getAltTieMeas().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAltTieMeas_AnalogValue() {
        return ( EReference ) getAltTieMeas().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEnergySourcePhase() {
        if( energySourcePhaseEClass == null ) {
            energySourcePhaseEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 86 );
        }
        return energySourcePhaseEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergySourcePhase_Phase() {
        return ( EAttribute ) getEnergySourcePhase().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEnergySourcePhase_EnergySource() {
        return ( EReference ) getEnergySourcePhase().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCommunicationLink() {
        if( communicationLinkEClass == null ) {
            communicationLinkEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 212 );
        }
        return communicationLinkEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCommunicationLink_RemoteUnits() {
        return ( EReference ) getCommunicationLink().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getNonlinearShuntCompensatorPoint() {
        if( nonlinearShuntCompensatorPointEClass == null ) {
            nonlinearShuntCompensatorPointEClass = ( EClass ) EPackage.Registry.INSTANCE
                    .getEPackage( CimPackage.eNS_URI ).getEClassifiers().get( 135 );
        }
        return nonlinearShuntCompensatorPointEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getNonlinearShuntCompensatorPoint_B() {
        return ( EAttribute ) getNonlinearShuntCompensatorPoint().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getNonlinearShuntCompensatorPoint_B0() {
        return ( EAttribute ) getNonlinearShuntCompensatorPoint().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getNonlinearShuntCompensatorPoint_G() {
        return ( EAttribute ) getNonlinearShuntCompensatorPoint().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getNonlinearShuntCompensatorPoint_G0() {
        return ( EAttribute ) getNonlinearShuntCompensatorPoint().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getNonlinearShuntCompensatorPoint_SectionNumber() {
        return ( EAttribute ) getNonlinearShuntCompensatorPoint().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getNonlinearShuntCompensatorPoint_NonlinearShuntCompensator() {
        return ( EReference ) getNonlinearShuntCompensatorPoint().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSynchronousMachine() {
        if( synchronousMachineEClass == null ) {
            synchronousMachineEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 101 );
        }
        return synchronousMachineEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_AVRToManualLag() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_AVRToManualLead() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_BaseQ() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_CondenserP() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_CoolantCondition() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_CoolantType() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_Earthing() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_EarthingStarPointR() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_EarthingStarPointX() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_Ikk() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_ManualToAVR() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_MaxQ() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_MaxU() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_MinQ() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 13 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_MinU() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 14 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_Mu() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 15 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_OperatingMode() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 16 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_QPercent() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 17 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_R() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 18 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_R0() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 19 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_R2() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 20 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_ReferencePriority() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 21 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_SatDirectSubtransX() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 22 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_SatDirectSyncX() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 23 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_SatDirectTransX() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 24 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_ShortCircuitRotorType() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 25 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_Type() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 26 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_VoltageRegulationRange() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 27 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_X0() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 28 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchronousMachine_X2() {
        return ( EAttribute ) getSynchronousMachine().getEStructuralFeatures().get( 29 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSynchronousMachine_PrimeMovers() {
        return ( EReference ) getSynchronousMachine().getEStructuralFeatures().get( 30 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSynchronousMachine_InitialReactiveCapabilityCurve() {
        return ( EReference ) getSynchronousMachine().getEStructuralFeatures().get( 32 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSynchronousMachine_ReactiveCapabilityCurves() {
        return ( EReference ) getSynchronousMachine().getEStructuralFeatures().get( 31 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getHeatRateCurve() {
        if( heatRateCurveEClass == null ) {
            heatRateCurveEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 121 );
        }
        return heatRateCurveEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHeatRateCurve_IsNetGrossP() {
        return ( EAttribute ) getHeatRateCurve().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getHeatRateCurve_ThermalGeneratingUnit() {
        return ( EReference ) getHeatRateCurve().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getBaseVoltage() {
        if( baseVoltageEClass == null ) {
            baseVoltageEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 95 );
        }
        return baseVoltageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBaseVoltage_NominalVoltage() {
        return ( EAttribute ) getBaseVoltage().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getBaseVoltage_TransformerEnds() {
        return ( EReference ) getBaseVoltage().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getBaseVoltage_TopologicalNode() {
        return ( EReference ) getBaseVoltage().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getBaseVoltage_VoltageLevel() {
        return ( EReference ) getBaseVoltage().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getBaseVoltage_ConductingEquipment() {
        return ( EReference ) getBaseVoltage().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getVsConverter() {
        if( vsConverterEClass == null ) {
            vsConverterEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 59 );
        }
        return vsConverterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getVsConverter_Delta() {
        return ( EAttribute ) getVsConverter().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getVsConverter_Droop() {
        return ( EAttribute ) getVsConverter().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getVsConverter_DroopCompensation() {
        return ( EAttribute ) getVsConverter().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getVsConverter_MaxModulationIndex() {
        return ( EAttribute ) getVsConverter().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getVsConverter_MaxValveCurrent() {
        return ( EAttribute ) getVsConverter().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getVsConverter_PPccControl() {
        return ( EAttribute ) getVsConverter().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getVsConverter_QPccControl() {
        return ( EAttribute ) getVsConverter().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getVsConverter_QShare() {
        return ( EAttribute ) getVsConverter().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getVsConverter_TargetQpcc() {
        return ( EAttribute ) getVsConverter().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getVsConverter_TargetUpcc() {
        return ( EAttribute ) getVsConverter().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getVsConverter_Uv() {
        return ( EAttribute ) getVsConverter().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getVsConverter_CapabilityCurve() {
        return ( EReference ) getVsConverter().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEnergyConnection() {
        if( energyConnectionEClass == null ) {
            energyConnectionEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 344 );
        }
        return energyConnectionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSwitchPhase() {
        if( switchPhaseEClass == null ) {
            switchPhaseEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 255 );
        }
        return switchPhaseEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSwitchPhase_Closed() {
        return ( EAttribute ) getSwitchPhase().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSwitchPhase_NormalOpen() {
        return ( EAttribute ) getSwitchPhase().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSwitchPhase_PhaseSide1() {
        return ( EAttribute ) getSwitchPhase().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSwitchPhase_PhaseSide2() {
        return ( EAttribute ) getSwitchPhase().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSwitchPhase_RatedCurrent() {
        return ( EAttribute ) getSwitchPhase().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSwitchPhase_Switch() {
        return ( EReference ) getSwitchPhase().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getConformLoadGroup() {
        if( conformLoadGroupEClass == null ) {
            conformLoadGroupEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 94 );
        }
        return conformLoadGroupEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getConformLoadGroup_EnergyConsumers() {
        return ( EReference ) getConformLoadGroup().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getConformLoadGroup_ConformLoadSchedules() {
        return ( EReference ) getConformLoadGroup().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDiagramObject() {
        if( diagramObjectEClass == null ) {
            diagramObjectEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 346 );
        }
        return diagramObjectEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDiagramObject_DrawingOrder() {
        return ( EAttribute ) getDiagramObject().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDiagramObject_IsPolygon() {
        return ( EAttribute ) getDiagramObject().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDiagramObject_OffsetX() {
        return ( EAttribute ) getDiagramObject().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDiagramObject_OffsetY() {
        return ( EAttribute ) getDiagramObject().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDiagramObject_Rotation() {
        return ( EAttribute ) getDiagramObject().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDiagramObject_DiagramObjectPoints() {
        return ( EReference ) getDiagramObject().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDiagramObject_IdentifiedObject() {
        return ( EReference ) getDiagramObject().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDiagramObject_DiagramObjectStyle() {
        return ( EReference ) getDiagramObject().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDiagramObject_Diagram() {
        return ( EReference ) getDiagramObject().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDiagramObject_VisibilityLayers() {
        return ( EReference ) getDiagramObject().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getHydroGeneratingUnit() {
        if( hydroGeneratingUnitEClass == null ) {
            hydroGeneratingUnitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 144 );
        }
        return hydroGeneratingUnitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroGeneratingUnit_DropHeight() {
        return ( EAttribute ) getHydroGeneratingUnit().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroGeneratingUnit_EnergyConversionCapability() {
        return ( EAttribute ) getHydroGeneratingUnit().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroGeneratingUnit_HydroUnitWaterCost() {
        return ( EAttribute ) getHydroGeneratingUnit().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroGeneratingUnit_TurbineType() {
        return ( EAttribute ) getHydroGeneratingUnit().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getHydroGeneratingUnit_HydroPowerPlant() {
        return ( EReference ) getHydroGeneratingUnit().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getHydroGeneratingUnit_TailbayLossCurve() {
        return ( EReference ) getHydroGeneratingUnit().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getHydroGeneratingUnit_HydroGeneratingEfficiencyCurves() {
        return ( EReference ) getHydroGeneratingUnit().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getHydroGeneratingUnit_PenstockLossCurve() {
        return ( EReference ) getHydroGeneratingUnit().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDisconnector() {
        if( disconnectorEClass == null ) {
            disconnectorEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 158 );
        }
        return disconnectorEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPerLengthImpedance() {
        if( perLengthImpedanceEClass == null ) {
            perLengthImpedanceEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 184 );
        }
        return perLengthImpedanceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPerLengthImpedance_ACLineSegments() {
        return ( EReference ) getPerLengthImpedance().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSubstation() {
        if( substationEClass == null ) {
            substationEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 233 );
        }
        return substationEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSubstation_DCConverterUnit() {
        return ( EReference ) getSubstation().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSubstation_NamingFeeder() {
        return ( EReference ) getSubstation().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSubstation_Bays() {
        return ( EReference ) getSubstation().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSubstation_VoltageLevels() {
        return ( EReference ) getSubstation().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSubstation_Region() {
        return ( EReference ) getSubstation().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSubstation_NormalEnergizedFeeder() {
        return ( EReference ) getSubstation().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSubstation_NormalEnergizingFeeder() {
        return ( EReference ) getSubstation().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDiagramObjectStyle() {
        if( diagramObjectStyleEClass == null ) {
            diagramObjectStyleEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 309 );
        }
        return diagramObjectStyleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDiagramObjectStyle_StyledObjects() {
        return ( EReference ) getDiagramObjectStyle().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCommand() {
        if( commandEClass == null ) {
            commandEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 143 );
        }
        return commandEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCommand_NormalValue() {
        return ( EAttribute ) getCommand().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCommand_Value() {
        return ( EAttribute ) getCommand().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCommand_ValueAliasSet() {
        return ( EReference ) getCommand().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCommand_DiscreteValue() {
        return ( EReference ) getCommand().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getShuntCompensator() {
        if( shuntCompensatorEClass == null ) {
            shuntCompensatorEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 84 );
        }
        return shuntCompensatorEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getShuntCompensator_AVRDelay() {
        return ( EAttribute ) getShuntCompensator().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getShuntCompensator_Grounded() {
        return ( EAttribute ) getShuntCompensator().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getShuntCompensator_MaximumSections() {
        return ( EAttribute ) getShuntCompensator().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getShuntCompensator_NomU() {
        return ( EAttribute ) getShuntCompensator().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getShuntCompensator_NormalSections() {
        return ( EAttribute ) getShuntCompensator().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getShuntCompensator_PhaseConnection() {
        return ( EAttribute ) getShuntCompensator().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getShuntCompensator_Sections() {
        return ( EAttribute ) getShuntCompensator().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getShuntCompensator_SwitchOnCount() {
        return ( EAttribute ) getShuntCompensator().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getShuntCompensator_SwitchOnDate() {
        return ( EAttribute ) getShuntCompensator().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getShuntCompensator_VoltageSensitivity() {
        return ( EAttribute ) getShuntCompensator().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getShuntCompensator_ShuntCompensatorPhase() {
        return ( EReference ) getShuntCompensator().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getShuntCompensator_SvShuntCompensatorSections() {
        return ( EReference ) getShuntCompensator().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getShuntCompensatorPhase() {
        if( shuntCompensatorPhaseEClass == null ) {
            shuntCompensatorPhaseEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 248 );
        }
        return shuntCompensatorPhaseEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getShuntCompensatorPhase_MaximumSections() {
        return ( EAttribute ) getShuntCompensatorPhase().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getShuntCompensatorPhase_NormalSections() {
        return ( EAttribute ) getShuntCompensatorPhase().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getShuntCompensatorPhase_Phase() {
        return ( EAttribute ) getShuntCompensatorPhase().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getShuntCompensatorPhase_ShuntCompensator() {
        return ( EReference ) getShuntCompensatorPhase().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getRecloseSequence() {
        if( recloseSequenceEClass == null ) {
            recloseSequenceEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 194 );
        }
        return recloseSequenceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRecloseSequence_RecloseDelay() {
        return ( EAttribute ) getRecloseSequence().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRecloseSequence_RecloseStep() {
        return ( EAttribute ) getRecloseSequence().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRecloseSequence_ProtectedSwitch() {
        return ( EReference ) getRecloseSequence().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getVoltageControlZone() {
        if( voltageControlZoneEClass == null ) {
            voltageControlZoneEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 345 );
        }
        return voltageControlZoneEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getVoltageControlZone_BusbarSection() {
        return ( EReference ) getVoltageControlZone().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getVoltageControlZone_RegulationSchedule() {
        return ( EReference ) getVoltageControlZone().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSteamSupply() {
        if( steamSupplyEClass == null ) {
            steamSupplyEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 123 );
        }
        return steamSupplyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSteamSupply_SteamSupplyRating() {
        return ( EAttribute ) getSteamSupply().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSteamSupply_SteamTurbines() {
        return ( EReference ) getSteamSupply().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getMeasurementValueQuality() {
        if( measurementValueQualityEClass == null ) {
            measurementValueQualityEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 137 );
        }
        return measurementValueQualityEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getMeasurementValueQuality_MeasurementValue() {
        return ( EReference ) getMeasurementValueQuality().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEquipmentFault() {
        if( equipmentFaultEClass == null ) {
            equipmentFaultEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 296 );
        }
        return equipmentFaultEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEquipmentFault_Terminal() {
        return ( EReference ) getEquipmentFault().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDCEquipmentContainer() {
        if( dcEquipmentContainerEClass == null ) {
            dcEquipmentContainerEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 308 );
        }
        return dcEquipmentContainerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDCEquipmentContainer_DCTopologicalNode() {
        return ( EReference ) getDCEquipmentContainer().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDCEquipmentContainer_DCNodes() {
        return ( EReference ) getDCEquipmentContainer().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getHeatRecoveryBoiler() {
        if( heatRecoveryBoilerEClass == null ) {
            heatRecoveryBoilerEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 299 );
        }
        return heatRecoveryBoilerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHeatRecoveryBoiler_SteamSupplyRating2() {
        return ( EAttribute ) getHeatRecoveryBoiler().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getHeatRecoveryBoiler_CombustionTurbines() {
        return ( EReference ) getHeatRecoveryBoiler().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEnergySchedulingType() {
        if( energySchedulingTypeEClass == null ) {
            energySchedulingTypeEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 213 );
        }
        return energySchedulingTypeEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEnergySchedulingType_EnergySource() {
        return ( EReference ) getEnergySchedulingType().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getContingencyEquipment() {
        if( contingencyEquipmentEClass == null ) {
            contingencyEquipmentEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 326 );
        }
        return contingencyEquipmentEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getContingencyEquipment_ContingentStatus() {
        return ( EAttribute ) getContingencyEquipment().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getContingencyEquipment_Equipment() {
        return ( EReference ) getContingencyEquipment().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getConformLoadSchedule() {
        if( conformLoadScheduleEClass == null ) {
            conformLoadScheduleEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 220 );
        }
        return conformLoadScheduleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getConformLoadSchedule_ConformLoadGroup() {
        return ( EReference ) getConformLoadSchedule().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getRegulationSchedule() {
        if( regulationScheduleEClass == null ) {
            regulationScheduleEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 340 );
        }
        return regulationScheduleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRegulationSchedule_RegulatingControl() {
        return ( EReference ) getRegulationSchedule().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRegulationSchedule_VoltageControlZones() {
        return ( EReference ) getRegulationSchedule().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getTailbayLossCurve() {
        if( tailbayLossCurveEClass == null ) {
            tailbayLossCurveEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 110 );
        }
        return tailbayLossCurveEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTailbayLossCurve_HydroGeneratingUnit() {
        return ( EReference ) getTailbayLossCurve().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getIdentifiedObject() {
        if( identifiedObjectEClass == null ) {
            identifiedObjectEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 266 );
        }
        return identifiedObjectEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getIdentifiedObject_AliasName() {
        return ( EAttribute ) getIdentifiedObject().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getIdentifiedObject_Description() {
        return ( EAttribute ) getIdentifiedObject().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getIdentifiedObject_MRID() {
        return ( EAttribute ) getIdentifiedObject().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getIdentifiedObject_Name() {
        return ( EAttribute ) getIdentifiedObject().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getIdentifiedObject_DiagramObjects() {
        return ( EReference ) getIdentifiedObject().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getIdentifiedObject_Names() {
        return ( EReference ) getIdentifiedObject().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getGroundingImpedance() {
        if( groundingImpedanceEClass == null ) {
            groundingImpedanceEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 218 );
        }
        return groundingImpedanceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGroundingImpedance_X() {
        return ( EAttribute ) getGroundingImpedance().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPowerElectronicsConnection() {
        if( powerElectronicsConnectionEClass == null ) {
            powerElectronicsConnectionEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 286 );
        }
        return powerElectronicsConnectionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerElectronicsConnection_MaxIFault() {
        return ( EAttribute ) getPowerElectronicsConnection().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerElectronicsConnection_MaxQ() {
        return ( EAttribute ) getPowerElectronicsConnection().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerElectronicsConnection_MinQ() {
        return ( EAttribute ) getPowerElectronicsConnection().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerElectronicsConnection_P() {
        return ( EAttribute ) getPowerElectronicsConnection().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerElectronicsConnection_Q() {
        return ( EAttribute ) getPowerElectronicsConnection().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerElectronicsConnection_R() {
        return ( EAttribute ) getPowerElectronicsConnection().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerElectronicsConnection_R0() {
        return ( EAttribute ) getPowerElectronicsConnection().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerElectronicsConnection_RatedS() {
        return ( EAttribute ) getPowerElectronicsConnection().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerElectronicsConnection_RatedU() {
        return ( EAttribute ) getPowerElectronicsConnection().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerElectronicsConnection_Rn() {
        return ( EAttribute ) getPowerElectronicsConnection().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerElectronicsConnection_X() {
        return ( EAttribute ) getPowerElectronicsConnection().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerElectronicsConnection_X0() {
        return ( EAttribute ) getPowerElectronicsConnection().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerElectronicsConnection_Xn() {
        return ( EAttribute ) getPowerElectronicsConnection().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPowerElectronicsConnection_PowerElectronicsConnectionPhase() {
        return ( EReference ) getPowerElectronicsConnection().getEStructuralFeatures().get( 13 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPowerElectronicsConnection_PowerElectronicsUnit() {
        return ( EReference ) getPowerElectronicsConnection().getEStructuralFeatures().get( 14 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getGround() {
        if( groundEClass == null ) {
            groundEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 247 );
        }
        return groundEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDiagram() {
        if( diagramEClass == null ) {
            diagramEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 153 );
        }
        return diagramEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDiagram_Orientation() {
        return ( EAttribute ) getDiagram().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDiagram_X1InitialView() {
        return ( EAttribute ) getDiagram().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDiagram_X2InitialView() {
        return ( EAttribute ) getDiagram().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDiagram_Y1InitialView() {
        return ( EAttribute ) getDiagram().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDiagram_Y2InitialView() {
        return ( EAttribute ) getDiagram().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDiagram_DiagramStyle() {
        return ( EReference ) getDiagram().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDiagram_DiagramElements() {
        return ( EReference ) getDiagram().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSvVoltage() {
        if( svVoltageEClass == null ) {
            svVoltageEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 176 );
        }
        return svVoltageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSvVoltage_Angle() {
        return ( EAttribute ) getSvVoltage().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSvVoltage_Phase() {
        return ( EAttribute ) getSvVoltage().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSvVoltage_V() {
        return ( EAttribute ) getSvVoltage().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSvVoltage_TopologicalNode() {
        return ( EReference ) getSvVoltage().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getTransformerMeshImpedance() {
        if( transformerMeshImpedanceEClass == null ) {
            transformerMeshImpedanceEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 79 );
        }
        return transformerMeshImpedanceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTransformerMeshImpedance_R() {
        return ( EAttribute ) getTransformerMeshImpedance().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTransformerMeshImpedance_R0() {
        return ( EAttribute ) getTransformerMeshImpedance().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTransformerMeshImpedance_X() {
        return ( EAttribute ) getTransformerMeshImpedance().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTransformerMeshImpedance_X0() {
        return ( EAttribute ) getTransformerMeshImpedance().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTransformerMeshImpedance_ToTransformerEnd() {
        return ( EReference ) getTransformerMeshImpedance().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTransformerMeshImpedance_FromTransformerEnd() {
        return ( EReference ) getTransformerMeshImpedance().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDCNode() {
        if( dcNodeEClass == null ) {
            dcNodeEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 330 );
        }
        return dcNodeEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDCNode_DCEquipmentContainer() {
        return ( EReference ) getDCNode().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDCNode_DCTopologicalNode() {
        return ( EReference ) getDCNode().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDCNode_DCTerminals() {
        return ( EReference ) getDCNode().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSurgeArrester() {
        if( surgeArresterEClass == null ) {
            surgeArresterEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 336 );
        }
        return surgeArresterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getIrregularIntervalSchedule() {
        if( irregularIntervalScheduleEClass == null ) {
            irregularIntervalScheduleEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 98 );
        }
        return irregularIntervalScheduleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getIrregularIntervalSchedule_TimePoints() {
        return ( EReference ) getIrregularIntervalSchedule().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getRatioTapChangerTablePoint() {
        if( ratioTapChangerTablePointEClass == null ) {
            ratioTapChangerTablePointEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 201 );
        }
        return ratioTapChangerTablePointEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRatioTapChangerTablePoint_RatioTapChangerTable() {
        return ( EReference ) getRatioTapChangerTablePoint().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCsConverter() {
        if( csConverterEClass == null ) {
            csConverterEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 318 );
        }
        return csConverterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCsConverter_Alpha() {
        return ( EAttribute ) getCsConverter().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCsConverter_Gamma() {
        return ( EAttribute ) getCsConverter().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCsConverter_MaxAlpha() {
        return ( EAttribute ) getCsConverter().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCsConverter_MaxGamma() {
        return ( EAttribute ) getCsConverter().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCsConverter_MaxIdc() {
        return ( EAttribute ) getCsConverter().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCsConverter_MinAlpha() {
        return ( EAttribute ) getCsConverter().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCsConverter_MinGamma() {
        return ( EAttribute ) getCsConverter().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCsConverter_MinIdc() {
        return ( EAttribute ) getCsConverter().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCsConverter_OperatingMode() {
        return ( EAttribute ) getCsConverter().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCsConverter_PPccControl() {
        return ( EAttribute ) getCsConverter().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCsConverter_RatedIdc() {
        return ( EAttribute ) getCsConverter().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCsConverter_TargetAlpha() {
        return ( EAttribute ) getCsConverter().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCsConverter_TargetGamma() {
        return ( EAttribute ) getCsConverter().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCsConverter_TargetIdc() {
        return ( EAttribute ) getCsConverter().getEStructuralFeatures().get( 13 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSeasonDayTypeSchedule() {
        if( seasonDayTypeScheduleEClass == null ) {
            seasonDayTypeScheduleEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 117 );
        }
        return seasonDayTypeScheduleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSeasonDayTypeSchedule_Season() {
        return ( EReference ) getSeasonDayTypeSchedule().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSeasonDayTypeSchedule_DayType() {
        return ( EReference ) getSeasonDayTypeSchedule().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPowerSystemResource() {
        if( powerSystemResourceEClass == null ) {
            powerSystemResourceEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 70 );
        }
        return powerSystemResourceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPowerSystemResource_ReportingGroup() {
        return ( EReference ) getPowerSystemResource().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPowerSystemResource_Controls() {
        return ( EReference ) getPowerSystemResource().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPowerSystemResource_PSRType() {
        return ( EReference ) getPowerSystemResource().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPowerSystemResource_Measurements() {
        return ( EReference ) getPowerSystemResource().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPowerSystemResource_OperatingShare() {
        return ( EReference ) getPowerSystemResource().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getJunction() {
        if( junctionEClass == null ) {
            junctionEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 128 );
        }
        return junctionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getLine() {
        if( lineEClass == null ) {
            lineEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 109 );
        }
        return lineEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getLine_Region() {
        return ( EReference ) getLine().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getJumper() {
        if( jumperEClass == null ) {
            jumperEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 63 );
        }
        return jumperEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getNonConformLoad() {
        if( nonConformLoadEClass == null ) {
            nonConformLoadEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 290 );
        }
        return nonConformLoadEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getNonConformLoad_LoadGroup() {
        return ( EReference ) getNonConformLoad().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getBusbarSection() {
        if( busbarSectionEClass == null ) {
            busbarSectionEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 195 );
        }
        return busbarSectionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBusbarSection_IpMax() {
        return ( EAttribute ) getBusbarSection().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getBusbarSection_VoltageControlZone() {
        return ( EReference ) getBusbarSection().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getIOPoint() {
        if( ioPointEClass == null ) {
            ioPointEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 257 );
        }
        return ioPointEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getBusNameMarker() {
        if( busNameMarkerEClass == null ) {
            busNameMarkerEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 312 );
        }
        return busNameMarkerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBusNameMarker_Priority() {
        return ( EAttribute ) getBusNameMarker().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getBusNameMarker_ReportingGroup() {
        return ( EReference ) getBusNameMarker().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getBusNameMarker_Terminal() {
        return ( EReference ) getBusNameMarker().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getBusNameMarker_TopologicalNode() {
        return ( EReference ) getBusNameMarker().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDCTopologicalNode() {
        if( dcTopologicalNodeEClass == null ) {
            dcTopologicalNodeEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 265 );
        }
        return dcTopologicalNodeEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDCTopologicalNode_DCEquipmentContainer() {
        return ( EReference ) getDCTopologicalNode().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDCTopologicalNode_DCTopologicalIsland() {
        return ( EReference ) getDCTopologicalNode().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDCTopologicalNode_DCTerminals() {
        return ( EReference ) getDCTopologicalNode().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDCTopologicalNode_DCNodes() {
        return ( EReference ) getDCTopologicalNode().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDCShunt() {
        if( dcShuntEClass == null ) {
            dcShuntEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 183 );
        }
        return dcShuntEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDCShunt_Capacitance() {
        return ( EAttribute ) getDCShunt().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDCShunt_Resistance() {
        return ( EAttribute ) getDCShunt().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getBay() {
        if( bayEClass == null ) {
            bayEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 104 );
        }
        return bayEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBay_BayEnergyMeasFlag() {
        return ( EAttribute ) getBay().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBay_BayPowerMeasFlag() {
        return ( EAttribute ) getBay().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBay_BreakerConfiguration() {
        return ( EAttribute ) getBay().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBay_BusBarConfiguration() {
        return ( EAttribute ) getBay().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getBay_Substation() {
        return ( EReference ) getBay().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getBay_VoltageLevel() {
        return ( EReference ) getBay().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPerLengthPhaseImpedance() {
        if( perLengthPhaseImpedanceEClass == null ) {
            perLengthPhaseImpedanceEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 177 );
        }
        return perLengthPhaseImpedanceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPerLengthPhaseImpedance_ConductorCount() {
        return ( EAttribute ) getPerLengthPhaseImpedance().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPerLengthPhaseImpedance_PhaseImpedanceData() {
        return ( EReference ) getPerLengthPhaseImpedance().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getMutualCoupling() {
        if( mutualCouplingEClass == null ) {
            mutualCouplingEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 208 );
        }
        return mutualCouplingEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getMutualCoupling_B0ch() {
        return ( EAttribute ) getMutualCoupling().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getMutualCoupling_Distance11() {
        return ( EAttribute ) getMutualCoupling().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getMutualCoupling_Distance12() {
        return ( EAttribute ) getMutualCoupling().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getMutualCoupling_Distance21() {
        return ( EAttribute ) getMutualCoupling().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getMutualCoupling_Distance22() {
        return ( EAttribute ) getMutualCoupling().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getMutualCoupling_G0ch() {
        return ( EAttribute ) getMutualCoupling().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getMutualCoupling_R0() {
        return ( EAttribute ) getMutualCoupling().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getMutualCoupling_X0() {
        return ( EAttribute ) getMutualCoupling().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getMutualCoupling_Second_Terminal() {
        return ( EReference ) getMutualCoupling().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getMutualCoupling_First_Terminal() {
        return ( EReference ) getMutualCoupling().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getLoadGroup() {
        if( loadGroupEClass == null ) {
            loadGroupEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 333 );
        }
        return loadGroupEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getLoadGroup_SubLoadArea() {
        return ( EReference ) getLoadGroup().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSupercritical() {
        if( supercriticalEClass == null ) {
            supercriticalEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 180 );
        }
        return supercriticalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getAnalog() {
        if( analogEClass == null ) {
            analogEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 179 );
        }
        return analogEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAnalog_MaxValue() {
        return ( EAttribute ) getAnalog().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAnalog_MinValue() {
        return ( EAttribute ) getAnalog().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAnalog_NormalValue() {
        return ( EAttribute ) getAnalog().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAnalog_PositiveFlowIn() {
        return ( EAttribute ) getAnalog().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAnalog_LimitSets() {
        return ( EReference ) getAnalog().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAnalog_AnalogValues() {
        return ( EReference ) getAnalog().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getControlArea() {
        if( controlAreaEClass == null ) {
            controlAreaEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 311 );
        }
        return controlAreaEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getControlArea_NetInterchange() {
        return ( EAttribute ) getControlArea().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getControlArea_PTolerance() {
        return ( EAttribute ) getControlArea().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getControlArea_Type() {
        return ( EAttribute ) getControlArea().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getControlArea_TieFlow() {
        return ( EReference ) getControlArea().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getControlArea_EnergyArea() {
        return ( EReference ) getControlArea().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getControlArea_ControlAreaGeneratingUnit() {
        return ( EReference ) getControlArea().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getStartMainFuelCurve() {
        if( startMainFuelCurveEClass == null ) {
            startMainFuelCurveEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 198 );
        }
        return startMainFuelCurveEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStartMainFuelCurve_MainFuelType() {
        return ( EAttribute ) getStartMainFuelCurve().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getStartMainFuelCurve_StartupModel() {
        return ( EReference ) getStartMainFuelCurve().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getMeasurementValue() {
        if( measurementValueEClass == null ) {
            measurementValueEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 332 );
        }
        return measurementValueEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getMeasurementValue_SensorAccuracy() {
        return ( EAttribute ) getMeasurementValue().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getMeasurementValue_TimeStamp() {
        return ( EAttribute ) getMeasurementValue().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getMeasurementValue_MeasurementValueQuality() {
        return ( EReference ) getMeasurementValue().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getMeasurementValue_RemoteSource() {
        return ( EReference ) getMeasurementValue().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getMeasurementValue_MeasurementValueSource() {
        return ( EReference ) getMeasurementValue().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPowerElectronicsWindUnit() {
        if( powerElectronicsWindUnitEClass == null ) {
            powerElectronicsWindUnitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 269 );
        }
        return powerElectronicsWindUnitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getTapChanger() {
        if( tapChangerEClass == null ) {
            tapChangerEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 331 );
        }
        return tapChangerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChanger_ControlEnabled() {
        return ( EAttribute ) getTapChanger().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChanger_HighStep() {
        return ( EAttribute ) getTapChanger().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChanger_InitialDelay() {
        return ( EAttribute ) getTapChanger().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChanger_LowStep() {
        return ( EAttribute ) getTapChanger().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChanger_LtcFlag() {
        return ( EAttribute ) getTapChanger().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChanger_NeutralStep() {
        return ( EAttribute ) getTapChanger().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChanger_NeutralU() {
        return ( EAttribute ) getTapChanger().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChanger_NormalStep() {
        return ( EAttribute ) getTapChanger().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChanger_Step() {
        return ( EAttribute ) getTapChanger().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChanger_SubsequentDelay() {
        return ( EAttribute ) getTapChanger().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTapChanger_TapChangerControl() {
        return ( EReference ) getTapChanger().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTapChanger_SvTapStep() {
        return ( EReference ) getTapChanger().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTapChanger_TapSchedules() {
        return ( EReference ) getTapChanger().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getConnector() {
        if( connectorEClass == null ) {
            connectorEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 116 );
        }
        return connectorEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCurrentTransformer() {
        if( currentTransformerEClass == null ) {
            currentTransformerEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 226 );
        }
        return currentTransformerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurrentTransformer_AccuracyClass() {
        return ( EAttribute ) getCurrentTransformer().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurrentTransformer_AccuracyLimit() {
        return ( EAttribute ) getCurrentTransformer().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurrentTransformer_CoreBurden() {
        return ( EAttribute ) getCurrentTransformer().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurrentTransformer_CtClass() {
        return ( EAttribute ) getCurrentTransformer().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurrentTransformer_Usage() {
        return ( EAttribute ) getCurrentTransformer().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDrumBoiler() {
        if( drumBoilerEClass == null ) {
            drumBoilerEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 254 );
        }
        return drumBoilerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDrumBoiler_DrumBoilerRating() {
        return ( EAttribute ) getDrumBoiler().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDCTopologicalIsland() {
        if( dcTopologicalIslandEClass == null ) {
            dcTopologicalIslandEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 118 );
        }
        return dcTopologicalIslandEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDCTopologicalIsland_DCTopologicalNodes() {
        return ( EReference ) getDCTopologicalIsland().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPWRSteamSupply() {
        if( pwrSteamSupplyEClass == null ) {
            pwrSteamSupplyEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 146 );
        }
        return pwrSteamSupplyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPWRSteamSupply_ColdLegFBLagTC() {
        return ( EAttribute ) getPWRSteamSupply().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPWRSteamSupply_ColdLegFBLeadTC1() {
        return ( EAttribute ) getPWRSteamSupply().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPWRSteamSupply_ColdLegFBLeadTC2() {
        return ( EAttribute ) getPWRSteamSupply().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPWRSteamSupply_ColdLegFG1() {
        return ( EAttribute ) getPWRSteamSupply().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPWRSteamSupply_ColdLegFG2() {
        return ( EAttribute ) getPWRSteamSupply().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPWRSteamSupply_ColdLegLagTC() {
        return ( EAttribute ) getPWRSteamSupply().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPWRSteamSupply_CoreHTLagTC1() {
        return ( EAttribute ) getPWRSteamSupply().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPWRSteamSupply_CoreHTLagTC2() {
        return ( EAttribute ) getPWRSteamSupply().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPWRSteamSupply_CoreNeutronicsEffTC() {
        return ( EAttribute ) getPWRSteamSupply().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPWRSteamSupply_CoreNeutronicsHT() {
        return ( EAttribute ) getPWRSteamSupply().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPWRSteamSupply_FeedbackFactor() {
        return ( EAttribute ) getPWRSteamSupply().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPWRSteamSupply_HotLegLagTC() {
        return ( EAttribute ) getPWRSteamSupply().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPWRSteamSupply_HotLegSteamGain() {
        return ( EAttribute ) getPWRSteamSupply().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPWRSteamSupply_HotLegToColdLegGain() {
        return ( EAttribute ) getPWRSteamSupply().getEStructuralFeatures().get( 13 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPWRSteamSupply_PressureCG() {
        return ( EAttribute ) getPWRSteamSupply().getEStructuralFeatures().get( 14 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPWRSteamSupply_SteamFlowFG() {
        return ( EAttribute ) getPWRSteamSupply().getEStructuralFeatures().get( 15 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPWRSteamSupply_SteamPressureDropLagTC() {
        return ( EAttribute ) getPWRSteamSupply().getEStructuralFeatures().get( 16 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPWRSteamSupply_SteamPressureFG() {
        return ( EAttribute ) getPWRSteamSupply().getEStructuralFeatures().get( 17 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPWRSteamSupply_ThrottlePressureFactor() {
        return ( EAttribute ) getPWRSteamSupply().getEStructuralFeatures().get( 18 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPWRSteamSupply_ThrottlePressureSP() {
        return ( EAttribute ) getPWRSteamSupply().getEStructuralFeatures().get( 19 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDiagramObjectGluePoint() {
        if( diagramObjectGluePointEClass == null ) {
            diagramObjectGluePointEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 231 );
        }
        return diagramObjectGluePointEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDiagramObjectGluePoint_DiagramObjectPoints() {
        return ( EReference ) getDiagramObjectGluePoint().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getRegularIntervalSchedule() {
        if( regularIntervalScheduleEClass == null ) {
            regularIntervalScheduleEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 197 );
        }
        return regularIntervalScheduleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRegularIntervalSchedule_EndTime() {
        return ( EAttribute ) getRegularIntervalSchedule().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRegularIntervalSchedule_TimeStep() {
        return ( EAttribute ) getRegularIntervalSchedule().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRegularIntervalSchedule_TimePoints() {
        return ( EReference ) getRegularIntervalSchedule().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSubLoadArea() {
        if( subLoadAreaEClass == null ) {
            subLoadAreaEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 295 );
        }
        return subLoadAreaEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSubLoadArea_LoadGroups() {
        return ( EReference ) getSubLoadArea().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSubLoadArea_LoadArea() {
        return ( EReference ) getSubLoadArea().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCurveData() {
        if( curveDataEClass == null ) {
            curveDataEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 111 );
        }
        return curveDataEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurveData_Xvalue() {
        return ( EAttribute ) getCurveData().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurveData_Y1value() {
        return ( EAttribute ) getCurveData().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurveData_Y2value() {
        return ( EAttribute ) getCurveData().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCurveData_Y3value() {
        return ( EAttribute ) getCurveData().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCurveData_Curve() {
        return ( EReference ) getCurveData().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getWindGeneratingUnit() {
        if( windGeneratingUnitEClass == null ) {
            windGeneratingUnitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 272 );
        }
        return windGeneratingUnitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getWindGeneratingUnit_WindGenUnitType() {
        return ( EAttribute ) getWindGeneratingUnit().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getTransformerTank() {
        if( transformerTankEClass == null ) {
            transformerTankEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 325 );
        }
        return transformerTankEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTransformerTank_PowerTransformer() {
        return ( EReference ) getTransformerTank().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTransformerTank_TransformerTankEnds() {
        return ( EReference ) getTransformerTank().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEquipment() {
        if( equipmentEClass == null ) {
            equipmentEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 103 );
        }
        return equipmentEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquipment_Aggregate() {
        return ( EAttribute ) getEquipment().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquipment_InService() {
        return ( EAttribute ) getEquipment().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquipment_NetworkAnalysisEnabled() {
        return ( EAttribute ) getEquipment().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquipment_NormallyInService() {
        return ( EAttribute ) getEquipment().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEquipment_EquipmentContainer() {
        return ( EReference ) getEquipment().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEquipment_Faults() {
        return ( EReference ) getEquipment().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEquipment_OperationalLimitSet() {
        return ( EReference ) getEquipment().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEquipment_AdditionalEquipmentContainer() {
        return ( EReference ) getEquipment().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEquipment_ContingencyEquipment() {
        return ( EReference ) getEquipment().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCombinedCyclePlant() {
        if( combinedCyclePlantEClass == null ) {
            combinedCyclePlantEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 189 );
        }
        return combinedCyclePlantEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCombinedCyclePlant_CombCyclePlantRating() {
        return ( EAttribute ) getCombinedCyclePlant().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCombinedCyclePlant_ThermalGeneratingUnits() {
        return ( EReference ) getCombinedCyclePlant().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getBranchGroup() {
        if( branchGroupEClass == null ) {
            branchGroupEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 324 );
        }
        return branchGroupEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBranchGroup_MaximumActivePower() {
        return ( EAttribute ) getBranchGroup().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBranchGroup_MaximumReactivePower() {
        return ( EAttribute ) getBranchGroup().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBranchGroup_MinimumActivePower() {
        return ( EAttribute ) getBranchGroup().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBranchGroup_MinimumReactivePower() {
        return ( EAttribute ) getBranchGroup().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBranchGroup_MonitorActivePower() {
        return ( EAttribute ) getBranchGroup().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBranchGroup_MonitorReactivePower() {
        return ( EAttribute ) getBranchGroup().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getBranchGroup_BranchGroupTerminal() {
        return ( EReference ) getBranchGroup().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSetPoint() {
        if( setPointEClass == null ) {
            setPointEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 127 );
        }
        return setPointEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSetPoint_NormalValue() {
        return ( EAttribute ) getSetPoint().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSetPoint_Value() {
        return ( EAttribute ) getSetPoint().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getAccumulatorLimit() {
        if( accumulatorLimitEClass == null ) {
            accumulatorLimitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 228 );
        }
        return accumulatorLimitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAccumulatorLimit_Value() {
        return ( EAttribute ) getAccumulatorLimit().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAccumulatorLimit_LimitSet() {
        return ( EReference ) getAccumulatorLimit().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSectionaliser() {
        if( sectionaliserEClass == null ) {
            sectionaliserEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 191 );
        }
        return sectionaliserEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getTapSchedule() {
        if( tapScheduleEClass == null ) {
            tapScheduleEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 196 );
        }
        return tapScheduleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTapSchedule_TapChanger() {
        return ( EReference ) getTapSchedule().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getLineFault() {
        if( lineFaultEClass == null ) {
            lineFaultEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 72 );
        }
        return lineFaultEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLineFault_LengthFromTerminal1() {
        return ( EAttribute ) getLineFault().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getLineFault_ACLineSegment() {
        return ( EReference ) getLineFault().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getGeneratingUnit() {
        if( generatingUnitEClass == null ) {
            generatingUnitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 171 );
        }
        return generatingUnitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_AllocSpinResP() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_AutoCntrlMarginP() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_BaseP() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_ControlDeadband() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_ControlPulseHigh() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_ControlPulseLow() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_ControlResponseRate() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_Efficiency() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_GenControlMode() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_GenControlSource() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_GovernorMPL() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_GovernorSCD() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_HighControlLimit() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_InitialP() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 13 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_LongPF() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 14 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_LowControlLimit() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 15 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_LowerRampRate() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 16 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_MaxEconomicP() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 17 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_MaximumAllowableSpinningReserve() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 18 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_MaxOperatingP() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 19 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_MinEconomicP() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 20 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_MinimumOffTime() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 21 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_MinOperatingP() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 22 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_ModelDetail() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 23 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_NominalP() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 24 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_NormalPF() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 25 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_PenaltyFactor() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 26 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_RaiseRampRate() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 27 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_RatedGrossMaxP() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 28 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_RatedGrossMinP() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 29 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_RatedNetMaxP() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 30 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_ShortPF() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 31 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_StartupCost() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 32 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_StartupTime() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 33 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_TieLinePF() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 34 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_TotalEfficiency() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 35 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getGeneratingUnit_VariableCost() {
        return ( EAttribute ) getGeneratingUnit().getEStructuralFeatures().get( 36 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getGeneratingUnit_GenUnitOpCostCurves() {
        return ( EReference ) getGeneratingUnit().getEStructuralFeatures().get( 40 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getGeneratingUnit_RotatingMachine() {
        return ( EReference ) getGeneratingUnit().getEStructuralFeatures().get( 41 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getGeneratingUnit_ControlAreaGeneratingUnit() {
        return ( EReference ) getGeneratingUnit().getEStructuralFeatures().get( 38 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getGeneratingUnit_GenUnitOpSchedule() {
        return ( EReference ) getGeneratingUnit().getEStructuralFeatures().get( 37 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getGeneratingUnit_GrossToNetActivePowerCurves() {
        return ( EReference ) getGeneratingUnit().getEStructuralFeatures().get( 39 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getHydroGeneratingEfficiencyCurve() {
        if( hydroGeneratingEfficiencyCurveEClass == null ) {
            hydroGeneratingEfficiencyCurveEClass = ( EClass ) EPackage.Registry.INSTANCE
                    .getEPackage( CimPackage.eNS_URI ).getEClassifiers().get( 173 );
        }
        return hydroGeneratingEfficiencyCurveEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getHydroGeneratingEfficiencyCurve_HydroGeneratingUnit() {
        return ( EReference ) getHydroGeneratingEfficiencyCurve().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getRemoteUnit() {
        if( remoteUnitEClass == null ) {
            remoteUnitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 67 );
        }
        return remoteUnitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRemoteUnit_RemoteUnitType() {
        return ( EAttribute ) getRemoteUnit().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRemoteUnit_RemotePoints() {
        return ( EReference ) getRemoteUnit().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRemoteUnit_CommunicationLinks() {
        return ( EReference ) getRemoteUnit().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSvPowerFlow() {
        if( svPowerFlowEClass == null ) {
            svPowerFlowEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 271 );
        }
        return svPowerFlowEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSvPowerFlow_P() {
        return ( EAttribute ) getSvPowerFlow().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSvPowerFlow_Phase() {
        return ( EAttribute ) getSvPowerFlow().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSvPowerFlow_Q() {
        return ( EAttribute ) getSvPowerFlow().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSvPowerFlow_Terminal() {
        return ( EReference ) getSvPowerFlow().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getFossilSteamSupply() {
        if( fossilSteamSupplyEClass == null ) {
            fossilSteamSupplyEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 355 );
        }
        return fossilSteamSupplyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_AuxPowerVersusFrequency() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_AuxPowerVersusVoltage() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_BoilerControlMode() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_ControlErrorBiasP() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_ControlIC() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_ControlPC() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_ControlPEB() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_ControlPED() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_ControlTC() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_FeedWaterIG() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_FeedWaterPG() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_FeedWaterTC() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_FuelDemandLimit() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_FuelSupplyDelay() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 13 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_FuelSupplyTC() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 14 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_MaxErrorRateP() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 15 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_MechPowerSensorLag() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 16 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_MinErrorRateP() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 17 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_PressureCtrlDG() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 18 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_PressureCtrlIG() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 19 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_PressureCtrlPG() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 20 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_PressureFeedback() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 21 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_SuperHeater1Capacity() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 22 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_SuperHeater2Capacity() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 23 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_SuperHeaterPipePD() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 24 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilSteamSupply_ThrottlePressureSP() {
        return ( EAttribute ) getFossilSteamSupply().getEStructuralFeatures().get( 25 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getBasePower() {
        if( basePowerEClass == null ) {
            basePowerEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 115 );
        }
        return basePowerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBasePower_BasePower() {
        return ( EAttribute ) getBasePower().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDCBaseTerminal() {
        if( dcBaseTerminalEClass == null ) {
            dcBaseTerminalEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 280 );
        }
        return dcBaseTerminalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDCBaseTerminal_DCNode() {
        return ( EReference ) getDCBaseTerminal().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDCBaseTerminal_DCTopologicalNode() {
        return ( EReference ) getDCBaseTerminal().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getOperationalLimitType() {
        if( operationalLimitTypeEClass == null ) {
            operationalLimitTypeEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 58 );
        }
        return operationalLimitTypeEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getOperationalLimitType_AcceptableDuration() {
        return ( EAttribute ) getOperationalLimitType().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getOperationalLimitType_Direction() {
        return ( EAttribute ) getOperationalLimitType().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getOperationalLimitType_OperationalLimit() {
        return ( EReference ) getOperationalLimitType().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSvInjection() {
        if( svInjectionEClass == null ) {
            svInjectionEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 122 );
        }
        return svInjectionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSvInjection_Phase() {
        return ( EAttribute ) getSvInjection().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSvInjection_PInjection() {
        return ( EAttribute ) getSvInjection().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSvInjection_QInjection() {
        return ( EAttribute ) getSvInjection().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSvInjection_TopologicalNode() {
        return ( EReference ) getSvInjection().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getActivePowerLimit() {
        if( activePowerLimitEClass == null ) {
            activePowerLimitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 172 );
        }
        return activePowerLimitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getActivePowerLimit_NormalValue() {
        return ( EAttribute ) getActivePowerLimit().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getActivePowerLimit_Value() {
        return ( EAttribute ) getActivePowerLimit().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getConformLoad() {
        if( conformLoadEClass == null ) {
            conformLoadEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 190 );
        }
        return conformLoadEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getConformLoad_LoadGroup() {
        return ( EReference ) getConformLoad().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDCTerminal() {
        if( dcTerminalEClass == null ) {
            dcTerminalEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 262 );
        }
        return dcTerminalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDCTerminal_DCConductingEquipment() {
        return ( EReference ) getDCTerminal().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSynchrocheckRelay() {
        if( synchrocheckRelayEClass == null ) {
            synchrocheckRelayEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 169 );
        }
        return synchrocheckRelayEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchrocheckRelay_MaxAngleDiff() {
        return ( EAttribute ) getSynchrocheckRelay().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchrocheckRelay_MaxFreqDiff() {
        return ( EAttribute ) getSynchrocheckRelay().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSynchrocheckRelay_MaxVoltDiff() {
        return ( EAttribute ) getSynchrocheckRelay().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getStartRampCurve() {
        if( startRampCurveEClass == null ) {
            startRampCurveEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 152 );
        }
        return startRampCurveEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStartRampCurve_HotStandbyRamp() {
        return ( EAttribute ) getStartRampCurve().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getStartRampCurve_StartupModel() {
        return ( EReference ) getStartRampCurve().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPSRType() {
        if( psrTypeEClass == null ) {
            psrTypeEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 186 );
        }
        return psrTypeEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPSRType_PowerSystemResources() {
        return ( EReference ) getPSRType().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPlant() {
        if( plantEClass == null ) {
            plantEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 264 );
        }
        return plantEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getMeasurementValueSource() {
        if( measurementValueSourceEClass == null ) {
            measurementValueSourceEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 99 );
        }
        return measurementValueSourceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getMeasurementValueSource_MeasurementValues() {
        return ( EReference ) getMeasurementValueSource().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getFeeder() {
        if( feederEClass == null ) {
            feederEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 148 );
        }
        return feederEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getFeeder_NormalEnergizedSubstation() {
        return ( EReference ) getFeeder().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getFeeder_NormalEnergizingSubstation() {
        return ( EReference ) getFeeder().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getFeeder_NormalHeadTerminal() {
        return ( EReference ) getFeeder().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getFeeder_NamingSecondarySubstation() {
        return ( EReference ) getFeeder().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getStartupModel() {
        if( startupModelEClass == null ) {
            startupModelEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 161 );
        }
        return startupModelEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStartupModel_FixedMaintCost() {
        return ( EAttribute ) getStartupModel().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStartupModel_HotStandbyHeat() {
        return ( EAttribute ) getStartupModel().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStartupModel_IncrementalMaintCost() {
        return ( EAttribute ) getStartupModel().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStartupModel_MinimumDownTime() {
        return ( EAttribute ) getStartupModel().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStartupModel_MinimumRunTime() {
        return ( EAttribute ) getStartupModel().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStartupModel_RiskFactorCost() {
        return ( EAttribute ) getStartupModel().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStartupModel_StartupCost() {
        return ( EAttribute ) getStartupModel().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStartupModel_StartupDate() {
        return ( EAttribute ) getStartupModel().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStartupModel_StartupPriority() {
        return ( EAttribute ) getStartupModel().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getStartupModel_StbyAuxP() {
        return ( EAttribute ) getStartupModel().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getStartupModel_StartMainFuelCurve() {
        return ( EReference ) getStartupModel().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getStartupModel_StartIgnFuelCurve() {
        return ( EReference ) getStartupModel().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getStartupModel_StartRampCurve() {
        return ( EReference ) getStartupModel().getEStructuralFeatures().get( 13 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getStartupModel_ThermalGeneratingUnit() {
        return ( EReference ) getStartupModel().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getFaultIndicator() {
        if( faultIndicatorEClass == null ) {
            faultIndicatorEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 314 );
        }
        return faultIndicatorEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getFlowSensor() {
        if( flowSensorEClass == null ) {
            flowSensorEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 356 );
        }
        return flowSensorEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getACDCConverter() {
        if( acdcConverterEClass == null ) {
            acdcConverterEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 315 );
        }
        return acdcConverterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACDCConverter_BaseS() {
        return ( EAttribute ) getACDCConverter().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACDCConverter_Idc() {
        return ( EAttribute ) getACDCConverter().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACDCConverter_IdleLoss() {
        return ( EAttribute ) getACDCConverter().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACDCConverter_MaxUdc() {
        return ( EAttribute ) getACDCConverter().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACDCConverter_MinUdc() {
        return ( EAttribute ) getACDCConverter().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACDCConverter_NumberOfValves() {
        return ( EAttribute ) getACDCConverter().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACDCConverter_P() {
        return ( EAttribute ) getACDCConverter().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACDCConverter_PoleLossP() {
        return ( EAttribute ) getACDCConverter().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACDCConverter_Q() {
        return ( EAttribute ) getACDCConverter().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACDCConverter_RatedUdc() {
        return ( EAttribute ) getACDCConverter().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACDCConverter_ResistiveLoss() {
        return ( EAttribute ) getACDCConverter().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACDCConverter_SwitchingLoss() {
        return ( EAttribute ) getACDCConverter().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACDCConverter_TargetPpcc() {
        return ( EAttribute ) getACDCConverter().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACDCConverter_TargetUdc() {
        return ( EAttribute ) getACDCConverter().getEStructuralFeatures().get( 13 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACDCConverter_Uc() {
        return ( EAttribute ) getACDCConverter().getEStructuralFeatures().get( 14 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACDCConverter_Udc() {
        return ( EAttribute ) getACDCConverter().getEStructuralFeatures().get( 15 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACDCConverter_ValveU0() {
        return ( EAttribute ) getACDCConverter().getEStructuralFeatures().get( 16 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getACDCConverter_PccTerminal() {
        return ( EReference ) getACDCConverter().getEStructuralFeatures().get( 18 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getACDCConverter_DCTerminals() {
        return ( EReference ) getACDCConverter().getEStructuralFeatures().get( 17 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEnergySource() {
        if( energySourceEClass == null ) {
            energySourceEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 316 );
        }
        return energySourceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergySource_ActivePower() {
        return ( EAttribute ) getEnergySource().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergySource_NominalVoltage() {
        return ( EAttribute ) getEnergySource().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergySource_PMax() {
        return ( EAttribute ) getEnergySource().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergySource_PMin() {
        return ( EAttribute ) getEnergySource().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergySource_R() {
        return ( EAttribute ) getEnergySource().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergySource_R0() {
        return ( EAttribute ) getEnergySource().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergySource_ReactivePower() {
        return ( EAttribute ) getEnergySource().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergySource_Rn() {
        return ( EAttribute ) getEnergySource().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergySource_VoltageAngle() {
        return ( EAttribute ) getEnergySource().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergySource_VoltageMagnitude() {
        return ( EAttribute ) getEnergySource().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergySource_X() {
        return ( EAttribute ) getEnergySource().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergySource_X0() {
        return ( EAttribute ) getEnergySource().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEnergySource_Xn() {
        return ( EAttribute ) getEnergySource().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEnergySource_EnergySourcePhase() {
        return ( EReference ) getEnergySource().getEStructuralFeatures().get( 14 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEnergySource_EnergySchedulingType() {
        return ( EReference ) getEnergySource().getEStructuralFeatures().get( 13 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getGroundDisconnector() {
        if( groundDisconnectorEClass == null ) {
            groundDisconnectorEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 339 );
        }
        return groundDisconnectorEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPhaseImpedanceData() {
        if( phaseImpedanceDataEClass == null ) {
            phaseImpedanceDataEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 80 );
        }
        return phaseImpedanceDataEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPhaseImpedanceData_B() {
        return ( EAttribute ) getPhaseImpedanceData().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPhaseImpedanceData_Column() {
        return ( EAttribute ) getPhaseImpedanceData().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPhaseImpedanceData_FromPhase() {
        return ( EAttribute ) getPhaseImpedanceData().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPhaseImpedanceData_G() {
        return ( EAttribute ) getPhaseImpedanceData().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPhaseImpedanceData_R() {
        return ( EAttribute ) getPhaseImpedanceData().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPhaseImpedanceData_Row() {
        return ( EAttribute ) getPhaseImpedanceData().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPhaseImpedanceData_ToPhase() {
        return ( EAttribute ) getPhaseImpedanceData().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPhaseImpedanceData_X() {
        return ( EAttribute ) getPhaseImpedanceData().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPhaseImpedanceData_PhaseImpedance() {
        return ( EReference ) getPhaseImpedanceData().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getAnalogLimit() {
        if( analogLimitEClass == null ) {
            analogLimitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 306 );
        }
        return analogLimitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAnalogLimit_Value() {
        return ( EAttribute ) getAnalogLimit().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAnalogLimit_LimitSet() {
        return ( EReference ) getAnalogLimit().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getRegularTimePoint() {
        if( regularTimePointEClass == null ) {
            regularTimePointEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 124 );
        }
        return regularTimePointEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRegularTimePoint_SequenceNumber() {
        return ( EAttribute ) getRegularTimePoint().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRegularTimePoint_Value1() {
        return ( EAttribute ) getRegularTimePoint().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRegularTimePoint_Value2() {
        return ( EAttribute ) getRegularTimePoint().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRegularTimePoint_IntervalSchedule() {
        return ( EReference ) getRegularTimePoint().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getFuse() {
        if( fuseEClass == null ) {
            fuseEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 224 );
        }
        return fuseEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCut() {
        if( cutEClass == null ) {
            cutEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 147 );
        }
        return cutEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCut_LengthFromTerminal1() {
        return ( EAttribute ) getCut().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCut_ACLineSegment() {
        return ( EReference ) getCut().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getMeasurement() {
        if( measurementEClass == null ) {
            measurementEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 134 );
        }
        return measurementEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getMeasurement_MeasurementType() {
        return ( EAttribute ) getMeasurement().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getMeasurement_Phases() {
        return ( EAttribute ) getMeasurement().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getMeasurement_UncefactUnitCode() {
        return ( EAttribute ) getMeasurement().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getMeasurement_UnitMultiplier() {
        return ( EAttribute ) getMeasurement().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getMeasurement_UnitSymbol() {
        return ( EAttribute ) getMeasurement().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getMeasurement_Terminal() {
        return ( EReference ) getMeasurement().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getMeasurement_PowerSystemResource() {
        return ( EReference ) getMeasurement().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getLevelVsVolumeCurve() {
        if( levelVsVolumeCurveEClass == null ) {
            levelVsVolumeCurveEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 235 );
        }
        return levelVsVolumeCurveEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getLevelVsVolumeCurve_Reservoir() {
        return ( EReference ) getLevelVsVolumeCurve().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDCGround() {
        if( dcGroundEClass == null ) {
            dcGroundEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 285 );
        }
        return dcGroundEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDCGround_Inductance() {
        return ( EAttribute ) getDCGround().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDCGround_R() {
        return ( EAttribute ) getDCGround().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getBatteryUnit() {
        if( batteryUnitEClass == null ) {
            batteryUnitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 107 );
        }
        return batteryUnitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBatteryUnit_BatteryState() {
        return ( EAttribute ) getBatteryUnit().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBatteryUnit_RatedE() {
        return ( EAttribute ) getBatteryUnit().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBatteryUnit_StoredE() {
        return ( EAttribute ) getBatteryUnit().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getAccumulatorLimitSet() {
        if( accumulatorLimitSetEClass == null ) {
            accumulatorLimitSetEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 132 );
        }
        return accumulatorLimitSetEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAccumulatorLimitSet_Limits() {
        return ( EReference ) getAccumulatorLimitSet().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAccumulatorLimitSet_Measurements() {
        return ( EReference ) getAccumulatorLimitSet().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPhotoVoltaicUnit() {
        if( photoVoltaicUnitEClass == null ) {
            photoVoltaicUnitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 302 );
        }
        return photoVoltaicUnitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDCChopper() {
        if( dcChopperEClass == null ) {
            dcChopperEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 163 );
        }
        return dcChopperEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getRemoteSource() {
        if( remoteSourceEClass == null ) {
            remoteSourceEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 181 );
        }
        return remoteSourceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRemoteSource_Deadband() {
        return ( EAttribute ) getRemoteSource().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRemoteSource_ScanInterval() {
        return ( EAttribute ) getRemoteSource().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRemoteSource_SensorMaximum() {
        return ( EAttribute ) getRemoteSource().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRemoteSource_SensorMinimum() {
        return ( EAttribute ) getRemoteSource().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRemoteSource_MeasurementValue() {
        return ( EReference ) getRemoteSource().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPhaseTapChanger() {
        if( phaseTapChangerEClass == null ) {
            phaseTapChangerEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 211 );
        }
        return phaseTapChangerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPhaseTapChanger_TransformerEnd() {
        return ( EReference ) getPhaseTapChanger().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getHeatInputCurve() {
        if( heatInputCurveEClass == null ) {
            heatInputCurveEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 250 );
        }
        return heatInputCurveEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHeatInputCurve_AuxPowerMult() {
        return ( EAttribute ) getHeatInputCurve().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHeatInputCurve_AuxPowerOffset() {
        return ( EAttribute ) getHeatInputCurve().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHeatInputCurve_HeatInputEff() {
        return ( EAttribute ) getHeatInputCurve().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHeatInputCurve_HeatInputOffset() {
        return ( EAttribute ) getHeatInputCurve().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHeatInputCurve_IsNetGrossP() {
        return ( EAttribute ) getHeatInputCurve().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getHeatInputCurve_ThermalGeneratingUnit() {
        return ( EReference ) getHeatInputCurve().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getThermalGeneratingUnit() {
        if( thermalGeneratingUnitEClass == null ) {
            thermalGeneratingUnitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 289 );
        }
        return thermalGeneratingUnitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getThermalGeneratingUnit_OMCost() {
        return ( EAttribute ) getThermalGeneratingUnit().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getThermalGeneratingUnit_EmmissionAccounts() {
        return ( EReference ) getThermalGeneratingUnit().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getThermalGeneratingUnit_EmissionCurves() {
        return ( EReference ) getThermalGeneratingUnit().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getThermalGeneratingUnit_HeatRateCurve() {
        return ( EReference ) getThermalGeneratingUnit().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getThermalGeneratingUnit_FossilFuels() {
        return ( EReference ) getThermalGeneratingUnit().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getThermalGeneratingUnit_ShutdownCurve() {
        return ( EReference ) getThermalGeneratingUnit().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getThermalGeneratingUnit_StartupModel() {
        return ( EReference ) getThermalGeneratingUnit().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getThermalGeneratingUnit_CAESPlant() {
        return ( EReference ) getThermalGeneratingUnit().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getThermalGeneratingUnit_CogenerationPlant() {
        return ( EReference ) getThermalGeneratingUnit().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getThermalGeneratingUnit_FuelAllocationSchedules() {
        return ( EReference ) getThermalGeneratingUnit().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getThermalGeneratingUnit_IncrementalHeatRateCurve() {
        return ( EReference ) getThermalGeneratingUnit().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getThermalGeneratingUnit_CombinedCyclePlant() {
        return ( EReference ) getThermalGeneratingUnit().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getThermalGeneratingUnit_HeatInputCurve() {
        return ( EReference ) getThermalGeneratingUnit().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getGrossToNetActivePowerCurve() {
        if( grossToNetActivePowerCurveEClass == null ) {
            grossToNetActivePowerCurveEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 297 );
        }
        return grossToNetActivePowerCurveEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getGrossToNetActivePowerCurve_GeneratingUnit() {
        return ( EReference ) getGrossToNetActivePowerCurve().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPhaseTapChangerSymmetrical() {
        if( phaseTapChangerSymmetricalEClass == null ) {
            phaseTapChangerSymmetricalEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 77 );
        }
        return phaseTapChangerSymmetricalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getTieFlow() {
        if( tieFlowEClass == null ) {
            tieFlowEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 307 );
        }
        return tieFlowEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTieFlow_PositiveFlowIn() {
        return ( EAttribute ) getTieFlow().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTieFlow_Terminal() {
        return ( EReference ) getTieFlow().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTieFlow_AltTieMeas() {
        return ( EReference ) getTieFlow().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTieFlow_ControlArea() {
        return ( EReference ) getTieFlow().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getAccumulator() {
        if( accumulatorEClass == null ) {
            accumulatorEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 260 );
        }
        return accumulatorEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAccumulator_MaxValue() {
        return ( EAttribute ) getAccumulator().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAccumulator_LimitSets() {
        return ( EReference ) getAccumulator().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAccumulator_AccumulatorValues() {
        return ( EReference ) getAccumulator().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getNonlinearShuntCompensator() {
        if( nonlinearShuntCompensatorEClass == null ) {
            nonlinearShuntCompensatorEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 230 );
        }
        return nonlinearShuntCompensatorEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getNonlinearShuntCompensator_NonlinearShuntCompensatorPoints() {
        return ( EReference ) getNonlinearShuntCompensator().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getFault() {
        if( faultEClass == null ) {
            faultEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 100 );
        }
        return faultEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFault_Kind() {
        return ( EAttribute ) getFault().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFault_OccurredDateTime() {
        return ( EAttribute ) getFault().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFault_Phases() {
        return ( EAttribute ) getFault().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getFault_Impedance() {
        return ( EReference ) getFault().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getFault_FaultCauseTypes() {
        return ( EReference ) getFault().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getFault_FaultyEquipment() {
        return ( EReference ) getFault().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getContingency() {
        if( contingencyEClass == null ) {
            contingencyEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 343 );
        }
        return contingencyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getContingency_MustStudy() {
        return ( EAttribute ) getContingency().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getContingency_ContingencyElement() {
        return ( EReference ) getContingency().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getBreaker() {
        if( breakerEClass == null ) {
            breakerEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 68 );
        }
        return breakerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBreaker_InTransitTime() {
        return ( EAttribute ) getBreaker().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCAESPlant() {
        if( caesPlantEClass == null ) {
            caesPlantEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 74 );
        }
        return caesPlantEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCAESPlant_EnergyStorageCapacity() {
        return ( EAttribute ) getCAESPlant().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getCAESPlant_RatedCapacityP() {
        return ( EAttribute ) getCAESPlant().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCAESPlant_ThermalGeneratingUnit() {
        return ( EReference ) getCAESPlant().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCAESPlant_AirCompressor() {
        return ( EReference ) getCAESPlant().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getTapChangerTablePoint() {
        if( tapChangerTablePointEClass == null ) {
            tapChangerTablePointEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 133 );
        }
        return tapChangerTablePointEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChangerTablePoint_B() {
        return ( EAttribute ) getTapChangerTablePoint().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChangerTablePoint_G() {
        return ( EAttribute ) getTapChangerTablePoint().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChangerTablePoint_R() {
        return ( EAttribute ) getTapChangerTablePoint().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChangerTablePoint_Ratio() {
        return ( EAttribute ) getTapChangerTablePoint().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChangerTablePoint_Step() {
        return ( EAttribute ) getTapChangerTablePoint().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTapChangerTablePoint_X() {
        return ( EAttribute ) getTapChangerTablePoint().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getFaultCauseType() {
        if( faultCauseTypeEClass == null ) {
            faultCauseTypeEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 62 );
        }
        return faultCauseTypeEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getFaultCauseType_Faults() {
        return ( EReference ) getFaultCauseType().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDCBreaker() {
        if( dcBreakerEClass == null ) {
            dcBreakerEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 283 );
        }
        return dcBreakerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getRatioTapChanger() {
        if( ratioTapChangerEClass == null ) {
            ratioTapChangerEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 156 );
        }
        return ratioTapChangerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRatioTapChanger_StepVoltageIncrement() {
        return ( EAttribute ) getRatioTapChanger().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getRatioTapChanger_TculControlMode() {
        return ( EAttribute ) getRatioTapChanger().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRatioTapChanger_RatioTapChangerTable() {
        return ( EReference ) getRatioTapChanger().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRatioTapChanger_TransformerEnd() {
        return ( EReference ) getRatioTapChanger().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getLoadArea() {
        if( loadAreaEClass == null ) {
            loadAreaEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 282 );
        }
        return loadAreaEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getLoadArea_SubLoadAreas() {
        return ( EReference ) getLoadArea().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getFuelAllocationSchedule() {
        if( fuelAllocationScheduleEClass == null ) {
            fuelAllocationScheduleEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 136 );
        }
        return fuelAllocationScheduleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFuelAllocationSchedule_FuelAllocationEndDate() {
        return ( EAttribute ) getFuelAllocationSchedule().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFuelAllocationSchedule_FuelAllocationStartDate() {
        return ( EAttribute ) getFuelAllocationSchedule().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFuelAllocationSchedule_FuelType() {
        return ( EAttribute ) getFuelAllocationSchedule().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFuelAllocationSchedule_MaxFuelAllocation() {
        return ( EAttribute ) getFuelAllocationSchedule().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFuelAllocationSchedule_MinFuelAllocation() {
        return ( EAttribute ) getFuelAllocationSchedule().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getFuelAllocationSchedule_ThermalGeneratingUnit() {
        return ( EReference ) getFuelAllocationSchedule().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getFuelAllocationSchedule_FossilFuel() {
        return ( EReference ) getFuelAllocationSchedule().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getACLineSegment() {
        if( acLineSegmentEClass == null ) {
            acLineSegmentEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 244 );
        }
        return acLineSegmentEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACLineSegment_B0ch() {
        return ( EAttribute ) getACLineSegment().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACLineSegment_Bch() {
        return ( EAttribute ) getACLineSegment().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACLineSegment_G0ch() {
        return ( EAttribute ) getACLineSegment().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACLineSegment_Gch() {
        return ( EAttribute ) getACLineSegment().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACLineSegment_R() {
        return ( EAttribute ) getACLineSegment().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACLineSegment_R0() {
        return ( EAttribute ) getACLineSegment().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACLineSegment_ShortCircuitEndTemperature() {
        return ( EAttribute ) getACLineSegment().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACLineSegment_X() {
        return ( EAttribute ) getACLineSegment().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACLineSegment_X0() {
        return ( EAttribute ) getACLineSegment().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getACLineSegment_LineFaults() {
        return ( EReference ) getACLineSegment().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getACLineSegment_ACLineSegmentPhases() {
        return ( EReference ) getACLineSegment().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getACLineSegment_Clamp() {
        return ( EReference ) getACLineSegment().getEStructuralFeatures().get( 13 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getACLineSegment_PerLengthImpedance() {
        return ( EReference ) getACLineSegment().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getACLineSegment_Cut() {
        return ( EReference ) getACLineSegment().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getLimit() {
        if( limitEClass == null ) {
            limitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 347 );
        }
        return limitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getValueAliasSet() {
        if( valueAliasSetEClass == null ) {
            valueAliasSetEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 300 );
        }
        return valueAliasSetEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getValueAliasSet_Discretes() {
        return ( EReference ) getValueAliasSet().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getValueAliasSet_RaiseLowerCommands() {
        return ( EReference ) getValueAliasSet().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getValueAliasSet_Commands() {
        return ( EReference ) getValueAliasSet().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getValueAliasSet_Values() {
        return ( EReference ) getValueAliasSet().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getLimitSet() {
        if( limitSetEClass == null ) {
            limitSetEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 82 );
        }
        return limitSetEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLimitSet_IsPercentageLimits() {
        return ( EAttribute ) getLimitSet().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getStateVariable() {
        if( stateVariableEClass == null ) {
            stateVariableEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 139 );
        }
        return stateVariableEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDiscreteValue() {
        if( discreteValueEClass == null ) {
            discreteValueEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 273 );
        }
        return discreteValueEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDiscreteValue_Value() {
        return ( EAttribute ) getDiscreteValue().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDiscreteValue_Discrete() {
        return ( EReference ) getDiscreteValue().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDiscreteValue_Command() {
        return ( EReference ) getDiscreteValue().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getAccumulatorValue() {
        if( accumulatorValueEClass == null ) {
            accumulatorValueEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 298 );
        }
        return accumulatorValueEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAccumulatorValue_Value() {
        return ( EAttribute ) getAccumulatorValue().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAccumulatorValue_Accumulator() {
        return ( EReference ) getAccumulatorValue().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAccumulatorValue_AccumulatorReset() {
        return ( EReference ) getAccumulatorValue().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getTransformerCoreAdmittance() {
        if( transformerCoreAdmittanceEClass == null ) {
            transformerCoreAdmittanceEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 237 );
        }
        return transformerCoreAdmittanceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTransformerCoreAdmittance_B() {
        return ( EAttribute ) getTransformerCoreAdmittance().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTransformerCoreAdmittance_B0() {
        return ( EAttribute ) getTransformerCoreAdmittance().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTransformerCoreAdmittance_G() {
        return ( EAttribute ) getTransformerCoreAdmittance().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTransformerCoreAdmittance_G0() {
        return ( EAttribute ) getTransformerCoreAdmittance().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTransformerCoreAdmittance_TransformerEnd() {
        return ( EReference ) getTransformerCoreAdmittance().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getTransformerStarImpedance() {
        if( transformerStarImpedanceEClass == null ) {
            transformerStarImpedanceEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 245 );
        }
        return transformerStarImpedanceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTransformerStarImpedance_R() {
        return ( EAttribute ) getTransformerStarImpedance().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTransformerStarImpedance_R0() {
        return ( EAttribute ) getTransformerStarImpedance().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTransformerStarImpedance_X() {
        return ( EAttribute ) getTransformerStarImpedance().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTransformerStarImpedance_X0() {
        return ( EAttribute ) getTransformerStarImpedance().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTransformerStarImpedance_TransformerEnd() {
        return ( EReference ) getTransformerStarImpedance().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getVoltageLevel() {
        if( voltageLevelEClass == null ) {
            voltageLevelEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 168 );
        }
        return voltageLevelEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getVoltageLevel_HighVoltageLimit() {
        return ( EAttribute ) getVoltageLevel().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getVoltageLevel_LowVoltageLimit() {
        return ( EAttribute ) getVoltageLevel().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getVoltageLevel_Bays() {
        return ( EReference ) getVoltageLevel().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getVoltageLevel_BaseVoltage() {
        return ( EReference ) getVoltageLevel().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getVoltageLevel_Substation() {
        return ( EReference ) getVoltageLevel().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getAccumulatorReset() {
        if( accumulatorResetEClass == null ) {
            accumulatorResetEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 206 );
        }
        return accumulatorResetEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAccumulatorReset_AccumulatorValue() {
        return ( EReference ) getAccumulatorReset().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPhaseTapChangerTablePoint() {
        if( phaseTapChangerTablePointEClass == null ) {
            phaseTapChangerTablePointEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 83 );
        }
        return phaseTapChangerTablePointEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPhaseTapChangerTablePoint_Angle() {
        return ( EAttribute ) getPhaseTapChangerTablePoint().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPhaseTapChangerTablePoint_PhaseTapChangerTable() {
        return ( EReference ) getPhaseTapChangerTablePoint().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSvSwitch() {
        if( svSwitchEClass == null ) {
            svSwitchEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 60 );
        }
        return svSwitchEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSvSwitch_Open() {
        return ( EAttribute ) getSvSwitch().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSvSwitch_Phase() {
        return ( EAttribute ) getSvSwitch().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSvSwitch_Switch() {
        return ( EReference ) getSvSwitch().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getTopologicalIsland() {
        if( topologicalIslandEClass == null ) {
            topologicalIslandEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 256 );
        }
        return topologicalIslandEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTopologicalIsland_AngleRefTopologicalNode() {
        return ( EReference ) getTopologicalIsland().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTopologicalIsland_TopologicalNodes() {
        return ( EReference ) getTopologicalIsland().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDCConductingEquipment() {
        if( dcConductingEquipmentEClass == null ) {
            dcConductingEquipmentEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 317 );
        }
        return dcConductingEquipmentEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDCConductingEquipment_RatedUdc() {
        return ( EAttribute ) getDCConductingEquipment().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDCConductingEquipment_DCTerminals() {
        return ( EReference ) getDCConductingEquipment().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getProtectionEquipment() {
        if( protectionEquipmentEClass == null ) {
            protectionEquipmentEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 287 );
        }
        return protectionEquipmentEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getProtectionEquipment_HighLimit() {
        return ( EAttribute ) getProtectionEquipment().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getProtectionEquipment_LowLimit() {
        return ( EAttribute ) getProtectionEquipment().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getProtectionEquipment_PowerDirectionFlag() {
        return ( EAttribute ) getProtectionEquipment().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getProtectionEquipment_RelayDelayTime() {
        return ( EAttribute ) getProtectionEquipment().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getProtectionEquipment_UnitMultiplier() {
        return ( EAttribute ) getProtectionEquipment().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getProtectionEquipment_UnitSymbol() {
        return ( EAttribute ) getProtectionEquipment().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getProtectionEquipment_ProtectedSwitches() {
        return ( EReference ) getProtectionEquipment().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getProtectionEquipment_ConductingEquipments() {
        return ( EReference ) getProtectionEquipment().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getLoadResponseCharacteristic() {
        if( loadResponseCharacteristicEClass == null ) {
            loadResponseCharacteristicEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 313 );
        }
        return loadResponseCharacteristicEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLoadResponseCharacteristic_ExponentModel() {
        return ( EAttribute ) getLoadResponseCharacteristic().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLoadResponseCharacteristic_PConstantCurrent() {
        return ( EAttribute ) getLoadResponseCharacteristic().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLoadResponseCharacteristic_PConstantImpedance() {
        return ( EAttribute ) getLoadResponseCharacteristic().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLoadResponseCharacteristic_PConstantPower() {
        return ( EAttribute ) getLoadResponseCharacteristic().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLoadResponseCharacteristic_PFrequencyExponent() {
        return ( EAttribute ) getLoadResponseCharacteristic().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLoadResponseCharacteristic_PVoltageExponent() {
        return ( EAttribute ) getLoadResponseCharacteristic().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLoadResponseCharacteristic_QConstantCurrent() {
        return ( EAttribute ) getLoadResponseCharacteristic().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLoadResponseCharacteristic_QConstantImpedance() {
        return ( EAttribute ) getLoadResponseCharacteristic().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLoadResponseCharacteristic_QConstantPower() {
        return ( EAttribute ) getLoadResponseCharacteristic().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLoadResponseCharacteristic_QFrequencyExponent() {
        return ( EAttribute ) getLoadResponseCharacteristic().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLoadResponseCharacteristic_QVoltageExponent() {
        return ( EAttribute ) getLoadResponseCharacteristic().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getLoadResponseCharacteristic_EnergyConsumer() {
        return ( EReference ) getLoadResponseCharacteristic().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPowerElectronicsUnit() {
        if( powerElectronicsUnitEClass == null ) {
            powerElectronicsUnitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 170 );
        }
        return powerElectronicsUnitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerElectronicsUnit_MaxP() {
        return ( EAttribute ) getPowerElectronicsUnit().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerElectronicsUnit_MinP() {
        return ( EAttribute ) getPowerElectronicsUnit().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPowerElectronicsUnit_PowerElectronicsConnection() {
        return ( EReference ) getPowerElectronicsUnit().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getAnalogLimitSet() {
        if( analogLimitSetEClass == null ) {
            analogLimitSetEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 149 );
        }
        return analogLimitSetEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAnalogLimitSet_Measurements() {
        return ( EReference ) getAnalogLimitSet().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAnalogLimitSet_Limits() {
        return ( EReference ) getAnalogLimitSet().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPostLineSensor() {
        if( postLineSensorEClass == null ) {
            postLineSensorEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 145 );
        }
        return postLineSensorEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getConnectivityNodeContainer() {
        if( connectivityNodeContainerEClass == null ) {
            connectivityNodeContainerEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 131 );
        }
        return connectivityNodeContainerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getConnectivityNodeContainer_ConnectivityNodes() {
        return ( EReference ) getConnectivityNodeContainer().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getConnectivityNodeContainer_TopologicalNode() {
        return ( EReference ) getConnectivityNodeContainer().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSolarGeneratingUnit() {
        if( solarGeneratingUnitEClass == null ) {
            solarGeneratingUnitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 337 );
        }
        return solarGeneratingUnitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getAirCompressor() {
        if( airCompressorEClass == null ) {
            airCompressorEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 66 );
        }
        return airCompressorEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAirCompressor_AirCompressorRating() {
        return ( EAttribute ) getAirCompressor().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAirCompressor_CAESPlant() {
        return ( EReference ) getAirCompressor().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAirCompressor_CombustionTurbine() {
        return ( EReference ) getAirCompressor().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPhaseTapChangerAsymmetrical() {
        if( phaseTapChangerAsymmetricalEClass == null ) {
            phaseTapChangerAsymmetricalEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 96 );
        }
        return phaseTapChangerAsymmetricalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPhaseTapChangerAsymmetrical_WindingConnectionAngle() {
        return ( EAttribute ) getPhaseTapChangerAsymmetrical().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEarthFaultCompensator() {
        if( earthFaultCompensatorEClass == null ) {
            earthFaultCompensatorEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 301 );
        }
        return earthFaultCompensatorEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEarthFaultCompensator_R() {
        return ( EAttribute ) getEarthFaultCompensator().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSvShuntCompensatorSections() {
        if( svShuntCompensatorSectionsEClass == null ) {
            svShuntCompensatorSectionsEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 91 );
        }
        return svShuntCompensatorSectionsEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSvShuntCompensatorSections_Phase() {
        return ( EAttribute ) getSvShuntCompensatorSections().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSvShuntCompensatorSections_Sections() {
        return ( EAttribute ) getSvShuntCompensatorSections().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSvShuntCompensatorSections_ShuntCompensator() {
        return ( EReference ) getSvShuntCompensatorSections().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getACLineSegmentPhase() {
        if( acLineSegmentPhaseEClass == null ) {
            acLineSegmentPhaseEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 284 );
        }
        return acLineSegmentPhaseEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACLineSegmentPhase_Phase() {
        return ( EAttribute ) getACLineSegmentPhase().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getACLineSegmentPhase_SequenceNumber() {
        return ( EAttribute ) getACLineSegmentPhase().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getACLineSegmentPhase_ACLineSegment() {
        return ( EReference ) getACLineSegmentPhase().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getLinearShuntCompensatorPhase() {
        if( linearShuntCompensatorPhaseEClass == null ) {
            linearShuntCompensatorPhaseEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 243 );
        }
        return linearShuntCompensatorPhaseEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLinearShuntCompensatorPhase_BPerSection() {
        return ( EAttribute ) getLinearShuntCompensatorPhase().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLinearShuntCompensatorPhase_GPerSection() {
        return ( EAttribute ) getLinearShuntCompensatorPhase().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPetersenCoil() {
        if( petersenCoilEClass == null ) {
            petersenCoilEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 281 );
        }
        return petersenCoilEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPetersenCoil_Mode() {
        return ( EAttribute ) getPetersenCoil().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPetersenCoil_NominalU() {
        return ( EAttribute ) getPetersenCoil().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPetersenCoil_OffsetCurrent() {
        return ( EAttribute ) getPetersenCoil().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPetersenCoil_PositionCurrent() {
        return ( EAttribute ) getPetersenCoil().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPetersenCoil_XGroundMax() {
        return ( EAttribute ) getPetersenCoil().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPetersenCoil_XGroundMin() {
        return ( EAttribute ) getPetersenCoil().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPetersenCoil_XGroundNominal() {
        return ( EAttribute ) getPetersenCoil().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDCSeriesDevice() {
        if( dcSeriesDeviceEClass == null ) {
            dcSeriesDeviceEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 129 );
        }
        return dcSeriesDeviceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDCSeriesDevice_Inductance() {
        return ( EAttribute ) getDCSeriesDevice().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDCSeriesDevice_Resistance() {
        return ( EAttribute ) getDCSeriesDevice().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getConductor() {
        if( conductorEClass == null ) {
            conductorEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 275 );
        }
        return conductorEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getConductor_Length() {
        return ( EAttribute ) getConductor().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEquivalentInjection() {
        if( equivalentInjectionEClass == null ) {
            equivalentInjectionEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 338 );
        }
        return equivalentInjectionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentInjection_MaxP() {
        return ( EAttribute ) getEquivalentInjection().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentInjection_MaxQ() {
        return ( EAttribute ) getEquivalentInjection().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentInjection_MinP() {
        return ( EAttribute ) getEquivalentInjection().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentInjection_MinQ() {
        return ( EAttribute ) getEquivalentInjection().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentInjection_P() {
        return ( EAttribute ) getEquivalentInjection().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentInjection_Q() {
        return ( EAttribute ) getEquivalentInjection().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentInjection_R() {
        return ( EAttribute ) getEquivalentInjection().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentInjection_R0() {
        return ( EAttribute ) getEquivalentInjection().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentInjection_R2() {
        return ( EAttribute ) getEquivalentInjection().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentInjection_RegulationCapability() {
        return ( EAttribute ) getEquivalentInjection().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentInjection_RegulationStatus() {
        return ( EAttribute ) getEquivalentInjection().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentInjection_RegulationTarget() {
        return ( EAttribute ) getEquivalentInjection().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentInjection_X() {
        return ( EAttribute ) getEquivalentInjection().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentInjection_X0() {
        return ( EAttribute ) getEquivalentInjection().getEStructuralFeatures().get( 13 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentInjection_X2() {
        return ( EAttribute ) getEquivalentInjection().getEStructuralFeatures().get( 14 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEquivalentInjection_ReactiveCapabilityCurve() {
        return ( EReference ) getEquivalentInjection().getEStructuralFeatures().get( 15 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getFossilFuel() {
        if( fossilFuelEClass == null ) {
            fossilFuelEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 349 );
        }
        return fossilFuelEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilFuel_FossilFuelType() {
        return ( EAttribute ) getFossilFuel().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilFuel_FuelCost() {
        return ( EAttribute ) getFossilFuel().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilFuel_FuelDispatchCost() {
        return ( EAttribute ) getFossilFuel().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilFuel_FuelEffFactor() {
        return ( EAttribute ) getFossilFuel().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilFuel_FuelHandlingCost() {
        return ( EAttribute ) getFossilFuel().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilFuel_FuelHeatContent() {
        return ( EAttribute ) getFossilFuel().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilFuel_FuelMixture() {
        return ( EAttribute ) getFossilFuel().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilFuel_FuelSulfur() {
        return ( EAttribute ) getFossilFuel().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilFuel_HighBreakpointP() {
        return ( EAttribute ) getFossilFuel().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getFossilFuel_LowBreakpointP() {
        return ( EAttribute ) getFossilFuel().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getFossilFuel_FuelAllocationSchedules() {
        return ( EReference ) getFossilFuel().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getFossilFuel_ThermalGeneratingUnit() {
        return ( EReference ) getFossilFuel().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDCLineSegment() {
        if( dcLineSegmentEClass == null ) {
            dcLineSegmentEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 253 );
        }
        return dcLineSegmentEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDCLineSegment_Capacitance() {
        return ( EAttribute ) getDCLineSegment().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDCLineSegment_Inductance() {
        return ( EAttribute ) getDCLineSegment().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDCLineSegment_Length() {
        return ( EAttribute ) getDCLineSegment().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDCLineSegment_Resistance() {
        return ( EAttribute ) getDCLineSegment().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDCLineSegment_PerLengthParameter() {
        return ( EReference ) getDCLineSegment().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getAnalogValue() {
        if( analogValueEClass == null ) {
            analogValueEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 200 );
        }
        return analogValueEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getAnalogValue_Value() {
        return ( EAttribute ) getAnalogValue().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAnalogValue_AnalogControl() {
        return ( EReference ) getAnalogValue().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAnalogValue_Analog() {
        return ( EReference ) getAnalogValue().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAnalogValue_AltGeneratingUnit() {
        return ( EReference ) getAnalogValue().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getAnalogValue_AltTieMeas() {
        return ( EReference ) getAnalogValue().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getReportingSuperGroup() {
        if( reportingSuperGroupEClass == null ) {
            reportingSuperGroupEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 88 );
        }
        return reportingSuperGroupEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getReportingSuperGroup_ReportingGroup() {
        return ( EReference ) getReportingSuperGroup().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPerLengthSequenceImpedance() {
        if( perLengthSequenceImpedanceEClass == null ) {
            perLengthSequenceImpedanceEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 207 );
        }
        return perLengthSequenceImpedanceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPerLengthSequenceImpedance_B0ch() {
        return ( EAttribute ) getPerLengthSequenceImpedance().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPerLengthSequenceImpedance_Bch() {
        return ( EAttribute ) getPerLengthSequenceImpedance().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPerLengthSequenceImpedance_G0ch() {
        return ( EAttribute ) getPerLengthSequenceImpedance().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPerLengthSequenceImpedance_Gch() {
        return ( EAttribute ) getPerLengthSequenceImpedance().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPerLengthSequenceImpedance_R() {
        return ( EAttribute ) getPerLengthSequenceImpedance().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPerLengthSequenceImpedance_R0() {
        return ( EAttribute ) getPerLengthSequenceImpedance().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPerLengthSequenceImpedance_X() {
        return ( EAttribute ) getPerLengthSequenceImpedance().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPerLengthSequenceImpedance_X0() {
        return ( EAttribute ) getPerLengthSequenceImpedance().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSeason() {
        if( seasonEClass == null ) {
            seasonEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 236 );
        }
        return seasonEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSeason_EndDate() {
        return ( EAttribute ) getSeason().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSeason_StartDate() {
        return ( EAttribute ) getSeason().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSeason_SeasonDayTypeSchedules() {
        return ( EReference ) getSeason().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDiagramStyle() {
        if( diagramStyleEClass == null ) {
            diagramStyleEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 199 );
        }
        return diagramStyleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDiagramStyle_Diagram() {
        return ( EReference ) getDiagramStyle().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPerLengthLineParameter() {
        if( perLengthLineParameterEClass == null ) {
            perLengthLineParameterEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 259 );
        }
        return perLengthLineParameterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSteamTurbine() {
        if( steamTurbineEClass == null ) {
            steamTurbineEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 209 );
        }
        return steamTurbineEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSteamTurbine_CrossoverTC() {
        return ( EAttribute ) getSteamTurbine().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSteamTurbine_Reheater1TC() {
        return ( EAttribute ) getSteamTurbine().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSteamTurbine_Reheater2TC() {
        return ( EAttribute ) getSteamTurbine().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSteamTurbine_Shaft1PowerHP() {
        return ( EAttribute ) getSteamTurbine().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSteamTurbine_Shaft1PowerIP() {
        return ( EAttribute ) getSteamTurbine().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSteamTurbine_Shaft1PowerLP1() {
        return ( EAttribute ) getSteamTurbine().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSteamTurbine_Shaft1PowerLP2() {
        return ( EAttribute ) getSteamTurbine().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSteamTurbine_Shaft2PowerHP() {
        return ( EAttribute ) getSteamTurbine().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSteamTurbine_Shaft2PowerIP() {
        return ( EAttribute ) getSteamTurbine().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSteamTurbine_Shaft2PowerLP1() {
        return ( EAttribute ) getSteamTurbine().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSteamTurbine_Shaft2PowerLP2() {
        return ( EAttribute ) getSteamTurbine().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSteamTurbine_SteamChestTC() {
        return ( EAttribute ) getSteamTurbine().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSteamTurbine_SteamSupplys() {
        return ( EReference ) getSteamTurbine().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPowerTransformerEnd() {
        if( powerTransformerEndEClass == null ) {
            powerTransformerEndEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 251 );
        }
        return powerTransformerEndEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerTransformerEnd_B() {
        return ( EAttribute ) getPowerTransformerEnd().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerTransformerEnd_B0() {
        return ( EAttribute ) getPowerTransformerEnd().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerTransformerEnd_ConnectionKind() {
        return ( EAttribute ) getPowerTransformerEnd().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerTransformerEnd_G() {
        return ( EAttribute ) getPowerTransformerEnd().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerTransformerEnd_G0() {
        return ( EAttribute ) getPowerTransformerEnd().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerTransformerEnd_PhaseAngleClock() {
        return ( EAttribute ) getPowerTransformerEnd().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerTransformerEnd_R() {
        return ( EAttribute ) getPowerTransformerEnd().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerTransformerEnd_R0() {
        return ( EAttribute ) getPowerTransformerEnd().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerTransformerEnd_RatedS() {
        return ( EAttribute ) getPowerTransformerEnd().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerTransformerEnd_RatedU() {
        return ( EAttribute ) getPowerTransformerEnd().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerTransformerEnd_X() {
        return ( EAttribute ) getPowerTransformerEnd().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerTransformerEnd_X0() {
        return ( EAttribute ) getPowerTransformerEnd().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPowerTransformerEnd_PowerTransformer() {
        return ( EReference ) getPowerTransformerEnd().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getControlAreaGeneratingUnit() {
        if( controlAreaGeneratingUnitEClass == null ) {
            controlAreaGeneratingUnitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 56 );
        }
        return controlAreaGeneratingUnitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getControlAreaGeneratingUnit_GeneratingUnit() {
        return ( EReference ) getControlAreaGeneratingUnit().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getControlAreaGeneratingUnit_AltGeneratingUnitMeas() {
        return ( EReference ) getControlAreaGeneratingUnit().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getControlAreaGeneratingUnit_ControlArea() {
        return ( EReference ) getControlAreaGeneratingUnit().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getLinearShuntCompensator() {
        if( linearShuntCompensatorEClass == null ) {
            linearShuntCompensatorEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 214 );
        }
        return linearShuntCompensatorEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLinearShuntCompensator_B0PerSection() {
        return ( EAttribute ) getLinearShuntCompensator().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLinearShuntCompensator_BPerSection() {
        return ( EAttribute ) getLinearShuntCompensator().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLinearShuntCompensator_G0PerSection() {
        return ( EAttribute ) getLinearShuntCompensator().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getLinearShuntCompensator_GPerSection() {
        return ( EAttribute ) getLinearShuntCompensator().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getCTTempActivePowerCurve() {
        if( ctTempActivePowerCurveEClass == null ) {
            ctTempActivePowerCurveEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 105 );
        }
        return ctTempActivePowerCurveEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getCTTempActivePowerCurve_CombustionTurbine() {
        return ( EReference ) getCTTempActivePowerCurve().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPhaseTapChangerLinear() {
        if( phaseTapChangerLinearEClass == null ) {
            phaseTapChangerLinearEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 279 );
        }
        return phaseTapChangerLinearEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPhaseTapChangerLinear_StepPhaseShiftIncrement() {
        return ( EAttribute ) getPhaseTapChangerLinear().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPhaseTapChangerLinear_XMax() {
        return ( EAttribute ) getPhaseTapChangerLinear().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPhaseTapChangerLinear_XMin() {
        return ( EAttribute ) getPhaseTapChangerLinear().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getHydroPowerPlant() {
        if( hydroPowerPlantEClass == null ) {
            hydroPowerPlantEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 89 );
        }
        return hydroPowerPlantEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroPowerPlant_DischargeTravelDelay() {
        return ( EAttribute ) getHydroPowerPlant().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroPowerPlant_GenRatedP() {
        return ( EAttribute ) getHydroPowerPlant().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroPowerPlant_HydroPlantStorageType() {
        return ( EAttribute ) getHydroPowerPlant().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroPowerPlant_PenstockType() {
        return ( EAttribute ) getHydroPowerPlant().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroPowerPlant_PlantDischargeCapacity() {
        return ( EAttribute ) getHydroPowerPlant().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroPowerPlant_PlantRatedHead() {
        return ( EAttribute ) getHydroPowerPlant().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroPowerPlant_PumpRatedP() {
        return ( EAttribute ) getHydroPowerPlant().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroPowerPlant_SurgeTankCode() {
        return ( EAttribute ) getHydroPowerPlant().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getHydroPowerPlant_SurgeTankCrestLevel() {
        return ( EAttribute ) getHydroPowerPlant().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getHydroPowerPlant_GenSourcePumpDischargeReservoir() {
        return ( EReference ) getHydroPowerPlant().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getHydroPowerPlant_HydroPumps() {
        return ( EReference ) getHydroPowerPlant().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getHydroPowerPlant_HydroGeneratingUnits() {
        return ( EReference ) getHydroPowerPlant().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getHydroPowerPlant_Reservoir() {
        return ( EReference ) getHydroPowerPlant().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSvStatus() {
        if( svStatusEClass == null ) {
            svStatusEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 267 );
        }
        return svStatusEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSvStatus_InService() {
        return ( EAttribute ) getSvStatus().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSvStatus_Phase() {
        return ( EAttribute ) getSvStatus().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSvStatus_ConductingEquipment() {
        return ( EReference ) getSvStatus().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getHydroPumpOpSchedule() {
        if( hydroPumpOpScheduleEClass == null ) {
            hydroPumpOpScheduleEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 234 );
        }
        return hydroPumpOpScheduleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getHydroPumpOpSchedule_HydroPump() {
        return ( EReference ) getHydroPumpOpSchedule().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPotentialTransformer() {
        if( potentialTransformerEClass == null ) {
            potentialTransformerEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 126 );
        }
        return potentialTransformerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPotentialTransformer_AccuracyClass() {
        return ( EAttribute ) getPotentialTransformer().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPotentialTransformer_NominalRatio() {
        return ( EAttribute ) getPotentialTransformer().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPotentialTransformer_PtClass() {
        return ( EAttribute ) getPotentialTransformer().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPotentialTransformer_Type() {
        return ( EAttribute ) getPotentialTransformer().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getVisibilityLayer() {
        if( visibilityLayerEClass == null ) {
            visibilityLayerEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 85 );
        }
        return visibilityLayerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getVisibilityLayer_DrawingOrder() {
        return ( EAttribute ) getVisibilityLayer().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getVisibilityLayer_VisibleObjects() {
        return ( EReference ) getVisibilityLayer().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getTerminal() {
        if( terminalEClass == null ) {
            terminalEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 319 );
        }
        return terminalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getTerminal_Phases() {
        return ( EAttribute ) getTerminal().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTerminal_NormalHeadFeeder() {
        return ( EReference ) getTerminal().getEStructuralFeatures().get( 14 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTerminal_TransformerEnd() {
        return ( EReference ) getTerminal().getEStructuralFeatures().get( 13 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTerminal_TopologicalNode() {
        return ( EReference ) getTerminal().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTerminal_EquipmentFaults() {
        return ( EReference ) getTerminal().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTerminal_TieFlow() {
        return ( EReference ) getTerminal().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTerminal_ConverterDCSides() {
        return ( EReference ) getTerminal().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTerminal_HasFirstMutualCoupling() {
        return ( EReference ) getTerminal().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTerminal_ConductingEquipment() {
        return ( EReference ) getTerminal().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTerminal_HasSecondMutualCoupling() {
        return ( EReference ) getTerminal().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTerminal_SvPowerFlow() {
        return ( EReference ) getTerminal().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTerminal_BranchGroupTerminal() {
        return ( EReference ) getTerminal().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTerminal_RegulatingControl() {
        return ( EReference ) getTerminal().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTerminal_AuxiliaryEquipment() {
        return ( EReference ) getTerminal().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getTerminal_ConnectivityNode() {
        return ( EReference ) getTerminal().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getName_() {
        if( nameEClass == null ) {
            nameEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 75 );
        }
        return nameEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getName_Name() {
        return ( EAttribute ) getName_().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getName_IdentifiedObject() {
        return ( EReference ) getName_().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getName_NameType() {
        return ( EReference ) getName_().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getBranchGroupTerminal() {
        if( branchGroupTerminalEClass == null ) {
            branchGroupTerminalEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 305 );
        }
        return branchGroupTerminalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBranchGroupTerminal_PositiveFlowIn() {
        return ( EAttribute ) getBranchGroupTerminal().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getBranchGroupTerminal_Terminal() {
        return ( EReference ) getBranchGroupTerminal().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getBranchGroupTerminal_BranchGroup() {
        return ( EReference ) getBranchGroupTerminal().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSensor() {
        if( sensorEClass == null ) {
            sensorEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 353 );
        }
        return sensorEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getConductingEquipment() {
        if( conductingEquipmentEClass == null ) {
            conductingEquipmentEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 304 );
        }
        return conductingEquipmentEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getConductingEquipment_ProtectionEquipments() {
        return ( EReference ) getConductingEquipment().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getConductingEquipment_BaseVoltage() {
        return ( EReference ) getConductingEquipment().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getConductingEquipment_Terminals() {
        return ( EReference ) getConductingEquipment().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getConductingEquipment_SvStatus() {
        return ( EReference ) getConductingEquipment().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getBaseFrequency() {
        if( baseFrequencyEClass == null ) {
            baseFrequencyEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 217 );
        }
        return baseFrequencyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getBaseFrequency_Frequency() {
        return ( EAttribute ) getBaseFrequency().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSvTapStep() {
        if( svTapStepEClass == null ) {
            svTapStepEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 150 );
        }
        return svTapStepEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getSvTapStep_Position() {
        return ( EAttribute ) getSvTapStep().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSvTapStep_TapChanger() {
        return ( EReference ) getSvTapStep().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPerLengthDCLineParameter() {
        if( perLengthDCLineParameterEClass == null ) {
            perLengthDCLineParameterEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 108 );
        }
        return perLengthDCLineParameterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPerLengthDCLineParameter_Capacitance() {
        return ( EAttribute ) getPerLengthDCLineParameter().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPerLengthDCLineParameter_Inductance() {
        return ( EAttribute ) getPerLengthDCLineParameter().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPerLengthDCLineParameter_Resistance() {
        return ( EAttribute ) getPerLengthDCLineParameter().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPerLengthDCLineParameter_DCLineSegments() {
        return ( EReference ) getPerLengthDCLineParameter().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEquivalentBranch() {
        if( equivalentBranchEClass == null ) {
            equivalentBranchEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 55 );
        }
        return equivalentBranchEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentBranch_NegativeR12() {
        return ( EAttribute ) getEquivalentBranch().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentBranch_NegativeR21() {
        return ( EAttribute ) getEquivalentBranch().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentBranch_NegativeX12() {
        return ( EAttribute ) getEquivalentBranch().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentBranch_NegativeX21() {
        return ( EAttribute ) getEquivalentBranch().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentBranch_PositiveR12() {
        return ( EAttribute ) getEquivalentBranch().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentBranch_PositiveR21() {
        return ( EAttribute ) getEquivalentBranch().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentBranch_PositiveX12() {
        return ( EAttribute ) getEquivalentBranch().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentBranch_PositiveX21() {
        return ( EAttribute ) getEquivalentBranch().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentBranch_R() {
        return ( EAttribute ) getEquivalentBranch().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentBranch_R21() {
        return ( EAttribute ) getEquivalentBranch().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentBranch_X() {
        return ( EAttribute ) getEquivalentBranch().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentBranch_X21() {
        return ( EAttribute ) getEquivalentBranch().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentBranch_ZeroR12() {
        return ( EAttribute ) getEquivalentBranch().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentBranch_ZeroR21() {
        return ( EAttribute ) getEquivalentBranch().getEStructuralFeatures().get( 13 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentBranch_ZeroX12() {
        return ( EAttribute ) getEquivalentBranch().getEStructuralFeatures().get( 14 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEquivalentBranch_ZeroX21() {
        return ( EAttribute ) getEquivalentBranch().getEStructuralFeatures().get( 15 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getDCConverterUnit() {
        if( dcConverterUnitEClass == null ) {
            dcConverterUnitEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 278 );
        }
        return dcConverterUnitEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getDCConverterUnit_OperationMode() {
        return ( EAttribute ) getDCConverterUnit().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getDCConverterUnit_Substation() {
        return ( EReference ) getDCConverterUnit().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPowerTransformer() {
        if( powerTransformerEClass == null ) {
            powerTransformerEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 352 );
        }
        return powerTransformerEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerTransformer_BeforeShCircuitHighestOperatingCurrent() {
        return ( EAttribute ) getPowerTransformer().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerTransformer_BeforeShCircuitHighestOperatingVoltage() {
        return ( EAttribute ) getPowerTransformer().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerTransformer_BeforeShortCircuitAnglePf() {
        return ( EAttribute ) getPowerTransformer().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerTransformer_HighSideMinOperatingU() {
        return ( EAttribute ) getPowerTransformer().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerTransformer_IsPartOfGeneratorUnit() {
        return ( EAttribute ) getPowerTransformer().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerTransformer_OperationalValuesConsidered() {
        return ( EAttribute ) getPowerTransformer().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getPowerTransformer_VectorGroup() {
        return ( EAttribute ) getPowerTransformer().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPowerTransformer_TransformerTanks() {
        return ( EReference ) getPowerTransformer().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPowerTransformer_PowerTransformerEnd() {
        return ( EReference ) getPowerTransformer().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getGeographicalRegion() {
        if( geographicalRegionEClass == null ) {
            geographicalRegionEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 112 );
        }
        return geographicalRegionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getGeographicalRegion_Regions() {
        return ( EReference ) getGeographicalRegion().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getPhaseTapChangerTable() {
        if( phaseTapChangerTableEClass == null ) {
            phaseTapChangerTableEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 159 );
        }
        return phaseTapChangerTableEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPhaseTapChangerTable_PhaseTapChangerTablePoint() {
        return ( EReference ) getPhaseTapChangerTable().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getPhaseTapChangerTable_PhaseTapChangerTabular() {
        return ( EReference ) getPhaseTapChangerTable().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getIncrementalHeatRateCurve() {
        if( incrementalHeatRateCurveEClass == null ) {
            incrementalHeatRateCurveEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 182 );
        }
        return incrementalHeatRateCurveEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getIncrementalHeatRateCurve_IsNetGrossP() {
        return ( EAttribute ) getIncrementalHeatRateCurve().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getIncrementalHeatRateCurve_ThermalGeneratingUnit() {
        return ( EReference ) getIncrementalHeatRateCurve().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEmissionCurve() {
        if( emissionCurveEClass == null ) {
            emissionCurveEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 188 );
        }
        return emissionCurveEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEmissionCurve_EmissionContent() {
        return ( EAttribute ) getEmissionCurve().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEmissionCurve_EmissionType() {
        return ( EAttribute ) getEmissionCurve().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getEmissionCurve_IsNetGrossP() {
        return ( EAttribute ) getEmissionCurve().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEmissionCurve_ThermalGeneratingUnit() {
        return ( EReference ) getEmissionCurve().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getNonlinearShuntCompensatorPhase() {
        if( nonlinearShuntCompensatorPhaseEClass == null ) {
            nonlinearShuntCompensatorPhaseEClass = ( EClass ) EPackage.Registry.INSTANCE
                    .getEPackage( CimPackage.eNS_URI ).getEClassifiers().get( 114 );
        }
        return nonlinearShuntCompensatorPhaseEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getNonlinearShuntCompensatorPhase_NonlinearShuntCompensatorPhasePoints() {
        return ( EReference ) getNonlinearShuntCompensatorPhase().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getExternalNetworkInjection() {
        if( externalNetworkInjectionEClass == null ) {
            externalNetworkInjectionEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 142 );
        }
        return externalNetworkInjectionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getExternalNetworkInjection_GovernorSCD() {
        return ( EAttribute ) getExternalNetworkInjection().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getExternalNetworkInjection_IkSecond() {
        return ( EAttribute ) getExternalNetworkInjection().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getExternalNetworkInjection_MaxInitialSymShCCurrent() {
        return ( EAttribute ) getExternalNetworkInjection().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getExternalNetworkInjection_MaxP() {
        return ( EAttribute ) getExternalNetworkInjection().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getExternalNetworkInjection_MaxQ() {
        return ( EAttribute ) getExternalNetworkInjection().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getExternalNetworkInjection_MaxR0ToX0Ratio() {
        return ( EAttribute ) getExternalNetworkInjection().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getExternalNetworkInjection_MaxR1ToX1Ratio() {
        return ( EAttribute ) getExternalNetworkInjection().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getExternalNetworkInjection_MaxZ0ToZ1Ratio() {
        return ( EAttribute ) getExternalNetworkInjection().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getExternalNetworkInjection_MinInitialSymShCCurrent() {
        return ( EAttribute ) getExternalNetworkInjection().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getExternalNetworkInjection_MinP() {
        return ( EAttribute ) getExternalNetworkInjection().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getExternalNetworkInjection_MinQ() {
        return ( EAttribute ) getExternalNetworkInjection().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getExternalNetworkInjection_MinR0ToX0Ratio() {
        return ( EAttribute ) getExternalNetworkInjection().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getExternalNetworkInjection_MinR1ToX1Ratio() {
        return ( EAttribute ) getExternalNetworkInjection().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getExternalNetworkInjection_MinZ0ToZ1Ratio() {
        return ( EAttribute ) getExternalNetworkInjection().getEStructuralFeatures().get( 13 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getExternalNetworkInjection_P() {
        return ( EAttribute ) getExternalNetworkInjection().getEStructuralFeatures().get( 14 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getExternalNetworkInjection_Q() {
        return ( EAttribute ) getExternalNetworkInjection().getEStructuralFeatures().get( 15 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getExternalNetworkInjection_ReferencePriority() {
        return ( EAttribute ) getExternalNetworkInjection().getEStructuralFeatures().get( 16 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getExternalNetworkInjection_VoltageFactor() {
        return ( EAttribute ) getExternalNetworkInjection().getEStructuralFeatures().get( 17 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getWaveTrap() {
        if( waveTrapEClass == null ) {
            waveTrapEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 292 );
        }
        return waveTrapEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getIrregularTimePoint() {
        if( irregularTimePointEClass == null ) {
            irregularTimePointEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 210 );
        }
        return irregularTimePointEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getIrregularTimePoint_Time() {
        return ( EAttribute ) getIrregularTimePoint().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getIrregularTimePoint_Value1() {
        return ( EAttribute ) getIrregularTimePoint().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getIrregularTimePoint_Value2() {
        return ( EAttribute ) getIrregularTimePoint().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getIrregularTimePoint_IntervalSchedule() {
        return ( EReference ) getIrregularTimePoint().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getQuality61850() {
        if( quality61850EClass == null ) {
            quality61850EClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 167 );
        }
        return quality61850EClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getQuality61850_BadReference() {
        return ( EAttribute ) getQuality61850().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getQuality61850_EstimatorReplaced() {
        return ( EAttribute ) getQuality61850().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getQuality61850_Failure() {
        return ( EAttribute ) getQuality61850().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getQuality61850_OldData() {
        return ( EAttribute ) getQuality61850().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getQuality61850_OperatorBlocked() {
        return ( EAttribute ) getQuality61850().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getQuality61850_Oscillatory() {
        return ( EAttribute ) getQuality61850().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getQuality61850_OutOfRange() {
        return ( EAttribute ) getQuality61850().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getQuality61850_OverFlow() {
        return ( EAttribute ) getQuality61850().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getQuality61850_Source() {
        return ( EAttribute ) getQuality61850().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getQuality61850_Suspect() {
        return ( EAttribute ) getQuality61850().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getQuality61850_Test() {
        return ( EAttribute ) getQuality61850().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getQuality61850_Validity() {
        return ( EAttribute ) getQuality61850().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getEquivalentNetwork() {
        if( equivalentNetworkEClass == null ) {
            equivalentNetworkEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 192 );
        }
        return equivalentNetworkEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getEquivalentNetwork_EquivalentEquipments() {
        return ( EReference ) getEquivalentNetwork().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getVsCapabilityCurve() {
        if( vsCapabilityCurveEClass == null ) {
            vsCapabilityCurveEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 227 );
        }
        return vsCapabilityCurveEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getVsCapabilityCurve_VsConverterDCSides() {
        return ( EReference ) getVsCapabilityCurve().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getReservoir() {
        if( reservoirEClass == null ) {
            reservoirEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 263 );
        }
        return reservoirEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getReservoir_ActiveStorageCapacity() {
        return ( EAttribute ) getReservoir().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getReservoir_EnergyStorageRating() {
        return ( EAttribute ) getReservoir().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getReservoir_FullSupplyLevel() {
        return ( EAttribute ) getReservoir().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getReservoir_GrossCapacity() {
        return ( EAttribute ) getReservoir().getEStructuralFeatures().get( 3 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getReservoir_NormalMinOperateLevel() {
        return ( EAttribute ) getReservoir().getEStructuralFeatures().get( 4 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getReservoir_RiverOutletWorks() {
        return ( EAttribute ) getReservoir().getEStructuralFeatures().get( 5 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getReservoir_SpillTravelDelay() {
        return ( EAttribute ) getReservoir().getEStructuralFeatures().get( 6 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getReservoir_SpillwayCapacity() {
        return ( EAttribute ) getReservoir().getEStructuralFeatures().get( 7 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getReservoir_SpillwayCrestLength() {
        return ( EAttribute ) getReservoir().getEStructuralFeatures().get( 8 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getReservoir_SpillwayCrestLevel() {
        return ( EAttribute ) getReservoir().getEStructuralFeatures().get( 9 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getReservoir_SpillWayGateType() {
        return ( EAttribute ) getReservoir().getEStructuralFeatures().get( 10 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getReservoir_InflowForecasts() {
        return ( EReference ) getReservoir().getEStructuralFeatures().get( 12 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getReservoir_TargetLevelSchedule() {
        return ( EReference ) getReservoir().getEStructuralFeatures().get( 13 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getReservoir_LevelVsVolumeCurves() {
        return ( EReference ) getReservoir().getEStructuralFeatures().get( 16 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getReservoir_SpillsIntoReservoirs() {
        return ( EReference ) getReservoir().getEStructuralFeatures().get( 15 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getReservoir_SpillsFromReservoir() {
        return ( EReference ) getReservoir().getEStructuralFeatures().get( 11 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getReservoir_HydroPowerPlants() {
        return ( EReference ) getReservoir().getEStructuralFeatures().get( 14 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getReservoir_UpstreamFromHydroPowerPlants() {
        return ( EReference ) getReservoir().getEStructuralFeatures().get( 17 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getSwitchSchedule() {
        if( switchScheduleEClass == null ) {
            switchScheduleEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 157 );
        }
        return switchScheduleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getSwitchSchedule_Switch() {
        return ( EReference ) getSwitchSchedule().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getShutdownCurve() {
        if( shutdownCurveEClass == null ) {
            shutdownCurveEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 225 );
        }
        return shutdownCurveEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getShutdownCurve_ShutdownCost() {
        return ( EAttribute ) getShutdownCurve().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EAttribute getShutdownCurve_ShutdownDate() {
        return ( EAttribute ) getShutdownCurve().getEStructuralFeatures().get( 1 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getShutdownCurve_ThermalGeneratingUnit() {
        return ( EReference ) getShutdownCurve().getEStructuralFeatures().get( 2 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EClass getRemotePoint() {
        if( remotePointEClass == null ) {
            remotePointEClass = ( EClass ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 205 );
        }
        return remotePointEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EReference getRemotePoint_RemoteUnit() {
        return ( EReference ) getRemotePoint().getEStructuralFeatures().get( 0 );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getVsPpccControlKind() {
        if( vsPpccControlKindEEnum == null ) {
            vsPpccControlKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 23 );
        }
        return vsPpccControlKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getCsOperatingModeKind() {
        if( csOperatingModeKindEEnum == null ) {
            csOperatingModeKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 31 );
        }
        return csOperatingModeKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getTransformerControlMode() {
        if( transformerControlModeEEnum == null ) {
            transformerControlModeEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 40 );
        }
        return transformerControlModeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getPhaseShuntConnectionKind() {
        if( phaseShuntConnectionKindEEnum == null ) {
            phaseShuntConnectionKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 13 );
        }
        return phaseShuntConnectionKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getOperationalLimitDirectionKind() {
        if( operationalLimitDirectionKindEEnum == null ) {
            operationalLimitDirectionKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 36 );
        }
        return operationalLimitDirectionKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getPhaseConnectedFaultKind() {
        if( phaseConnectedFaultKindEEnum == null ) {
            phaseConnectedFaultKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 28 );
        }
        return phaseConnectedFaultKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getBatteryStateKind() {
        if( batteryStateKindEEnum == null ) {
            batteryStateKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 2 );
        }
        return batteryStateKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getCurveStyle() {
        if( curveStyleEEnum == null ) {
            curveStyleEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 21 );
        }
        return curveStyleEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getCurrency() {
        if( currencyEEnum == null ) {
            currencyEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 18 );
        }
        return currencyEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getWindingConnection() {
        if( windingConnectionEEnum == null ) {
            windingConnectionEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 27 );
        }
        return windingConnectionEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getWindGenUnitKind() {
        if( windGenUnitKindEEnum == null ) {
            windGenUnitKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 44 );
        }
        return windGenUnitKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getSinglePhaseKind() {
        if( singlePhaseKindEEnum == null ) {
            singlePhaseKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 1 );
        }
        return singlePhaseKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getPhaseCode() {
        if( phaseCodeEEnum == null ) {
            phaseCodeEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 33 );
        }
        return phaseCodeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getBreakerConfiguration() {
        if( breakerConfigurationEEnum == null ) {
            breakerConfigurationEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 39 );
        }
        return breakerConfigurationEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getRegulatingControlModeKind() {
        if( regulatingControlModeKindEEnum == null ) {
            regulatingControlModeKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 43 );
        }
        return regulatingControlModeKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getDCConverterOperatingModeKind() {
        if( dcConverterOperatingModeKindEEnum == null ) {
            dcConverterOperatingModeKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 32 );
        }
        return dcConverterOperatingModeKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getBoilerControlMode() {
        if( boilerControlModeEEnum == null ) {
            boilerControlModeEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 41 );
        }
        return boilerControlModeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getCoolantType() {
        if( coolantTypeEEnum == null ) {
            coolantTypeEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 16 );
        }
        return coolantTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getHydroEnergyConversionKind() {
        if( hydroEnergyConversionKindEEnum == null ) {
            hydroEnergyConversionKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 8 );
        }
        return hydroEnergyConversionKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getEmissionType() {
        if( emissionTypeEEnum == null ) {
            emissionTypeEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 6 );
        }
        return emissionTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getPetersenCoilModeKind() {
        if( petersenCoilModeKindEEnum == null ) {
            petersenCoilModeKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 20 );
        }
        return petersenCoilModeKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getUnitMultiplier() {
        if( unitMultiplierEEnum == null ) {
            unitMultiplierEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 10 );
        }
        return unitMultiplierEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getContingencyEquipmentStatusKind() {
        if( contingencyEquipmentStatusKindEEnum == null ) {
            contingencyEquipmentStatusKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 25 );
        }
        return contingencyEquipmentStatusKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getValidity() {
        if( validityEEnum == null ) {
            validityEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 45 );
        }
        return validityEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getControlAreaTypeKind() {
        if( controlAreaTypeKindEEnum == null ) {
            controlAreaTypeKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 38 );
        }
        return controlAreaTypeKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getPotentialTransformerKind() {
        if( potentialTransformerKindEEnum == null ) {
            potentialTransformerKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 15 );
        }
        return potentialTransformerKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getUnitSymbol() {
        if( unitSymbolEEnum == null ) {
            unitSymbolEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 42 );
        }
        return unitSymbolEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getBusbarConfiguration() {
        if( busbarConfigurationEEnum == null ) {
            busbarConfigurationEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 3 );
        }
        return busbarConfigurationEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getGeneratorControlSource() {
        if( generatorControlSourceEEnum == null ) {
            generatorControlSourceEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 5 );
        }
        return generatorControlSourceEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getSource() {
        if( sourceEEnum == null ) {
            sourceEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 34 );
        }
        return sourceEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getAsynchronousMachineKind() {
        if( asynchronousMachineKindEEnum == null ) {
            asynchronousMachineKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 12 );
        }
        return asynchronousMachineKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getSVCControlMode() {
        if( svcControlModeEEnum == null ) {
            svcControlModeEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 19 );
        }
        return svcControlModeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getHydroPlantStorageKind() {
        if( hydroPlantStorageKindEEnum == null ) {
            hydroPlantStorageKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 24 );
        }
        return hydroPlantStorageKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getRemoteUnitType() {
        if( remoteUnitTypeEEnum == null ) {
            remoteUnitTypeEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 14 );
        }
        return remoteUnitTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getOrientationKind() {
        if( orientationKindEEnum == null ) {
            orientationKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 4 );
        }
        return orientationKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getSynchronousMachineKind() {
        if( synchronousMachineKindEEnum == null ) {
            synchronousMachineKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 35 );
        }
        return synchronousMachineKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getCsPpccControlKind() {
        if( csPpccControlKindEEnum == null ) {
            csPpccControlKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 11 );
        }
        return csPpccControlKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getFuelType() {
        if( fuelTypeEEnum == null ) {
            fuelTypeEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI ).getEClassifiers()
                    .get( 37 );
        }
        return fuelTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getVsQpccControlKind() {
        if( vsQpccControlKindEEnum == null ) {
            vsQpccControlKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 7 );
        }
        return vsQpccControlKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getDCPolarityKind() {
        if( dcPolarityKindEEnum == null ) {
            dcPolarityKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 30 );
        }
        return dcPolarityKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getSynchronousMachineOperatingMode() {
        if( synchronousMachineOperatingModeEEnum == null ) {
            synchronousMachineOperatingModeEEnum = ( EEnum ) EPackage.Registry.INSTANCE
                    .getEPackage( CimPackage.eNS_URI ).getEClassifiers().get( 17 );
        }
        return synchronousMachineOperatingModeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getShortCircuitRotorKind() {
        if( shortCircuitRotorKindEEnum == null ) {
            shortCircuitRotorKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 29 );
        }
        return shortCircuitRotorKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getHydroTurbineKind() {
        if( hydroTurbineKindEEnum == null ) {
            hydroTurbineKindEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 9 );
        }
        return hydroTurbineKindEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getGeneratorControlMode() {
        if( generatorControlModeEEnum == null ) {
            generatorControlModeEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 26 );
        }
        return generatorControlModeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public EEnum getEmissionValueSource() {
        if( emissionValueSourceEEnum == null ) {
            emissionValueSourceEEnum = ( EEnum ) EPackage.Registry.INSTANCE.getEPackage( CimPackage.eNS_URI )
                    .getEClassifiers().get( 22 );
        }
        return emissionValueSourceEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public CimFactory getCimFactory() {
        return ( CimFactory ) getEFactoryInstance();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private boolean isLoaded = false;

    /**
     * Laods the package and any sub-packages from their serialized form.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void loadPackage() {
        if( isLoaded ) return;
        isLoaded = true;

        URL url = getClass().getResource( packageFilename );
        if( url == null ) {
            throw new RuntimeException( "Missing serialized package: " + packageFilename );
        }
        URI uri = URI.createURI( url.toString() );
        Resource resource = new EcoreResourceFactoryImpl().createResource( uri );
        try {
            resource.load( null );
        }
        catch( IOException exception ) {
            throw new WrappedException( exception );
        }
        initializeFromLoadedEPackage( this, ( EPackage ) resource.getContents().get( 0 ) );
        createResource( eNS_URI );
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    private boolean isFixed = false;

    /**
     * Fixes up the loaded package, to make it appear as if it had been programmatically built.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void fixPackageContents() {
        if( isFixed ) return;
        isFixed = true;
        fixEClassifiers();
    }

    /**
     * Sets the instance class on the given classifier.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected void fixInstanceClass( EClassifier eClassifier ) {
        if( eClassifier.getInstanceClassName() == null ) {
            eClassifier.setInstanceClassName(
                    "fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim." + eClassifier.getName() );
            setGeneratedClassName( eClassifier );
        }
    }

} //CimPackageImpl
