/*
*************************************************************************
**  Copyright (c) 2020 CentraleSupélec & EDF.
**  All rights reserved. This program and the accompanying materials
**  are made available under the terms of the Eclipse Public License v2.0
**  which accompanies this distribution, and is available at
**  https://www.eclipse.org/legal/epl-v20.html
** 
**  This file is part of the RiseClipse tool
**  
**  Contributors:
**      Computer Science Department, CentraleSupélec
**      EDF R&D
**  Contacts:
**      dominique.marcadet@centralesupelec.fr
**      aurelie.dehouck-neveu@edf.fr
**  Web site:
**      http://wdi.supelec.fr/software/RiseClipse/
*************************************************************************
*/
package fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Battery Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.BatteryUnit#getBatteryState <em>Battery State</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.BatteryUnit#getRatedE <em>Rated E</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.BatteryUnit#getStoredE <em>Stored E</em>}</li>
 * </ul>
 *
 * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getBatteryUnit()
 * @model
 * @generated
 */
public interface BatteryUnit extends PowerElectronicsUnit {
    /**
     * Returns the value of the '<em><b>Battery State</b></em>' attribute.
     * The literals are from the enumeration {@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.BatteryStateKind}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Battery State</em>' attribute.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.BatteryStateKind
     * @see #isSetBatteryState()
     * @see #unsetBatteryState()
     * @see #setBatteryState(BatteryStateKind)
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getBatteryUnit_BatteryState()
     * @model unsettable="true"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='BatteryUnit.batteryState' kind='element'"
     * @generated
     */
    BatteryStateKind getBatteryState();

    /**
     * Sets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.BatteryUnit#getBatteryState <em>Battery State</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Battery State</em>' attribute.
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.BatteryStateKind
     * @see #isSetBatteryState()
     * @see #unsetBatteryState()
     * @see #getBatteryState()
     * @generated
     */
    void setBatteryState( BatteryStateKind value );

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.BatteryUnit#getBatteryState <em>Battery State</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetBatteryState()
     * @see #getBatteryState()
     * @see #setBatteryState(BatteryStateKind)
     * @generated
     */
    void unsetBatteryState();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.BatteryUnit#getBatteryState <em>Battery State</em>}' attribute is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>Battery State</em>' attribute is set.
     * @see #unsetBatteryState()
     * @see #getBatteryState()
     * @see #setBatteryState(BatteryStateKind)
     * @generated
     */
    boolean isSetBatteryState();

    /**
     * Returns the value of the '<em><b>Rated E</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Rated E</em>' attribute.
     * @see #isSetRatedE()
     * @see #unsetRatedE()
     * @see #setRatedE(Float)
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getBatteryUnit_RatedE()
     * @model unsettable="true"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='BatteryUnit.ratedE' kind='element'"
     * @generated
     */
    Float getRatedE();

    /**
     * Sets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.BatteryUnit#getRatedE <em>Rated E</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Rated E</em>' attribute.
     * @see #isSetRatedE()
     * @see #unsetRatedE()
     * @see #getRatedE()
     * @generated
     */
    void setRatedE( Float value );

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.BatteryUnit#getRatedE <em>Rated E</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetRatedE()
     * @see #getRatedE()
     * @see #setRatedE(Float)
     * @generated
     */
    void unsetRatedE();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.BatteryUnit#getRatedE <em>Rated E</em>}' attribute is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>Rated E</em>' attribute is set.
     * @see #unsetRatedE()
     * @see #getRatedE()
     * @see #setRatedE(Float)
     * @generated
     */
    boolean isSetRatedE();

    /**
     * Returns the value of the '<em><b>Stored E</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Stored E</em>' attribute.
     * @see #isSetStoredE()
     * @see #unsetStoredE()
     * @see #setStoredE(Float)
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getBatteryUnit_StoredE()
     * @model unsettable="true"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='BatteryUnit.storedE' kind='element'"
     * @generated
     */
    Float getStoredE();

    /**
     * Sets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.BatteryUnit#getStoredE <em>Stored E</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Stored E</em>' attribute.
     * @see #isSetStoredE()
     * @see #unsetStoredE()
     * @see #getStoredE()
     * @generated
     */
    void setStoredE( Float value );

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.BatteryUnit#getStoredE <em>Stored E</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetStoredE()
     * @see #getStoredE()
     * @see #setStoredE(Float)
     * @generated
     */
    void unsetStoredE();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.BatteryUnit#getStoredE <em>Stored E</em>}' attribute is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>Stored E</em>' attribute is set.
     * @see #unsetStoredE()
     * @see #getStoredE()
     * @see #setStoredE(Float)
     * @generated
     */
    boolean isSetStoredE();

} // BatteryUnit
