/*
*************************************************************************
**  Copyright (c) 2020 CentraleSupélec & EDF.
**  All rights reserved. This program and the accompanying materials
**  are made available under the terms of the Eclipse Public License v2.0
**  which accompanies this distribution, and is available at
**  https://www.eclipse.org/legal/epl-v20.html
** 
**  This file is part of the RiseClipse tool
**  
**  Contributors:
**      Computer Science Department, CentraleSupélec
**      EDF R&D
**  Contacts:
**      dominique.marcadet@centralesupelec.fr
**      aurelie.dehouck-neveu@edf.fr
**  Web site:
**      http://wdi.supelec.fr/software/RiseClipse/
*************************************************************************
*/
package fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Substation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getNamingFeeder <em>Naming Feeder</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getBays <em>Bays</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getRegion <em>Region</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getNormalEnergizedFeeder <em>Normal Energized Feeder</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getVoltageLevels <em>Voltage Levels</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getNormalEnergizingFeeder <em>Normal Energizing Feeder</em>}</li>
 *   <li>{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getDCConverterUnit <em>DC Converter Unit</em>}</li>
 * </ul>
 *
 * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getSubstation()
 * @model
 * @generated
 */
public interface Substation extends EquipmentContainer {
    /**
     * Returns the value of the '<em><b>DC Converter Unit</b></em>' reference list.
     * The list contents are of type {@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.DCConverterUnit}.
     * It is bidirectional and its opposite is '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.DCConverterUnit#getSubstation <em>Substation</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>DC Converter Unit</em>' reference list.
     * @see #isSetDCConverterUnit()
     * @see #unsetDCConverterUnit()
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getSubstation_DCConverterUnit()
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.DCConverterUnit#getSubstation
     * @model opposite="Substation" unsettable="true" transient="true" ordered="false"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='Substation.DCConverterUnit' kind='element'"
     * @generated
     */
    EList< DCConverterUnit > getDCConverterUnit();

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getDCConverterUnit <em>DC Converter Unit</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetDCConverterUnit()
     * @see #getDCConverterUnit()
     * @generated
     */
    void unsetDCConverterUnit();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getDCConverterUnit <em>DC Converter Unit</em>}' reference list is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>DC Converter Unit</em>' reference list is set.
     * @see #unsetDCConverterUnit()
     * @see #getDCConverterUnit()
     * @generated
     */
    boolean isSetDCConverterUnit();

    /**
     * Returns the value of the '<em><b>Naming Feeder</b></em>' reference.
     * It is bidirectional and its opposite is '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder#getNamingSecondarySubstation <em>Naming Secondary Substation</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Naming Feeder</em>' reference.
     * @see #isSetNamingFeeder()
     * @see #unsetNamingFeeder()
     * @see #setNamingFeeder(Feeder)
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getSubstation_NamingFeeder()
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder#getNamingSecondarySubstation
     * @model opposite="NamingSecondarySubstation" resolveProxies="false" unsettable="true" ordered="false"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='Substation.NamingFeeder' kind='element'"
     * @generated
     */
    Feeder getNamingFeeder();

    /**
     * Sets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getNamingFeeder <em>Naming Feeder</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Naming Feeder</em>' reference.
     * @see #isSetNamingFeeder()
     * @see #unsetNamingFeeder()
     * @see #getNamingFeeder()
     * @generated
     */
    void setNamingFeeder( Feeder value );

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getNamingFeeder <em>Naming Feeder</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetNamingFeeder()
     * @see #getNamingFeeder()
     * @see #setNamingFeeder(Feeder)
     * @generated
     */
    void unsetNamingFeeder();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getNamingFeeder <em>Naming Feeder</em>}' reference is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>Naming Feeder</em>' reference is set.
     * @see #unsetNamingFeeder()
     * @see #getNamingFeeder()
     * @see #setNamingFeeder(Feeder)
     * @generated
     */
    boolean isSetNamingFeeder();

    /**
     * Returns the value of the '<em><b>Bays</b></em>' reference list.
     * The list contents are of type {@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Bay}.
     * It is bidirectional and its opposite is '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Bay#getSubstation <em>Substation</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Bays</em>' reference list.
     * @see #isSetBays()
     * @see #unsetBays()
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getSubstation_Bays()
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Bay#getSubstation
     * @model opposite="Substation" unsettable="true" transient="true" ordered="false"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='Substation.Bays' kind='element'"
     * @generated
     */
    EList< Bay > getBays();

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getBays <em>Bays</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetBays()
     * @see #getBays()
     * @generated
     */
    void unsetBays();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getBays <em>Bays</em>}' reference list is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>Bays</em>' reference list is set.
     * @see #unsetBays()
     * @see #getBays()
     * @generated
     */
    boolean isSetBays();

    /**
     * Returns the value of the '<em><b>Voltage Levels</b></em>' reference list.
     * The list contents are of type {@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.VoltageLevel}.
     * It is bidirectional and its opposite is '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.VoltageLevel#getSubstation <em>Substation</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Voltage Levels</em>' reference list.
     * @see #isSetVoltageLevels()
     * @see #unsetVoltageLevels()
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getSubstation_VoltageLevels()
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.VoltageLevel#getSubstation
     * @model opposite="Substation" unsettable="true" transient="true" ordered="false"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='Substation.VoltageLevels' kind='element'"
     * @generated
     */
    EList< VoltageLevel > getVoltageLevels();

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getVoltageLevels <em>Voltage Levels</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetVoltageLevels()
     * @see #getVoltageLevels()
     * @generated
     */
    void unsetVoltageLevels();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getVoltageLevels <em>Voltage Levels</em>}' reference list is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>Voltage Levels</em>' reference list is set.
     * @see #unsetVoltageLevels()
     * @see #getVoltageLevels()
     * @generated
     */
    boolean isSetVoltageLevels();

    /**
     * Returns the value of the '<em><b>Region</b></em>' reference.
     * It is bidirectional and its opposite is '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.SubGeographicalRegion#getSubstations <em>Substations</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Region</em>' reference.
     * @see #isSetRegion()
     * @see #unsetRegion()
     * @see #setRegion(SubGeographicalRegion)
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getSubstation_Region()
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.SubGeographicalRegion#getSubstations
     * @model opposite="Substations" resolveProxies="false" unsettable="true" ordered="false"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='Substation.Region' kind='element'"
     * @generated
     */
    SubGeographicalRegion getRegion();

    /**
     * Sets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getRegion <em>Region</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @param value the new value of the '<em>Region</em>' reference.
     * @see #isSetRegion()
     * @see #unsetRegion()
     * @see #getRegion()
     * @generated
     */
    void setRegion( SubGeographicalRegion value );

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getRegion <em>Region</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetRegion()
     * @see #getRegion()
     * @see #setRegion(SubGeographicalRegion)
     * @generated
     */
    void unsetRegion();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getRegion <em>Region</em>}' reference is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>Region</em>' reference is set.
     * @see #unsetRegion()
     * @see #getRegion()
     * @see #setRegion(SubGeographicalRegion)
     * @generated
     */
    boolean isSetRegion();

    /**
     * Returns the value of the '<em><b>Normal Energized Feeder</b></em>' reference list.
     * The list contents are of type {@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder}.
     * It is bidirectional and its opposite is '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder#getNormalEnergizingSubstation <em>Normal Energizing Substation</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Normal Energized Feeder</em>' reference list.
     * @see #isSetNormalEnergizedFeeder()
     * @see #unsetNormalEnergizedFeeder()
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getSubstation_NormalEnergizedFeeder()
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder#getNormalEnergizingSubstation
     * @model opposite="NormalEnergizingSubstation" unsettable="true" transient="true" ordered="false"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='Substation.NormalEnergizedFeeder' kind='element'"
     * @generated
     */
    EList< Feeder > getNormalEnergizedFeeder();

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getNormalEnergizedFeeder <em>Normal Energized Feeder</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetNormalEnergizedFeeder()
     * @see #getNormalEnergizedFeeder()
     * @generated
     */
    void unsetNormalEnergizedFeeder();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getNormalEnergizedFeeder <em>Normal Energized Feeder</em>}' reference list is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>Normal Energized Feeder</em>' reference list is set.
     * @see #unsetNormalEnergizedFeeder()
     * @see #getNormalEnergizedFeeder()
     * @generated
     */
    boolean isSetNormalEnergizedFeeder();

    /**
     * Returns the value of the '<em><b>Normal Energizing Feeder</b></em>' reference list.
     * The list contents are of type {@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder}.
     * It is bidirectional and its opposite is '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder#getNormalEnergizedSubstation <em>Normal Energized Substation</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the value of the '<em>Normal Energizing Feeder</em>' reference list.
     * @see #isSetNormalEnergizingFeeder()
     * @see #unsetNormalEnergizingFeeder()
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage#getSubstation_NormalEnergizingFeeder()
     * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Feeder#getNormalEnergizedSubstation
     * @model opposite="NormalEnergizedSubstation" unsettable="true" transient="true" ordered="false"
     *        extendedMetaData="namespace='http://iec.ch/TC57/2016/CIM-schema-cim17' name='Substation.NormalEnergizingFeeder' kind='element'"
     * @generated
     */
    EList< Feeder > getNormalEnergizingFeeder();

    /**
     * Unsets the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getNormalEnergizingFeeder <em>Normal Energizing Feeder</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #isSetNormalEnergizingFeeder()
     * @see #getNormalEnergizingFeeder()
     * @generated
     */
    void unsetNormalEnergizingFeeder();

    /**
     * Returns whether the value of the '{@link fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.Substation#getNormalEnergizingFeeder <em>Normal Energizing Feeder</em>}' reference list is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return whether the value of the '<em>Normal Energizing Feeder</em>' reference list is set.
     * @see #unsetNormalEnergizingFeeder()
     * @see #getNormalEnergizingFeeder()
     * @generated
     */
    boolean isSetNormalEnergizingFeeder();

} // Substation
