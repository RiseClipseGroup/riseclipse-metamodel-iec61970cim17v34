/*
*************************************************************************
**  Copyright (c) 2020 CentraleSupélec & EDF.
**  All rights reserved. This program and the accompanying materials
**  are made available under the terms of the Eclipse Public License v2.0
**  which accompanies this distribution, and is available at
**  https://www.eclipse.org/legal/epl-v20.html
** 
**  This file is part of the RiseClipse tool
**  
**  Contributors:
**      Computer Science Department, CentraleSupélec
**      EDF R&D
**  Contacts:
**      dominique.marcadet@centralesupelec.fr
**      aurelie.dehouck-neveu@edf.fr
**  Web site:
**      http://wdi.supelec.fr/software/RiseClipse/
*************************************************************************
*/
package fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.CimPackage
 * @generated
 */
public interface CimFactory extends EFactory {
    /**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    CimFactory eINSTANCE = fr.centralesupelec.edf.riseclipse.cim.cim17.iec61970cim17v34.cim.impl.CimFactoryImpl.init();

    /**
     * Returns a new object of class '<em>Date Interval</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Date Interval</em>'.
     * @generated
     */
    DateInterval createDateInterval();

    /**
     * Returns a new object of class '<em>Month Day Interval</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Month Day Interval</em>'.
     * @generated
     */
    MonthDayInterval createMonthDayInterval();

    /**
     * Returns a new object of class '<em>String Quantity</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>String Quantity</em>'.
     * @generated
     */
    StringQuantity createStringQuantity();

    /**
     * Returns a new object of class '<em>Time Interval</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Time Interval</em>'.
     * @generated
     */
    TimeInterval createTimeInterval();

    /**
     * Returns a new object of class '<em>Fault Impedance</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Fault Impedance</em>'.
     * @generated
     */
    FaultImpedance createFaultImpedance();

    /**
     * Returns a new object of class '<em>Decimal Quantity</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Decimal Quantity</em>'.
     * @generated
     */
    DecimalQuantity createDecimalQuantity();

    /**
     * Returns a new object of class '<em>Date Time Interval</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Date Time Interval</em>'.
     * @generated
     */
    DateTimeInterval createDateTimeInterval();

    /**
     * Returns a new object of class '<em>Float Quantity</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Float Quantity</em>'.
     * @generated
     */
    FloatQuantity createFloatQuantity();

    /**
     * Returns a new object of class '<em>Integer Quantity</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Integer Quantity</em>'.
     * @generated
     */
    IntegerQuantity createIntegerQuantity();

    /**
     * Returns a new object of class '<em>Composite Switch</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Composite Switch</em>'.
     * @generated
     */
    CompositeSwitch createCompositeSwitch();

    /**
     * Returns a new object of class '<em>DC Line</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>DC Line</em>'.
     * @generated
     */
    DCLine createDCLine();

    /**
     * Returns a new object of class '<em>Penstock Loss Curve</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Penstock Loss Curve</em>'.
     * @generated
     */
    PenstockLossCurve createPenstockLossCurve();

    /**
     * Returns a new object of class '<em>IEC61970CIM Version</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>IEC61970CIM Version</em>'.
     * @generated
     */
    IEC61970CIMVersion createIEC61970CIMVersion();

    /**
     * Returns a new object of class '<em>DC Busbar</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>DC Busbar</em>'.
     * @generated
     */
    DCBusbar createDCBusbar();

    /**
     * Returns a new object of class '<em>Regulating Cond Eq</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Regulating Cond Eq</em>'.
     * @generated
     */
    RegulatingCondEq createRegulatingCondEq();

    /**
     * Returns a new object of class '<em>DC Disconnector</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>DC Disconnector</em>'.
     * @generated
     */
    DCDisconnector createDCDisconnector();

    /**
     * Returns a new object of class '<em>Reactive Capability Curve</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Reactive Capability Curve</em>'.
     * @generated
     */
    ReactiveCapabilityCurve createReactiveCapabilityCurve();

    /**
     * Returns a new object of class '<em>Recloser</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Recloser</em>'.
     * @generated
     */
    Recloser createRecloser();

    /**
     * Returns a new object of class '<em>Auxiliary Equipment</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Auxiliary Equipment</em>'.
     * @generated
     */
    AuxiliaryEquipment createAuxiliaryEquipment();

    /**
     * Returns a new object of class '<em>Discrete</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Discrete</em>'.
     * @generated
     */
    Discrete createDiscrete();

    /**
     * Returns a new object of class '<em>Rotating Machine</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Rotating Machine</em>'.
     * @generated
     */
    RotatingMachine createRotatingMachine();

    /**
     * Returns a new object of class '<em>Nonlinear Shunt Compensator Phase Point</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Nonlinear Shunt Compensator Phase Point</em>'.
     * @generated
     */
    NonlinearShuntCompensatorPhasePoint createNonlinearShuntCompensatorPhasePoint();

    /**
     * Returns a new object of class '<em>Operating Share</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Operating Share</em>'.
     * @generated
     */
    OperatingShare createOperatingShare();

    /**
     * Returns a new object of class '<em>Frequency Converter</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Frequency Converter</em>'.
     * @generated
     */
    FrequencyConverter createFrequencyConverter();

    /**
     * Returns a new object of class '<em>Analog Control</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Analog Control</em>'.
     * @generated
     */
    AnalogControl createAnalogControl();

    /**
     * Returns a new object of class '<em>Switch</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Switch</em>'.
     * @generated
     */
    Switch createSwitch();

    /**
     * Returns a new object of class '<em>Inflow Forecast</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Inflow Forecast</em>'.
     * @generated
     */
    InflowForecast createInflowForecast();

    /**
     * Returns a new object of class '<em>Protected Switch</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Protected Switch</em>'.
     * @generated
     */
    ProtectedSwitch createProtectedSwitch();

    /**
     * Returns a new object of class '<em>Emission Account</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Emission Account</em>'.
     * @generated
     */
    EmissionAccount createEmissionAccount();

    /**
     * Returns a new object of class '<em>Load Break Switch</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Load Break Switch</em>'.
     * @generated
     */
    LoadBreakSwitch createLoadBreakSwitch();

    /**
     * Returns a new object of class '<em>Current Relay</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Current Relay</em>'.
     * @generated
     */
    CurrentRelay createCurrentRelay();

    /**
     * Returns a new object of class '<em>Remote Control</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Remote Control</em>'.
     * @generated
     */
    RemoteControl createRemoteControl();

    /**
     * Returns a new object of class '<em>Steam Sendout Schedule</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Steam Sendout Schedule</em>'.
     * @generated
     */
    SteamSendoutSchedule createSteamSendoutSchedule();

    /**
     * Returns a new object of class '<em>Start Ign Fuel Curve</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Start Ign Fuel Curve</em>'.
     * @generated
     */
    StartIgnFuelCurve createStartIgnFuelCurve();

    /**
     * Returns a new object of class '<em>Static Var Compensator</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Static Var Compensator</em>'.
     * @generated
     */
    StaticVarCompensator createStaticVarCompensator();

    /**
     * Returns a new object of class '<em>DC Switch</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>DC Switch</em>'.
     * @generated
     */
    DCSwitch createDCSwitch();

    /**
     * Returns a new object of class '<em>BWR Steam Supply</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>BWR Steam Supply</em>'.
     * @generated
     */
    BWRSteamSupply createBWRSteamSupply();

    /**
     * Returns a new object of class '<em>Control</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Control</em>'.
     * @generated
     */
    Control createControl();

    /**
     * Returns a new object of class '<em>Day Type</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Day Type</em>'.
     * @generated
     */
    DayType createDayType();

    /**
     * Returns a new object of class '<em>Connectivity Node</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Connectivity Node</em>'.
     * @generated
     */
    ConnectivityNode createConnectivityNode();

    /**
     * Returns a new object of class '<em>Combustion Turbine</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Combustion Turbine</em>'.
     * @generated
     */
    CombustionTurbine createCombustionTurbine();

    /**
     * Returns a new object of class '<em>Ratio Tap Changer Table</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Ratio Tap Changer Table</em>'.
     * @generated
     */
    RatioTapChangerTable createRatioTapChangerTable();

    /**
     * Returns a new object of class '<em>Value To Alias</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Value To Alias</em>'.
     * @generated
     */
    ValueToAlias createValueToAlias();

    /**
     * Returns a new object of class '<em>Gen Unit Op Cost Curve</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Gen Unit Op Cost Curve</em>'.
     * @generated
     */
    GenUnitOpCostCurve createGenUnitOpCostCurve();

    /**
     * Returns a new object of class '<em>Phase Tap Changer Non Linear</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Phase Tap Changer Non Linear</em>'.
     * @generated
     */
    PhaseTapChangerNonLinear createPhaseTapChangerNonLinear();

    /**
     * Returns a new object of class '<em>Name Type Authority</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Name Type Authority</em>'.
     * @generated
     */
    NameTypeAuthority createNameTypeAuthority();

    /**
     * Returns a new object of class '<em>ACDC Terminal</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>ACDC Terminal</em>'.
     * @generated
     */
    ACDCTerminal createACDCTerminal();

    /**
     * Returns a new object of class '<em>Text Diagram Object</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Text Diagram Object</em>'.
     * @generated
     */
    TextDiagramObject createTextDiagramObject();

    /**
     * Returns a new object of class '<em>Nuclear Generating Unit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Nuclear Generating Unit</em>'.
     * @generated
     */
    NuclearGeneratingUnit createNuclearGeneratingUnit();

    /**
     * Returns a new object of class '<em>Clamp</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Clamp</em>'.
     * @generated
     */
    Clamp createClamp();

    /**
     * Returns a new object of class '<em>Apparent Power Limit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Apparent Power Limit</em>'.
     * @generated
     */
    ApparentPowerLimit createApparentPowerLimit();

    /**
     * Returns a new object of class '<em>String Measurement Value</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>String Measurement Value</em>'.
     * @generated
     */
    StringMeasurementValue createStringMeasurementValue();

    /**
     * Returns a new object of class '<em>Equipment Container</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Equipment Container</em>'.
     * @generated
     */
    EquipmentContainer createEquipmentContainer();

    /**
     * Returns a new object of class '<em>Subcritical</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Subcritical</em>'.
     * @generated
     */
    Subcritical createSubcritical();

    /**
     * Returns a new object of class '<em>Tap Changer Control</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Tap Changer Control</em>'.
     * @generated
     */
    TapChangerControl createTapChangerControl();

    /**
     * Returns a new object of class '<em>Hydro Pump</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Hydro Pump</em>'.
     * @generated
     */
    HydroPump createHydroPump();

    /**
     * Returns a new object of class '<em>Asynchronous Machine</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Asynchronous Machine</em>'.
     * @generated
     */
    AsynchronousMachine createAsynchronousMachine();

    /**
     * Returns a new object of class '<em>Energy Consumer Phase</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Energy Consumer Phase</em>'.
     * @generated
     */
    EnergyConsumerPhase createEnergyConsumerPhase();

    /**
     * Returns a new object of class '<em>Contingency Element</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Contingency Element</em>'.
     * @generated
     */
    ContingencyElement createContingencyElement();

    /**
     * Returns a new object of class '<em>Power Electronics Connection Phase</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Power Electronics Connection Phase</em>'.
     * @generated
     */
    PowerElectronicsConnectionPhase createPowerElectronicsConnectionPhase();

    /**
     * Returns a new object of class '<em>Series Compensator</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Series Compensator</em>'.
     * @generated
     */
    SeriesCompensator createSeriesCompensator();

    /**
     * Returns a new object of class '<em>Diagram Object Point</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Diagram Object Point</em>'.
     * @generated
     */
    DiagramObjectPoint createDiagramObjectPoint();

    /**
     * Returns a new object of class '<em>Equivalent Equipment</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Equivalent Equipment</em>'.
     * @generated
     */
    EquivalentEquipment createEquivalentEquipment();

    /**
     * Returns a new object of class '<em>Equivalent Shunt</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Equivalent Shunt</em>'.
     * @generated
     */
    EquivalentShunt createEquivalentShunt();

    /**
     * Returns a new object of class '<em>Energy Consumer</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Energy Consumer</em>'.
     * @generated
     */
    EnergyConsumer createEnergyConsumer();

    /**
     * Returns a new object of class '<em>Operational Limit Set</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Operational Limit Set</em>'.
     * @generated
     */
    OperationalLimitSet createOperationalLimitSet();

    /**
     * Returns a new object of class '<em>Power Cut Zone</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Power Cut Zone</em>'.
     * @generated
     */
    PowerCutZone createPowerCutZone();

    /**
     * Returns a new object of class '<em>Curve</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Curve</em>'.
     * @generated
     */
    Curve createCurve();

    /**
     * Returns a new object of class '<em>Non Conform Load Schedule</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Non Conform Load Schedule</em>'.
     * @generated
     */
    NonConformLoadSchedule createNonConformLoadSchedule();

    /**
     * Returns a new object of class '<em>Transformer End</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Transformer End</em>'.
     * @generated
     */
    TransformerEnd createTransformerEnd();

    /**
     * Returns a new object of class '<em>Target Level Schedule</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Target Level Schedule</em>'.
     * @generated
     */
    TargetLevelSchedule createTargetLevelSchedule();

    /**
     * Returns a new object of class '<em>Voltage Limit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Voltage Limit</em>'.
     * @generated
     */
    VoltageLimit createVoltageLimit();

    /**
     * Returns a new object of class '<em>Operating Participant</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Operating Participant</em>'.
     * @generated
     */
    OperatingParticipant createOperatingParticipant();

    /**
     * Returns a new object of class '<em>Current Limit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Current Limit</em>'.
     * @generated
     */
    CurrentLimit createCurrentLimit();

    /**
     * Returns a new object of class '<em>Alt Generating Unit Meas</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Alt Generating Unit Meas</em>'.
     * @generated
     */
    AltGeneratingUnitMeas createAltGeneratingUnitMeas();

    /**
     * Returns a new object of class '<em>Prime Mover</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Prime Mover</em>'.
     * @generated
     */
    PrimeMover createPrimeMover();

    /**
     * Returns a new object of class '<em>Phase Tap Changer Tabular</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Phase Tap Changer Tabular</em>'.
     * @generated
     */
    PhaseTapChangerTabular createPhaseTapChangerTabular();

    /**
     * Returns a new object of class '<em>Topological Node</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Topological Node</em>'.
     * @generated
     */
    TopologicalNode createTopologicalNode();

    /**
     * Returns a new object of class '<em>String Measurement</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>String Measurement</em>'.
     * @generated
     */
    StringMeasurement createStringMeasurement();

    /**
     * Returns a new object of class '<em>Energy Area</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Energy Area</em>'.
     * @generated
     */
    EnergyArea createEnergyArea();

    /**
     * Returns a new object of class '<em>Cogeneration Plant</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Cogeneration Plant</em>'.
     * @generated
     */
    CogenerationPlant createCogenerationPlant();

    /**
     * Returns a new object of class '<em>Gen Unit Op Schedule</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Gen Unit Op Schedule</em>'.
     * @generated
     */
    GenUnitOpSchedule createGenUnitOpSchedule();

    /**
     * Returns a new object of class '<em>Non Conform Load Group</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Non Conform Load Group</em>'.
     * @generated
     */
    NonConformLoadGroup createNonConformLoadGroup();

    /**
     * Returns a new object of class '<em>Raise Lower Command</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Raise Lower Command</em>'.
     * @generated
     */
    RaiseLowerCommand createRaiseLowerCommand();

    /**
     * Returns a new object of class '<em>Basic Interval Schedule</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Basic Interval Schedule</em>'.
     * @generated
     */
    BasicIntervalSchedule createBasicIntervalSchedule();

    /**
     * Returns a new object of class '<em>Operational Limit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Operational Limit</em>'.
     * @generated
     */
    OperationalLimit createOperationalLimit();

    /**
     * Returns a new object of class '<em>Station Supply</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Station Supply</em>'.
     * @generated
     */
    StationSupply createStationSupply();

    /**
     * Returns a new object of class '<em>Sub Geographical Region</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Sub Geographical Region</em>'.
     * @generated
     */
    SubGeographicalRegion createSubGeographicalRegion();

    /**
     * Returns a new object of class '<em>Hydro Turbine</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Hydro Turbine</em>'.
     * @generated
     */
    HydroTurbine createHydroTurbine();

    /**
     * Returns a new object of class '<em>ACDC Converter DC Terminal</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>ACDC Converter DC Terminal</em>'.
     * @generated
     */
    ACDCConverterDCTerminal createACDCConverterDCTerminal();

    /**
     * Returns a new object of class '<em>Transformer Tank End</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Transformer Tank End</em>'.
     * @generated
     */
    TransformerTankEnd createTransformerTankEnd();

    /**
     * Returns a new object of class '<em>Name Type</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Name Type</em>'.
     * @generated
     */
    NameType createNameType();

    /**
     * Returns a new object of class '<em>Regulating Control</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Regulating Control</em>'.
     * @generated
     */
    RegulatingControl createRegulatingControl();

    /**
     * Returns a new object of class '<em>Reporting Group</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Reporting Group</em>'.
     * @generated
     */
    ReportingGroup createReportingGroup();

    /**
     * Returns a new object of class '<em>Alt Tie Meas</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Alt Tie Meas</em>'.
     * @generated
     */
    AltTieMeas createAltTieMeas();

    /**
     * Returns a new object of class '<em>Energy Source Phase</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Energy Source Phase</em>'.
     * @generated
     */
    EnergySourcePhase createEnergySourcePhase();

    /**
     * Returns a new object of class '<em>Communication Link</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Communication Link</em>'.
     * @generated
     */
    CommunicationLink createCommunicationLink();

    /**
     * Returns a new object of class '<em>Nonlinear Shunt Compensator Point</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Nonlinear Shunt Compensator Point</em>'.
     * @generated
     */
    NonlinearShuntCompensatorPoint createNonlinearShuntCompensatorPoint();

    /**
     * Returns a new object of class '<em>Synchronous Machine</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Synchronous Machine</em>'.
     * @generated
     */
    SynchronousMachine createSynchronousMachine();

    /**
     * Returns a new object of class '<em>Heat Rate Curve</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Heat Rate Curve</em>'.
     * @generated
     */
    HeatRateCurve createHeatRateCurve();

    /**
     * Returns a new object of class '<em>Base Voltage</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Base Voltage</em>'.
     * @generated
     */
    BaseVoltage createBaseVoltage();

    /**
     * Returns a new object of class '<em>Vs Converter</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Vs Converter</em>'.
     * @generated
     */
    VsConverter createVsConverter();

    /**
     * Returns a new object of class '<em>Energy Connection</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Energy Connection</em>'.
     * @generated
     */
    EnergyConnection createEnergyConnection();

    /**
     * Returns a new object of class '<em>Switch Phase</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Switch Phase</em>'.
     * @generated
     */
    SwitchPhase createSwitchPhase();

    /**
     * Returns a new object of class '<em>Conform Load Group</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Conform Load Group</em>'.
     * @generated
     */
    ConformLoadGroup createConformLoadGroup();

    /**
     * Returns a new object of class '<em>Diagram Object</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Diagram Object</em>'.
     * @generated
     */
    DiagramObject createDiagramObject();

    /**
     * Returns a new object of class '<em>Hydro Generating Unit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Hydro Generating Unit</em>'.
     * @generated
     */
    HydroGeneratingUnit createHydroGeneratingUnit();

    /**
     * Returns a new object of class '<em>Disconnector</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Disconnector</em>'.
     * @generated
     */
    Disconnector createDisconnector();

    /**
     * Returns a new object of class '<em>Per Length Impedance</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Per Length Impedance</em>'.
     * @generated
     */
    PerLengthImpedance createPerLengthImpedance();

    /**
     * Returns a new object of class '<em>Substation</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Substation</em>'.
     * @generated
     */
    Substation createSubstation();

    /**
     * Returns a new object of class '<em>Diagram Object Style</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Diagram Object Style</em>'.
     * @generated
     */
    DiagramObjectStyle createDiagramObjectStyle();

    /**
     * Returns a new object of class '<em>Command</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Command</em>'.
     * @generated
     */
    Command createCommand();

    /**
     * Returns a new object of class '<em>Shunt Compensator</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Shunt Compensator</em>'.
     * @generated
     */
    ShuntCompensator createShuntCompensator();

    /**
     * Returns a new object of class '<em>Shunt Compensator Phase</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Shunt Compensator Phase</em>'.
     * @generated
     */
    ShuntCompensatorPhase createShuntCompensatorPhase();

    /**
     * Returns a new object of class '<em>Reclose Sequence</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Reclose Sequence</em>'.
     * @generated
     */
    RecloseSequence createRecloseSequence();

    /**
     * Returns a new object of class '<em>Voltage Control Zone</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Voltage Control Zone</em>'.
     * @generated
     */
    VoltageControlZone createVoltageControlZone();

    /**
     * Returns a new object of class '<em>Steam Supply</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Steam Supply</em>'.
     * @generated
     */
    SteamSupply createSteamSupply();

    /**
     * Returns a new object of class '<em>Measurement Value Quality</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Measurement Value Quality</em>'.
     * @generated
     */
    MeasurementValueQuality createMeasurementValueQuality();

    /**
     * Returns a new object of class '<em>Equipment Fault</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Equipment Fault</em>'.
     * @generated
     */
    EquipmentFault createEquipmentFault();

    /**
     * Returns a new object of class '<em>DC Equipment Container</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>DC Equipment Container</em>'.
     * @generated
     */
    DCEquipmentContainer createDCEquipmentContainer();

    /**
     * Returns a new object of class '<em>Heat Recovery Boiler</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Heat Recovery Boiler</em>'.
     * @generated
     */
    HeatRecoveryBoiler createHeatRecoveryBoiler();

    /**
     * Returns a new object of class '<em>Energy Scheduling Type</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Energy Scheduling Type</em>'.
     * @generated
     */
    EnergySchedulingType createEnergySchedulingType();

    /**
     * Returns a new object of class '<em>Contingency Equipment</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Contingency Equipment</em>'.
     * @generated
     */
    ContingencyEquipment createContingencyEquipment();

    /**
     * Returns a new object of class '<em>Conform Load Schedule</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Conform Load Schedule</em>'.
     * @generated
     */
    ConformLoadSchedule createConformLoadSchedule();

    /**
     * Returns a new object of class '<em>Regulation Schedule</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Regulation Schedule</em>'.
     * @generated
     */
    RegulationSchedule createRegulationSchedule();

    /**
     * Returns a new object of class '<em>Tailbay Loss Curve</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Tailbay Loss Curve</em>'.
     * @generated
     */
    TailbayLossCurve createTailbayLossCurve();

    /**
     * Returns a new object of class '<em>Identified Object</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Identified Object</em>'.
     * @generated
     */
    IdentifiedObject createIdentifiedObject();

    /**
     * Returns a new object of class '<em>Grounding Impedance</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Grounding Impedance</em>'.
     * @generated
     */
    GroundingImpedance createGroundingImpedance();

    /**
     * Returns a new object of class '<em>Power Electronics Connection</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Power Electronics Connection</em>'.
     * @generated
     */
    PowerElectronicsConnection createPowerElectronicsConnection();

    /**
     * Returns a new object of class '<em>Ground</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Ground</em>'.
     * @generated
     */
    Ground createGround();

    /**
     * Returns a new object of class '<em>Diagram</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Diagram</em>'.
     * @generated
     */
    Diagram createDiagram();

    /**
     * Returns a new object of class '<em>Sv Voltage</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Sv Voltage</em>'.
     * @generated
     */
    SvVoltage createSvVoltage();

    /**
     * Returns a new object of class '<em>Transformer Mesh Impedance</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Transformer Mesh Impedance</em>'.
     * @generated
     */
    TransformerMeshImpedance createTransformerMeshImpedance();

    /**
     * Returns a new object of class '<em>DC Node</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>DC Node</em>'.
     * @generated
     */
    DCNode createDCNode();

    /**
     * Returns a new object of class '<em>Surge Arrester</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Surge Arrester</em>'.
     * @generated
     */
    SurgeArrester createSurgeArrester();

    /**
     * Returns a new object of class '<em>Irregular Interval Schedule</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Irregular Interval Schedule</em>'.
     * @generated
     */
    IrregularIntervalSchedule createIrregularIntervalSchedule();

    /**
     * Returns a new object of class '<em>Ratio Tap Changer Table Point</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Ratio Tap Changer Table Point</em>'.
     * @generated
     */
    RatioTapChangerTablePoint createRatioTapChangerTablePoint();

    /**
     * Returns a new object of class '<em>Cs Converter</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Cs Converter</em>'.
     * @generated
     */
    CsConverter createCsConverter();

    /**
     * Returns a new object of class '<em>Season Day Type Schedule</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Season Day Type Schedule</em>'.
     * @generated
     */
    SeasonDayTypeSchedule createSeasonDayTypeSchedule();

    /**
     * Returns a new object of class '<em>Power System Resource</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Power System Resource</em>'.
     * @generated
     */
    PowerSystemResource createPowerSystemResource();

    /**
     * Returns a new object of class '<em>Junction</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Junction</em>'.
     * @generated
     */
    Junction createJunction();

    /**
     * Returns a new object of class '<em>Line</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Line</em>'.
     * @generated
     */
    Line createLine();

    /**
     * Returns a new object of class '<em>Jumper</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Jumper</em>'.
     * @generated
     */
    Jumper createJumper();

    /**
     * Returns a new object of class '<em>Non Conform Load</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Non Conform Load</em>'.
     * @generated
     */
    NonConformLoad createNonConformLoad();

    /**
     * Returns a new object of class '<em>Busbar Section</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Busbar Section</em>'.
     * @generated
     */
    BusbarSection createBusbarSection();

    /**
     * Returns a new object of class '<em>IO Point</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>IO Point</em>'.
     * @generated
     */
    IOPoint createIOPoint();

    /**
     * Returns a new object of class '<em>Bus Name Marker</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Bus Name Marker</em>'.
     * @generated
     */
    BusNameMarker createBusNameMarker();

    /**
     * Returns a new object of class '<em>DC Topological Node</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>DC Topological Node</em>'.
     * @generated
     */
    DCTopologicalNode createDCTopologicalNode();

    /**
     * Returns a new object of class '<em>DC Shunt</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>DC Shunt</em>'.
     * @generated
     */
    DCShunt createDCShunt();

    /**
     * Returns a new object of class '<em>Bay</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Bay</em>'.
     * @generated
     */
    Bay createBay();

    /**
     * Returns a new object of class '<em>Per Length Phase Impedance</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Per Length Phase Impedance</em>'.
     * @generated
     */
    PerLengthPhaseImpedance createPerLengthPhaseImpedance();

    /**
     * Returns a new object of class '<em>Mutual Coupling</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Mutual Coupling</em>'.
     * @generated
     */
    MutualCoupling createMutualCoupling();

    /**
     * Returns a new object of class '<em>Load Group</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Load Group</em>'.
     * @generated
     */
    LoadGroup createLoadGroup();

    /**
     * Returns a new object of class '<em>Supercritical</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Supercritical</em>'.
     * @generated
     */
    Supercritical createSupercritical();

    /**
     * Returns a new object of class '<em>Analog</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Analog</em>'.
     * @generated
     */
    Analog createAnalog();

    /**
     * Returns a new object of class '<em>Control Area</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Control Area</em>'.
     * @generated
     */
    ControlArea createControlArea();

    /**
     * Returns a new object of class '<em>Start Main Fuel Curve</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Start Main Fuel Curve</em>'.
     * @generated
     */
    StartMainFuelCurve createStartMainFuelCurve();

    /**
     * Returns a new object of class '<em>Measurement Value</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Measurement Value</em>'.
     * @generated
     */
    MeasurementValue createMeasurementValue();

    /**
     * Returns a new object of class '<em>Power Electronics Wind Unit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Power Electronics Wind Unit</em>'.
     * @generated
     */
    PowerElectronicsWindUnit createPowerElectronicsWindUnit();

    /**
     * Returns a new object of class '<em>Tap Changer</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Tap Changer</em>'.
     * @generated
     */
    TapChanger createTapChanger();

    /**
     * Returns a new object of class '<em>Connector</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Connector</em>'.
     * @generated
     */
    Connector createConnector();

    /**
     * Returns a new object of class '<em>Current Transformer</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Current Transformer</em>'.
     * @generated
     */
    CurrentTransformer createCurrentTransformer();

    /**
     * Returns a new object of class '<em>Drum Boiler</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Drum Boiler</em>'.
     * @generated
     */
    DrumBoiler createDrumBoiler();

    /**
     * Returns a new object of class '<em>DC Topological Island</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>DC Topological Island</em>'.
     * @generated
     */
    DCTopologicalIsland createDCTopologicalIsland();

    /**
     * Returns a new object of class '<em>PWR Steam Supply</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>PWR Steam Supply</em>'.
     * @generated
     */
    PWRSteamSupply createPWRSteamSupply();

    /**
     * Returns a new object of class '<em>Diagram Object Glue Point</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Diagram Object Glue Point</em>'.
     * @generated
     */
    DiagramObjectGluePoint createDiagramObjectGluePoint();

    /**
     * Returns a new object of class '<em>Regular Interval Schedule</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Regular Interval Schedule</em>'.
     * @generated
     */
    RegularIntervalSchedule createRegularIntervalSchedule();

    /**
     * Returns a new object of class '<em>Sub Load Area</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Sub Load Area</em>'.
     * @generated
     */
    SubLoadArea createSubLoadArea();

    /**
     * Returns a new object of class '<em>Curve Data</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Curve Data</em>'.
     * @generated
     */
    CurveData createCurveData();

    /**
     * Returns a new object of class '<em>Wind Generating Unit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Wind Generating Unit</em>'.
     * @generated
     */
    WindGeneratingUnit createWindGeneratingUnit();

    /**
     * Returns a new object of class '<em>Transformer Tank</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Transformer Tank</em>'.
     * @generated
     */
    TransformerTank createTransformerTank();

    /**
     * Returns a new object of class '<em>Equipment</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Equipment</em>'.
     * @generated
     */
    Equipment createEquipment();

    /**
     * Returns a new object of class '<em>Combined Cycle Plant</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Combined Cycle Plant</em>'.
     * @generated
     */
    CombinedCyclePlant createCombinedCyclePlant();

    /**
     * Returns a new object of class '<em>Branch Group</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Branch Group</em>'.
     * @generated
     */
    BranchGroup createBranchGroup();

    /**
     * Returns a new object of class '<em>Set Point</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Set Point</em>'.
     * @generated
     */
    SetPoint createSetPoint();

    /**
     * Returns a new object of class '<em>Accumulator Limit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Accumulator Limit</em>'.
     * @generated
     */
    AccumulatorLimit createAccumulatorLimit();

    /**
     * Returns a new object of class '<em>Sectionaliser</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Sectionaliser</em>'.
     * @generated
     */
    Sectionaliser createSectionaliser();

    /**
     * Returns a new object of class '<em>Tap Schedule</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Tap Schedule</em>'.
     * @generated
     */
    TapSchedule createTapSchedule();

    /**
     * Returns a new object of class '<em>Line Fault</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Line Fault</em>'.
     * @generated
     */
    LineFault createLineFault();

    /**
     * Returns a new object of class '<em>Generating Unit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Generating Unit</em>'.
     * @generated
     */
    GeneratingUnit createGeneratingUnit();

    /**
     * Returns a new object of class '<em>Hydro Generating Efficiency Curve</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Hydro Generating Efficiency Curve</em>'.
     * @generated
     */
    HydroGeneratingEfficiencyCurve createHydroGeneratingEfficiencyCurve();

    /**
     * Returns a new object of class '<em>Remote Unit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Remote Unit</em>'.
     * @generated
     */
    RemoteUnit createRemoteUnit();

    /**
     * Returns a new object of class '<em>Sv Power Flow</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Sv Power Flow</em>'.
     * @generated
     */
    SvPowerFlow createSvPowerFlow();

    /**
     * Returns a new object of class '<em>Fossil Steam Supply</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Fossil Steam Supply</em>'.
     * @generated
     */
    FossilSteamSupply createFossilSteamSupply();

    /**
     * Returns a new object of class '<em>Base Power</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Base Power</em>'.
     * @generated
     */
    BasePower createBasePower();

    /**
     * Returns a new object of class '<em>DC Base Terminal</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>DC Base Terminal</em>'.
     * @generated
     */
    DCBaseTerminal createDCBaseTerminal();

    /**
     * Returns a new object of class '<em>Operational Limit Type</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Operational Limit Type</em>'.
     * @generated
     */
    OperationalLimitType createOperationalLimitType();

    /**
     * Returns a new object of class '<em>Sv Injection</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Sv Injection</em>'.
     * @generated
     */
    SvInjection createSvInjection();

    /**
     * Returns a new object of class '<em>Active Power Limit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Active Power Limit</em>'.
     * @generated
     */
    ActivePowerLimit createActivePowerLimit();

    /**
     * Returns a new object of class '<em>Conform Load</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Conform Load</em>'.
     * @generated
     */
    ConformLoad createConformLoad();

    /**
     * Returns a new object of class '<em>DC Terminal</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>DC Terminal</em>'.
     * @generated
     */
    DCTerminal createDCTerminal();

    /**
     * Returns a new object of class '<em>Synchrocheck Relay</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Synchrocheck Relay</em>'.
     * @generated
     */
    SynchrocheckRelay createSynchrocheckRelay();

    /**
     * Returns a new object of class '<em>Start Ramp Curve</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Start Ramp Curve</em>'.
     * @generated
     */
    StartRampCurve createStartRampCurve();

    /**
     * Returns a new object of class '<em>PSR Type</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>PSR Type</em>'.
     * @generated
     */
    PSRType createPSRType();

    /**
     * Returns a new object of class '<em>Plant</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Plant</em>'.
     * @generated
     */
    Plant createPlant();

    /**
     * Returns a new object of class '<em>Measurement Value Source</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Measurement Value Source</em>'.
     * @generated
     */
    MeasurementValueSource createMeasurementValueSource();

    /**
     * Returns a new object of class '<em>Feeder</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Feeder</em>'.
     * @generated
     */
    Feeder createFeeder();

    /**
     * Returns a new object of class '<em>Startup Model</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Startup Model</em>'.
     * @generated
     */
    StartupModel createStartupModel();

    /**
     * Returns a new object of class '<em>Fault Indicator</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Fault Indicator</em>'.
     * @generated
     */
    FaultIndicator createFaultIndicator();

    /**
     * Returns a new object of class '<em>Flow Sensor</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Flow Sensor</em>'.
     * @generated
     */
    FlowSensor createFlowSensor();

    /**
     * Returns a new object of class '<em>ACDC Converter</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>ACDC Converter</em>'.
     * @generated
     */
    ACDCConverter createACDCConverter();

    /**
     * Returns a new object of class '<em>Energy Source</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Energy Source</em>'.
     * @generated
     */
    EnergySource createEnergySource();

    /**
     * Returns a new object of class '<em>Ground Disconnector</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Ground Disconnector</em>'.
     * @generated
     */
    GroundDisconnector createGroundDisconnector();

    /**
     * Returns a new object of class '<em>Phase Impedance Data</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Phase Impedance Data</em>'.
     * @generated
     */
    PhaseImpedanceData createPhaseImpedanceData();

    /**
     * Returns a new object of class '<em>Analog Limit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Analog Limit</em>'.
     * @generated
     */
    AnalogLimit createAnalogLimit();

    /**
     * Returns a new object of class '<em>Regular Time Point</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Regular Time Point</em>'.
     * @generated
     */
    RegularTimePoint createRegularTimePoint();

    /**
     * Returns a new object of class '<em>Fuse</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Fuse</em>'.
     * @generated
     */
    Fuse createFuse();

    /**
     * Returns a new object of class '<em>Cut</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Cut</em>'.
     * @generated
     */
    Cut createCut();

    /**
     * Returns a new object of class '<em>Measurement</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Measurement</em>'.
     * @generated
     */
    Measurement createMeasurement();

    /**
     * Returns a new object of class '<em>Level Vs Volume Curve</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Level Vs Volume Curve</em>'.
     * @generated
     */
    LevelVsVolumeCurve createLevelVsVolumeCurve();

    /**
     * Returns a new object of class '<em>DC Ground</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>DC Ground</em>'.
     * @generated
     */
    DCGround createDCGround();

    /**
     * Returns a new object of class '<em>Battery Unit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Battery Unit</em>'.
     * @generated
     */
    BatteryUnit createBatteryUnit();

    /**
     * Returns a new object of class '<em>Accumulator Limit Set</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Accumulator Limit Set</em>'.
     * @generated
     */
    AccumulatorLimitSet createAccumulatorLimitSet();

    /**
     * Returns a new object of class '<em>Photo Voltaic Unit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Photo Voltaic Unit</em>'.
     * @generated
     */
    PhotoVoltaicUnit createPhotoVoltaicUnit();

    /**
     * Returns a new object of class '<em>DC Chopper</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>DC Chopper</em>'.
     * @generated
     */
    DCChopper createDCChopper();

    /**
     * Returns a new object of class '<em>Remote Source</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Remote Source</em>'.
     * @generated
     */
    RemoteSource createRemoteSource();

    /**
     * Returns a new object of class '<em>Phase Tap Changer</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Phase Tap Changer</em>'.
     * @generated
     */
    PhaseTapChanger createPhaseTapChanger();

    /**
     * Returns a new object of class '<em>Heat Input Curve</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Heat Input Curve</em>'.
     * @generated
     */
    HeatInputCurve createHeatInputCurve();

    /**
     * Returns a new object of class '<em>Thermal Generating Unit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Thermal Generating Unit</em>'.
     * @generated
     */
    ThermalGeneratingUnit createThermalGeneratingUnit();

    /**
     * Returns a new object of class '<em>Gross To Net Active Power Curve</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Gross To Net Active Power Curve</em>'.
     * @generated
     */
    GrossToNetActivePowerCurve createGrossToNetActivePowerCurve();

    /**
     * Returns a new object of class '<em>Phase Tap Changer Symmetrical</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Phase Tap Changer Symmetrical</em>'.
     * @generated
     */
    PhaseTapChangerSymmetrical createPhaseTapChangerSymmetrical();

    /**
     * Returns a new object of class '<em>Tie Flow</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Tie Flow</em>'.
     * @generated
     */
    TieFlow createTieFlow();

    /**
     * Returns a new object of class '<em>Accumulator</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Accumulator</em>'.
     * @generated
     */
    Accumulator createAccumulator();

    /**
     * Returns a new object of class '<em>Nonlinear Shunt Compensator</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Nonlinear Shunt Compensator</em>'.
     * @generated
     */
    NonlinearShuntCompensator createNonlinearShuntCompensator();

    /**
     * Returns a new object of class '<em>Fault</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Fault</em>'.
     * @generated
     */
    Fault createFault();

    /**
     * Returns a new object of class '<em>Contingency</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Contingency</em>'.
     * @generated
     */
    Contingency createContingency();

    /**
     * Returns a new object of class '<em>Breaker</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Breaker</em>'.
     * @generated
     */
    Breaker createBreaker();

    /**
     * Returns a new object of class '<em>CAES Plant</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CAES Plant</em>'.
     * @generated
     */
    CAESPlant createCAESPlant();

    /**
     * Returns a new object of class '<em>Tap Changer Table Point</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Tap Changer Table Point</em>'.
     * @generated
     */
    TapChangerTablePoint createTapChangerTablePoint();

    /**
     * Returns a new object of class '<em>Fault Cause Type</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Fault Cause Type</em>'.
     * @generated
     */
    FaultCauseType createFaultCauseType();

    /**
     * Returns a new object of class '<em>DC Breaker</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>DC Breaker</em>'.
     * @generated
     */
    DCBreaker createDCBreaker();

    /**
     * Returns a new object of class '<em>Ratio Tap Changer</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Ratio Tap Changer</em>'.
     * @generated
     */
    RatioTapChanger createRatioTapChanger();

    /**
     * Returns a new object of class '<em>Load Area</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Load Area</em>'.
     * @generated
     */
    LoadArea createLoadArea();

    /**
     * Returns a new object of class '<em>Fuel Allocation Schedule</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Fuel Allocation Schedule</em>'.
     * @generated
     */
    FuelAllocationSchedule createFuelAllocationSchedule();

    /**
     * Returns a new object of class '<em>AC Line Segment</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>AC Line Segment</em>'.
     * @generated
     */
    ACLineSegment createACLineSegment();

    /**
     * Returns a new object of class '<em>Limit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Limit</em>'.
     * @generated
     */
    Limit createLimit();

    /**
     * Returns a new object of class '<em>Value Alias Set</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Value Alias Set</em>'.
     * @generated
     */
    ValueAliasSet createValueAliasSet();

    /**
     * Returns a new object of class '<em>Limit Set</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Limit Set</em>'.
     * @generated
     */
    LimitSet createLimitSet();

    /**
     * Returns a new object of class '<em>State Variable</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>State Variable</em>'.
     * @generated
     */
    StateVariable createStateVariable();

    /**
     * Returns a new object of class '<em>Discrete Value</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Discrete Value</em>'.
     * @generated
     */
    DiscreteValue createDiscreteValue();

    /**
     * Returns a new object of class '<em>Accumulator Value</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Accumulator Value</em>'.
     * @generated
     */
    AccumulatorValue createAccumulatorValue();

    /**
     * Returns a new object of class '<em>Transformer Core Admittance</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Transformer Core Admittance</em>'.
     * @generated
     */
    TransformerCoreAdmittance createTransformerCoreAdmittance();

    /**
     * Returns a new object of class '<em>Transformer Star Impedance</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Transformer Star Impedance</em>'.
     * @generated
     */
    TransformerStarImpedance createTransformerStarImpedance();

    /**
     * Returns a new object of class '<em>Voltage Level</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Voltage Level</em>'.
     * @generated
     */
    VoltageLevel createVoltageLevel();

    /**
     * Returns a new object of class '<em>Accumulator Reset</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Accumulator Reset</em>'.
     * @generated
     */
    AccumulatorReset createAccumulatorReset();

    /**
     * Returns a new object of class '<em>Phase Tap Changer Table Point</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Phase Tap Changer Table Point</em>'.
     * @generated
     */
    PhaseTapChangerTablePoint createPhaseTapChangerTablePoint();

    /**
     * Returns a new object of class '<em>Sv Switch</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Sv Switch</em>'.
     * @generated
     */
    SvSwitch createSvSwitch();

    /**
     * Returns a new object of class '<em>Topological Island</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Topological Island</em>'.
     * @generated
     */
    TopologicalIsland createTopologicalIsland();

    /**
     * Returns a new object of class '<em>DC Conducting Equipment</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>DC Conducting Equipment</em>'.
     * @generated
     */
    DCConductingEquipment createDCConductingEquipment();

    /**
     * Returns a new object of class '<em>Protection Equipment</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Protection Equipment</em>'.
     * @generated
     */
    ProtectionEquipment createProtectionEquipment();

    /**
     * Returns a new object of class '<em>Load Response Characteristic</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Load Response Characteristic</em>'.
     * @generated
     */
    LoadResponseCharacteristic createLoadResponseCharacteristic();

    /**
     * Returns a new object of class '<em>Power Electronics Unit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Power Electronics Unit</em>'.
     * @generated
     */
    PowerElectronicsUnit createPowerElectronicsUnit();

    /**
     * Returns a new object of class '<em>Analog Limit Set</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Analog Limit Set</em>'.
     * @generated
     */
    AnalogLimitSet createAnalogLimitSet();

    /**
     * Returns a new object of class '<em>Post Line Sensor</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Post Line Sensor</em>'.
     * @generated
     */
    PostLineSensor createPostLineSensor();

    /**
     * Returns a new object of class '<em>Connectivity Node Container</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Connectivity Node Container</em>'.
     * @generated
     */
    ConnectivityNodeContainer createConnectivityNodeContainer();

    /**
     * Returns a new object of class '<em>Solar Generating Unit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Solar Generating Unit</em>'.
     * @generated
     */
    SolarGeneratingUnit createSolarGeneratingUnit();

    /**
     * Returns a new object of class '<em>Air Compressor</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Air Compressor</em>'.
     * @generated
     */
    AirCompressor createAirCompressor();

    /**
     * Returns a new object of class '<em>Phase Tap Changer Asymmetrical</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Phase Tap Changer Asymmetrical</em>'.
     * @generated
     */
    PhaseTapChangerAsymmetrical createPhaseTapChangerAsymmetrical();

    /**
     * Returns a new object of class '<em>Earth Fault Compensator</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Earth Fault Compensator</em>'.
     * @generated
     */
    EarthFaultCompensator createEarthFaultCompensator();

    /**
     * Returns a new object of class '<em>Sv Shunt Compensator Sections</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Sv Shunt Compensator Sections</em>'.
     * @generated
     */
    SvShuntCompensatorSections createSvShuntCompensatorSections();

    /**
     * Returns a new object of class '<em>AC Line Segment Phase</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>AC Line Segment Phase</em>'.
     * @generated
     */
    ACLineSegmentPhase createACLineSegmentPhase();

    /**
     * Returns a new object of class '<em>Linear Shunt Compensator Phase</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Linear Shunt Compensator Phase</em>'.
     * @generated
     */
    LinearShuntCompensatorPhase createLinearShuntCompensatorPhase();

    /**
     * Returns a new object of class '<em>Petersen Coil</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Petersen Coil</em>'.
     * @generated
     */
    PetersenCoil createPetersenCoil();

    /**
     * Returns a new object of class '<em>DC Series Device</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>DC Series Device</em>'.
     * @generated
     */
    DCSeriesDevice createDCSeriesDevice();

    /**
     * Returns a new object of class '<em>Conductor</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Conductor</em>'.
     * @generated
     */
    Conductor createConductor();

    /**
     * Returns a new object of class '<em>Equivalent Injection</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Equivalent Injection</em>'.
     * @generated
     */
    EquivalentInjection createEquivalentInjection();

    /**
     * Returns a new object of class '<em>Fossil Fuel</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Fossil Fuel</em>'.
     * @generated
     */
    FossilFuel createFossilFuel();

    /**
     * Returns a new object of class '<em>DC Line Segment</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>DC Line Segment</em>'.
     * @generated
     */
    DCLineSegment createDCLineSegment();

    /**
     * Returns a new object of class '<em>Analog Value</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Analog Value</em>'.
     * @generated
     */
    AnalogValue createAnalogValue();

    /**
     * Returns a new object of class '<em>Reporting Super Group</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Reporting Super Group</em>'.
     * @generated
     */
    ReportingSuperGroup createReportingSuperGroup();

    /**
     * Returns a new object of class '<em>Per Length Sequence Impedance</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Per Length Sequence Impedance</em>'.
     * @generated
     */
    PerLengthSequenceImpedance createPerLengthSequenceImpedance();

    /**
     * Returns a new object of class '<em>Season</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Season</em>'.
     * @generated
     */
    Season createSeason();

    /**
     * Returns a new object of class '<em>Diagram Style</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Diagram Style</em>'.
     * @generated
     */
    DiagramStyle createDiagramStyle();

    /**
     * Returns a new object of class '<em>Per Length Line Parameter</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Per Length Line Parameter</em>'.
     * @generated
     */
    PerLengthLineParameter createPerLengthLineParameter();

    /**
     * Returns a new object of class '<em>Steam Turbine</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Steam Turbine</em>'.
     * @generated
     */
    SteamTurbine createSteamTurbine();

    /**
     * Returns a new object of class '<em>Power Transformer End</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Power Transformer End</em>'.
     * @generated
     */
    PowerTransformerEnd createPowerTransformerEnd();

    /**
     * Returns a new object of class '<em>Control Area Generating Unit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Control Area Generating Unit</em>'.
     * @generated
     */
    ControlAreaGeneratingUnit createControlAreaGeneratingUnit();

    /**
     * Returns a new object of class '<em>Linear Shunt Compensator</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Linear Shunt Compensator</em>'.
     * @generated
     */
    LinearShuntCompensator createLinearShuntCompensator();

    /**
     * Returns a new object of class '<em>CT Temp Active Power Curve</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>CT Temp Active Power Curve</em>'.
     * @generated
     */
    CTTempActivePowerCurve createCTTempActivePowerCurve();

    /**
     * Returns a new object of class '<em>Phase Tap Changer Linear</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Phase Tap Changer Linear</em>'.
     * @generated
     */
    PhaseTapChangerLinear createPhaseTapChangerLinear();

    /**
     * Returns a new object of class '<em>Hydro Power Plant</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Hydro Power Plant</em>'.
     * @generated
     */
    HydroPowerPlant createHydroPowerPlant();

    /**
     * Returns a new object of class '<em>Sv Status</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Sv Status</em>'.
     * @generated
     */
    SvStatus createSvStatus();

    /**
     * Returns a new object of class '<em>Hydro Pump Op Schedule</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Hydro Pump Op Schedule</em>'.
     * @generated
     */
    HydroPumpOpSchedule createHydroPumpOpSchedule();

    /**
     * Returns a new object of class '<em>Potential Transformer</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Potential Transformer</em>'.
     * @generated
     */
    PotentialTransformer createPotentialTransformer();

    /**
     * Returns a new object of class '<em>Visibility Layer</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Visibility Layer</em>'.
     * @generated
     */
    VisibilityLayer createVisibilityLayer();

    /**
     * Returns a new object of class '<em>Terminal</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Terminal</em>'.
     * @generated
     */
    Terminal createTerminal();

    /**
     * Returns a new object of class '<em>Name</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Name</em>'.
     * @generated
     */
    Name createName();

    /**
     * Returns a new object of class '<em>Branch Group Terminal</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Branch Group Terminal</em>'.
     * @generated
     */
    BranchGroupTerminal createBranchGroupTerminal();

    /**
     * Returns a new object of class '<em>Sensor</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Sensor</em>'.
     * @generated
     */
    Sensor createSensor();

    /**
     * Returns a new object of class '<em>Conducting Equipment</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Conducting Equipment</em>'.
     * @generated
     */
    ConductingEquipment createConductingEquipment();

    /**
     * Returns a new object of class '<em>Base Frequency</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Base Frequency</em>'.
     * @generated
     */
    BaseFrequency createBaseFrequency();

    /**
     * Returns a new object of class '<em>Sv Tap Step</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Sv Tap Step</em>'.
     * @generated
     */
    SvTapStep createSvTapStep();

    /**
     * Returns a new object of class '<em>Per Length DC Line Parameter</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Per Length DC Line Parameter</em>'.
     * @generated
     */
    PerLengthDCLineParameter createPerLengthDCLineParameter();

    /**
     * Returns a new object of class '<em>Equivalent Branch</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Equivalent Branch</em>'.
     * @generated
     */
    EquivalentBranch createEquivalentBranch();

    /**
     * Returns a new object of class '<em>DC Converter Unit</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>DC Converter Unit</em>'.
     * @generated
     */
    DCConverterUnit createDCConverterUnit();

    /**
     * Returns a new object of class '<em>Power Transformer</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Power Transformer</em>'.
     * @generated
     */
    PowerTransformer createPowerTransformer();

    /**
     * Returns a new object of class '<em>Geographical Region</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Geographical Region</em>'.
     * @generated
     */
    GeographicalRegion createGeographicalRegion();

    /**
     * Returns a new object of class '<em>Phase Tap Changer Table</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Phase Tap Changer Table</em>'.
     * @generated
     */
    PhaseTapChangerTable createPhaseTapChangerTable();

    /**
     * Returns a new object of class '<em>Incremental Heat Rate Curve</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Incremental Heat Rate Curve</em>'.
     * @generated
     */
    IncrementalHeatRateCurve createIncrementalHeatRateCurve();

    /**
     * Returns a new object of class '<em>Emission Curve</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Emission Curve</em>'.
     * @generated
     */
    EmissionCurve createEmissionCurve();

    /**
     * Returns a new object of class '<em>Nonlinear Shunt Compensator Phase</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Nonlinear Shunt Compensator Phase</em>'.
     * @generated
     */
    NonlinearShuntCompensatorPhase createNonlinearShuntCompensatorPhase();

    /**
     * Returns a new object of class '<em>External Network Injection</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>External Network Injection</em>'.
     * @generated
     */
    ExternalNetworkInjection createExternalNetworkInjection();

    /**
     * Returns a new object of class '<em>Wave Trap</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Wave Trap</em>'.
     * @generated
     */
    WaveTrap createWaveTrap();

    /**
     * Returns a new object of class '<em>Irregular Time Point</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Irregular Time Point</em>'.
     * @generated
     */
    IrregularTimePoint createIrregularTimePoint();

    /**
     * Returns a new object of class '<em>Quality61850</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Quality61850</em>'.
     * @generated
     */
    Quality61850 createQuality61850();

    /**
     * Returns a new object of class '<em>Equivalent Network</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Equivalent Network</em>'.
     * @generated
     */
    EquivalentNetwork createEquivalentNetwork();

    /**
     * Returns a new object of class '<em>Vs Capability Curve</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Vs Capability Curve</em>'.
     * @generated
     */
    VsCapabilityCurve createVsCapabilityCurve();

    /**
     * Returns a new object of class '<em>Reservoir</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Reservoir</em>'.
     * @generated
     */
    Reservoir createReservoir();

    /**
     * Returns a new object of class '<em>Switch Schedule</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Switch Schedule</em>'.
     * @generated
     */
    SwitchSchedule createSwitchSchedule();

    /**
     * Returns a new object of class '<em>Shutdown Curve</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Shutdown Curve</em>'.
     * @generated
     */
    ShutdownCurve createShutdownCurve();

    /**
     * Returns a new object of class '<em>Remote Point</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return a new object of class '<em>Remote Point</em>'.
     * @generated
     */
    RemotePoint createRemotePoint();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @return the package supported by this factory.
     * @generated
     */
    CimPackage getCimPackage();

} //CimFactory
